﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Trialcar.Web.Infrastructure
{
    public class ControllerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(

              //All MVC controllers
              // Classes.FromThisAssembly().BasedOn<IController>().LifestyleTransient()
              Classes.FromThisAssembly()
                                   .BasedOn<IController>()
                                    .If(t => t.Name.EndsWith("Controller")).LifestyleTransient()


                );
            
        }
    }
}