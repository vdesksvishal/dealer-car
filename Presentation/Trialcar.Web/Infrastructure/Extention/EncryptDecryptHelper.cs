﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Trialcar.Web
{
    public static class EncryptDecryptHelper
    {
        //Here key is of 128 bit  
        //Key should be either of 128 bit or of 192 bit 
        public static string myKey = "cars-3hn8-sqoy19";


        public static string encrypt(string encryptString)
        {
            string encryptKey = string.Empty;
            try
            {
                byte[] inputArray = Encoding.UTF8.GetBytes(encryptString);
                TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
                tripleDES.Key = Encoding.UTF8.GetBytes(myKey);
                tripleDES.Mode = CipherMode.ECB;
                tripleDES.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = tripleDES.CreateEncryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
                tripleDES.Clear();
                encryptKey = Base64UrlEncode(Convert.ToBase64String(resultArray, 0, resultArray.Length));
                return encryptKey;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return encryptKey;
            }
        }

        public static string encrypt(int encryptString)
        {
            string encryptKey = string.Empty;
            try
            {
                encryptKey = Convert.ToString(encryptString);
                byte[] inputArray = Encoding.UTF8.GetBytes(encryptKey);
                TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
                tripleDES.Key = Encoding.UTF8.GetBytes(myKey);
                tripleDES.Mode = CipherMode.ECB;
                tripleDES.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = tripleDES.CreateEncryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
                tripleDES.Clear();
                encryptKey = Base64UrlEncode(Convert.ToBase64String(resultArray, 0, resultArray.Length));
                return encryptKey;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return encryptKey;
            }
        }

        //public static string encrypt(int encryptString)
        //{
        //    string encryptKey = Convert.ToString(encryptString);
        //    try
        //    {

        //        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(encryptKey);
        //        encryptKey = System.Convert.ToBase64String(plainTextBytes);
        //        return encryptKey;
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        return encryptKey;
        //    }
        //}

        public static string Decrypt(string decryptString)
        {
            string decryptKey = string.Empty;
            try
            {
                decryptString = Base64UrlDecode(decryptString);
                byte[] inputArray = Convert.FromBase64String(decryptString);
                TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
                tripleDES.Key = UTF8Encoding.UTF8.GetBytes(myKey);
                tripleDES.Mode = CipherMode.ECB;
                tripleDES.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = tripleDES.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
                tripleDES.Clear();
                decryptKey = UTF8Encoding.UTF8.GetString(resultArray);
                return decryptKey;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return decryptKey;
            }
        }

        public static string Base64UrlEncode(this string value)
        {
            var bytes = Encoding.UTF8.GetBytes(value);
            var s = Convert.ToBase64String(bytes); // Regular base64 encoder
            s = s.Split('=')[0]; // Remove any trailing '='s
            s = s.Replace('+', '-'); // 62nd char of encoding
            s = s.Replace('/', '_'); // 63rd char of encoding
            return s;
        }

        public static string Base64UrlDecode(this string value)
        {
            var s = value;
            s = s.Replace('-', '+'); // 62nd char of encoding
            s = s.Replace('_', '/'); // 63rd char of encoding
            switch (s.Length % 4) // Pad with trailing '='s
            {
                case 0:
                    break; // No pad chars in this case
                case 2:
                    s += "==";
                    break; // Two pad chars
                case 3:
                    s += "=";
                    break; // One pad char
                default:
                    throw new Exception("Illegal base64 url string!");
            }

            var bytes = Convert.FromBase64String(s); // Standard base64 decoder
            return Encoding.UTF8.GetString(bytes);
        }
    }
}