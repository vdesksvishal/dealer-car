﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Incite.Core.Domain;
using Incite.Data.Repositories;
using Incite.Services.EzcmdHelper;
using Newtonsoft.Json;

namespace Trialcar.Web
{
    public static class AppExtentionHelper
    {
        public static AppUsers GetUserById(this int userId)
        {
            AppUsers appUsers = new AppUsers();
            if (userId != 0)
            {
                var _appUserRepo = Incite.Dependency.PluginLocator.Resolve<IAppUsersRepository>();
                appUsers = _appUserRepo.Get(userId);
            }
            return appUsers;
        }

        public static DealerCustomerCreation GetDelarCustomerAccount(this int userId)
        {
            DealerCustomerCreation result = null;
            if (userId != 0)
            {
                var _delarcustomeraccount = Incite.Dependency.PluginLocator.Resolve<IDealerCustomerCreationRepository>();
                result = _delarcustomeraccount.Get(x => x.User == userId.GetUserById());
            }
            return result;
        }

        public static DealerConnectCreation GetDelarConnectAccount(this int userId)
        {
            DealerConnectCreation result = null;
            if (userId != 0)
            {
                var _delarconnectaccount = Incite.Dependency.PluginLocator.Resolve<IDealerConnectCreationRepository>();
                result = _delarconnectaccount.Get(x => x.User == userId.GetUserById());
            }
            return result;
        }
        public static UserProfile GetUserProfileByUserId(this int userid)
        {
            UserProfile userProfile = new UserProfile();
            if (userid != 0)
            {
                var user = userid.GetUserById();
                if (user != null)
                {
                    var _userprofileRepository = Incite.Dependency.PluginLocator.Resolve<IUserProfileRepository>();
                    userProfile = _userprofileRepository.Get(x => x.User == user);
                }
            }
            return userProfile;

        }
        public static string GetMaileDistance(this int currentUserid,int caruserid )
        {
            string result = string.Empty;
            var _mileFilterDataRepository = Incite.Dependency.PluginLocator.Resolve<IMileFilterDataRepository>();
            var caruserprofile= GetUserProfileByUserId(caruserid);
            var userprofile = GetUserProfileByUserId(currentUserid);
            if (userprofile != null && caruserprofile!=null)
            {
                var all = _mileFilterDataRepository.GetAll(x => x.Zipcod == userprofile.Zipcode);
                foreach (var finzip in all.OrderBy(x => Convert.ToInt32(x.Mile)).ToList())
                {
                    List<MileFilterRecored> recode = JsonConvert.DeserializeObject<List<MileFilterRecored>>(Encoding.ASCII.GetString(finzip.Record));
                    var findrecoed = recode.FirstOrDefault(x => x.zipcode == caruserprofile.Zipcode);
                    if (findrecoed != null)
                    {
                        decimal convertdistance = 0;
                        int convertMile = 0;
                        if (decimal.TryParse(findrecoed.distance, out convertdistance))
                        {
                            if (int.TryParse(finzip.Mile, out convertMile))
                            {
                                if (convertdistance <= convertMile)
                                {
                                    result = convertdistance.ToString()+ " mi. from you";
                                }
                                
                            }
                        }
                        if(string.IsNullOrEmpty(result))
                        result = finzip.Mile + " mi. from you" ;
                        break;
                    }
                }
            }
            return result;
        }
    }
}
