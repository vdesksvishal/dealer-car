﻿using Incite.Core.Domain;
using Incite.Core.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Trialcar.Web.Infrastructure
{
    public class Authentication
    {
        internal static IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.Current.GetOwinContext().Authentication; }
        }
        public static void IdentitySignin(AppUsers appUserState, string providerKey = null, List<AppUsersRole> userRole = null, bool isPersistent = false)
        {
            try
            {
                var claims = new List<Claim>();

                // create *required* claims
                claims.Add(new Claim(ClaimTypes.NameIdentifier, appUserState.Id.ToString()));
                claims.Add(new Claim(ClaimTypes.Name, appUserState.UserName));
                if(userRole!=null)
                {
                    foreach (var role in userRole)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, role.RoleName));
                    }

                }
                claims.Add(new Claim(ClaimTypes.Email, appUserState.Email));
                // serialized AppUserState object
                claims.Add(new Claim("userState", appUserState.ToString()));
                var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

                // add to user here!
                AuthenticationManager.SignIn(new AuthenticationProperties()
                {
                    AllowRefresh = true,
                    IsPersistent = isPersistent,
                    ExpiresUtc = DateTime.UtcNow.AddDays(7)
                }, identity);
            }
            catch (Exception ex)
            {

            }
        }
        public static void IdentitySignout()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie,
                DefaultAuthenticationTypes.ExternalCookie);
        }

        public static Task<ExternalLoginInfo> Getlogininfo()
        {
            return AuthenticationManager.GetExternalLoginInfoAsync();
        }

        public static int? CurrentUserId
        {
            get
            {
                int UserId = 0;
                try
                {
                    var user = HttpContext.Current.User.Identity as ClaimsIdentity;
                    if (user != null)
                    {
                        var currentid = user.GetUserId();
                        int.TryParse(currentid.ToString(), out UserId);
                    }
                    else
                    {
                        Authentication.IdentitySignout();
                        OverlayManager.cleareOverlay();
                    }
                }
                catch (Exception)
                {
                }
                return UserId;
            }
        }
    }
}