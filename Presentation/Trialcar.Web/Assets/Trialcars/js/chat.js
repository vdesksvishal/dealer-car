﻿/// <reference path="../../Scripts/knockout-2.2.0.debug.js" />
/// <reference path="../../Scripts/jquery-3.3.1.intellisense.js" />
/// <reference path="../../Scripts/jquery.signalR-1.0.0-rc1.js" />

(function () {

    function Message(from, msg, isPrivate) {
        this.from = ko.observable(from);
        this.message = ko.observable(msg);
        this.isPrivate = ko.observable(isPrivate);
        //this.isReceiver = ko.observable(isReceiver);
        //this.isNewUser = ko.observable(isNewUser);
    }

    function User(name) {

        var self = this;

        self.name = ko.observable(name);
        self.isPrivateChatUser = ko.observable(false);
        self.setAsPrivateChat = function (user) {
            populateMessagesByName(user.name());
            viewModel.privateChatUser(user.name());
            viewModel.isInPrivateChat(true);
            $.each(viewModel.users(), function (i, user) {
                user.isPrivateChatUser(false);
            });
            self.isPrivateChatUser(true);
        };
    }

    var viewModel = {
        messages: ko.observableArray([]),
        users: ko.observableArray([]),
        isInPrivateChat: ko.observable(false),
        privateChatUser: ko.observable(),
        exitFromPrivateChat: function () {

            viewModel.isInPrivateChat(false);
            viewModel.privateChatUser(null);
            $.each(viewModel.users(), function (i, user) {
                user.isPrivateChatUser(false);
            });
        }
    };

    $(function () {

        var chatHub = $.connection.chatHub,
            loginHub = $.connection.login,
            $sendBtn = $('#msg_submit_btn'),
            $msgTxt = $('#msg_txt_usr');

        // turn the logging on for demo purposes
        $.connection.hub.logging = true;

        /*chatHub.client.hubPopulate = function (dealer_id) {
            alert(dealer_id);
        };*/

        chatHub.client.received = function (message) {
            //alert(viewModel.isInPrivateChat());
            window.console.log("isInPrivateChat: " + viewModel.isInPrivateChat() + "|isReceiver: " + message.isReceiver + "|sender: " + message.sender + "|sender_uname: " + $("#sender_uname").val()  );
            setTimeout(function () {
                //alert(1111);
                //if (viewModel.isInPrivateChat()) {
                    var senderText = "";
                    var date = new Date();
                    var time = date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
                    
                    if (message.isReceiver && message.sender === $("#sender_uname").val()) {

                        var usrImgDiv;
                        if ($("#msg_content_div div.msg_top_div:last").hasClass("msg_right_div")) {
                            var imgSrc = $("#sender_img_src").val();
                            var img = "";
                            if (imgSrc !== "") {
                                img = $("<img>").attr("alt", "User");
                                img.attr("src", imgSrc);
                                img.addClass("user_image");
                                //img.insertBefore("span[name = '" + uname + "']");                       
                            } else {
                                var alt_txt = $("#sender_alt_txt").val();
                                img = $("<div></div>").text(alt_txt).addClass("alt_usr_img");
                            }
                            var usrImgInnerDiv = $("<div></div>").addClass("disp_inline");
                            usrImgDiv = $("<div></div>").addClass("msg_img_div").append(img).append(usrImgInnerDiv);

                        } else {

                            usrImgDiv = $("<div>&nbsp;</div>").addClass("float_left").addClass("width50");
                        }

                        var dispInlineDiv = $("<div></div>").addClass("disp_inline");
                        if ($("#usrTimeTo").val() !== time || $("#msg_content_div div.msg_top_div:last").hasClass("msg_right_div")) {
                            senderText = message.sender + ", " + time;
                            var innerDiv = $("<div></div>").text(senderText);
                            dispInlineDiv.append(innerDiv);
                            $("#usrTimeTo").val(time);
                        }

                        var innerDiv2 = $("<div></div>").text(message.message).addClass("msg_div");
                        dispInlineDiv.append(innerDiv2);
                        var upperDiv = $("<div></div>").addClass("msg_left_div msg_top_div").append(usrImgDiv).append(dispInlineDiv);
                        
                        $("#msg_content_div").append(upperDiv);

                    } else if (!message.isReceiver) {

                        var upperDiv = $("<div></div>").addClass("msg_right_div msg_top_div");
                        if ($("#usrTimeFrom").val() !== time || $("#msg_content_div div.msg_top_div:last").hasClass("msg_left_div")) {
                            var innerDiv = $("<div></div>").text(time).addClass("float_right");
                            upperDiv.append(innerDiv);
                            $("#usrTimeFrom").val(time);
                        }

                        var innerDiv2 = $("<div></div>").text(message.message).addClass("msg_div");
                        upperDiv.append(innerDiv2);

                        $("#msg_content_div").append(upperDiv);
                    }

                //}

                if (message.isReceiver && message.isNewUser && $("span[name='" + message.sender + "']").text() === "") {

                    if ($("#users_listing_div").prev("h5").text() !== "") {
                        //window.location.href = "/Dealer/Messages";
                        $("#users_listing_div").prev("h5").remove();
                    }
                    //else {
                    var childDiv = $("<span></span>").text(" " + message.sender).attr("name", message.sender);
                    var parentDiv = $("<div></div>").addClass("message_user_div").attr("id", "user_div_" + message.sender);
                    parentDiv.append(childDiv);
                    parentDiv.click(function (event) {

                        viewModel.privateChatUser(message.sender);
                        viewModel.isInPrivateChat(true);
                        //alert(viewModel.isInPrivateChat());
                        populateMessagesByName(message.sender, this);
                        event.preventDefault();
                    });

                    $("#users_listing_div").append(parentDiv);
                    insertUserActiveImage(message.sender);
                    //}

                }
                //populateMessagesByName(message.sender);

                //viewModel.messages.push(new Message(message.sender, message.message, message.isPrivate));

            }, 2500);

            //
        };

        chatHub.client.userConnected = function (username) {
            //alert('userConnected');
            //viewModel.users.push(new User(username));
            //$('<img id="img_user_active_' + username + '"  src="/Assets/images/img_user_active.jpg" style="height:20px;width:20px">').insertBefore("span[name = '" + username + "']");
            insertUserActiveImage(username);
            if ($("#user_div_" + username).hasClass("user_selected")) {
                viewModel.privateChatUser(username);
                viewModel.isInPrivateChat(true);
            }
            $('#user_div_' + username).click(function (event) {
                //alert('userconnected event');
                viewModel.privateChatUser(username);
                viewModel.isInPrivateChat(true);
                //alert(viewModel.isInPrivateChat());
                //populateMessagesByName(username, this);
                event.preventDefault();
            });
        };

        chatHub.client.userDisconnected = function (username) {
            if ($("#user_div_" + username).hasClass("user_selected")) {
                viewModel.privateChatUser(null);
                viewModel.isInPrivateChat(false);
            }
            $('#user_div_' + username).click(function (event) {
                //alert('userDisconnected event');
                viewModel.privateChatUser(null);
                viewModel.isInPrivateChat(false);
                //alert(viewModel.isInPrivateChat());
                //populateMessagesByName(username, this);
                event.preventDefault();
            });
            //viewModel.users.pop(new User(username));
            /*if (viewModel.isInPrivateChat() && viewModel.privateChatUser() === username) {
                alert('userDisconnected condition');
                viewModel.isInPrivateChat(false);
                alert(viewModel.isInPrivateChat());
                viewModel.privateChatUser(null);
            }*/
            $("#img_user_active_" + username).remove();
        };

        // $.connection.hub.starting(callback)
        // there is also $.connection.hub.(received|stateChanged|error|disconnected|connectionSlow|reconnected)

        // $($.connection.hub).bind($.signalR.events.onStart, callback)

        // $.connection.hub.error(function () {
        //     console.log("foo");
        // });

        startConnection();
        ko.applyBindings(viewModel);

        function startConnection() {

            $.connection.hub.start().done(function () {
                //alert('connection start');
                //toggleInputs(false);
                bindClickEvents();
                $msgTxt.focus();

                chatHub.server.getConnectedUsers().done(function (users) {
                    /*
                    /*
                    if (users[0]) {
                        $('#user_div_' + users[0]).addClass('user_selected');
                        populateMessagesByName(users[0]);
                        viewModel.privateChatUser(users[0]);
                        viewModel.isInPrivateChat(true);
                    }
                    */
                    $.each(users, function (i, username) {
                        //viewModel.users.push(new User(username));
                        //alert($("span[name = '" + username + "']").html());
                        //$('<img id="img_user_active_' + username + '"  src="/Assets/images/img_user_active.jpg" style="height:20px;width:20px">').insertBefore("span[name = '" + username + "']");
                        insertUserActiveImage(username);

                        $('#user_div_' + username).click(function (event) {
                            //alert('getConnectedUsers()');
                            viewModel.privateChatUser(username);
                            viewModel.isInPrivateChat(true);
                            //alert(viewModel.isInPrivateChat());
                            //populateMessagesByName(username, this);
                            event.preventDefault();
                        });
                    });
                });

            }).fail(function (err) {

                console.log(err);
            });
        }

        function insertUserActiveImage(uname) {
            //alert($("#img_user_active_" + uname).hasClass("user_active_img"));
            if ($("#img_user_active_" + uname).hasClass("user_active_img") === false) {
                /*var img = $("<img>").attr("id", "img_user_active_" + uname);
                img.attr("src", "/Assets/images/img_user_active.jpg");
                img.addClass("user_active_img");
                img.insertBefore("span[name = '" + uname + "']");*/
                var actIcon = $("<span>").attr("id", "img_user_active_" + uname).addClass("user_active_img");
                actIcon.insertBefore("span[name = '" + uname + "']");
            }
        }

        function bindClickEvents() {
            $('.message_user_div').click(function (event) {
                //alert('all user click');
                viewModel.privateChatUser(null);
                viewModel.isInPrivateChat(false);
                //alert(viewModel.isInPrivateChat());
                //$(this).
                //populateMessagesByName(username, this);
                event.preventDefault();
            });
            //alert('bindClickEvents : func');
            //alert($sendBtn.val());
            $msgTxt.keypress(function (e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code === 13) {
                    sendMessage();
                    e.preventDefault();
                }
            });

            $sendBtn.click(function (e) {
                sendMessage();
                e.preventDefault();
            });
        }

        function sendMessage() {
            //alert($("#msg_content_div").children().last().prev().prev().html());
            //alert($("#msg_content_div div.msg_top_div:last").hasClass("msg_right_div"));

            var msgValue = $msgTxt.val();
            if (msgValue !== null && msgValue.length > 0) {

                if (viewModel.isInPrivateChat()) {
                    messageSubmit(true);

                    //alert('sendmessage()');
                    chatHub.server.send(msgValue, viewModel.privateChatUser(), false).fail(function (err) {
                        console.log('Send method failed: ' + err);
                    });

                } else {

                    messageSubmit(false);

                    if ($('#fromAction').val() == 'carDetail') {

                        var uname = $('#toDealerUserName').val();

                        viewModel.privateChatUser(uname);
                        viewModel.isInPrivateChat(true);

                        chatHub.server.send(msgValue, viewModel.privateChatUser(), true).fail(function (err) {
                            console.log('Send method failed: ' + err);
                        });
                    }
                }
                /*else {
                    chatHub.server.send(msgValue).fail(function (err) {
                        console.log('Send method failed: ' + err);
                    });
                }*/
            }

            $msgTxt.val(null);
            $msgTxt.focus();
        }

        function toggleInputs(status) {

            $sendBtn.prop('disabled', status);
            $msgTxt.prop('disabled', status);
        }

    });
}());

function populateMessagesByName(uName, thisObj) {

    $.ajax({
        type: 'GET',
        url: '/Dealer/PopulateMessagesByName/' + uName,
        dataType: "html"

    }).done(function (result) {
        $("#msg_content_div").html(result);
        $(".message_content").addClass("msg_filled");
        $("#send_msg_frm_div").addClass("send_msg_div_show");
        $(".message_user_div").removeClass("user_selected");
        $(thisObj).addClass("user_selected");
    }).fail(function () {
        alert("Problem While Retrieving Messages");
    });
}

function populateMessages(dealerId, thisObj) {
    //setTimeout(function () {
        //alert('populateMessages(dealerId, thisObj)');
        $.ajax({
            type: 'GET',
            url: '/Dealer/PopulateMessages/' + dealerId,
            dataType: "html"

        }).done(function (result) {
            $("#msg_content_div").html(result);
            $(".message_content").addClass("msg_filled");
            $(".message_user_div").removeClass("user_selected");
            $(thisObj).addClass("user_selected");
            $("#send_msg_frm_div").addClass("send_msg_div_show");

        }).fail(function () {
            alert("Problem While Retrieving Messages");
        });
    //}, 1000);  
}