﻿
$(document).on('click', '#btnResetPassword', function (event) {
    event.preventDefault();

    var frm = $("#frmResetPassword");

    if (frm.valid()) {
        $.ajax(
            {
                url: "/Account/ResetPassword",
                type: "POST",
                data: frm.serialize(),
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                processData: false,
                success: function (data) {
                    if (data != undefined && data != null) {
                        if (data.status) {
                            debugger;   
                            frm[0].reset();
                            $(".dvdismiss").show();
                            $(".dvdismiss").html(data.message);
                            //$('#loginform').modal("show");
                            window.location.href = "/";
                        }
                        else {
                            frm[0].reset();
                            $(".dvdismiss").show();
                            $(".dvdismiss").html(data.message);
                            //$('#loginform').modal("show");

                        }
                    }
                },
                error: function (data) {
                    $(".dvdismiss").show();
                    $(".dvdismiss").html(data.message)
                }
            });
    }
});


