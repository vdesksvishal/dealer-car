﻿$(document).on('click', '#btnSubmit', function (event) {
    event.preventDefault();
    composeFormSubmit(event);
});

function composeFormSubmit(event) {
    event.preventDefault();
    var $captcha = $('#recaptcha'),
        response = grecaptcha.getResponse();

    if (response.length === 0) {
        $('.msg-error').text("CAPTCHA is mandatory");
        if (!$captcha.hasClass("error")) {
            $captcha.addClass("error");
            $("#validationRecaptcha").show();
            return;
        }
    } else {
        $('.msg-error').text('');
        $captcha.removeClass("error");
        $("#validationRecaptcha").hide();
    }

    var frm = $("#frmSignUp");
    if (frm.valid()) {
        var fd = new FormData();
        var file_data = $('input[type="file"]')[0].files;
        for (var i = 0; i < file_data.length; i++) {
            fd.append("file_" + i, file_data[i]);
        }
        var other_data = frm.serializeArray();
        $.each(other_data, function (key, input) {
            fd.append(input.name, input.value);
        });

        //$.ajax(
        //    {
        //        url: "/api/Accounts/Compose",
        //        method: "POST",
        //        data: fd,
        //        contentType: false,
        //        processData: false,
        //        success: function (data) {
        //            grecaptcha.reset();
        //            if (data != undefined && data != null) {
        //                if (data.status == true) {
        //                    $("#dvMessage").show();
        //                    $("#dvMessage>div>div").removeClass("alert-danger").addClass("alert-success");
        //                    $("#dvMessage>div>div").html(data.message); // Set Message from Dictionary.
        //                    ResetElement("ComposeForm");
        //                }
        //                else {
        //                    $("#dvMessage").show();
        //                    $("#dvMessage>div>div").removeClass("alert-info").addClass("alert-danger");
        //                    $("#dvMessage>div>div").html(data.message); // Set Message from Dictionary.
        //                    ResetElement("ComposeForm");
        //                }
        //            }
        //        },
        //        error: function (data) {
        //            ResetElement("ComposeForm");
        //            grecaptcha.reset();
        //        }
        //    });

    }
}