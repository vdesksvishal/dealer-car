﻿
$(document).on('click', '#btnSubmit', function (event) {
    event.preventDefault();
    var $captcha = $('#recaptcha'), response = grecaptcha.getResponse();

    if (response.length === 0) {
        $('.msg-error').text("CAPTCHA is mandatory");
        if (!$captcha.hasClass("error")) {
            $captcha.addClass("error");
        }
        $("#validationRecaptcha").show();
        return;
    } else {
        $('.msg-error').text('');
        $captcha.removeClass("error");
        $("#validationRecaptcha").hide();
    }

    var termconditioncheckbox = $('#TermsAccepted');
    var isTermConditionchecked = $('#TermsAccepted:checked').length;

    if (isTermConditionchecked === 0) {
        $('.msg-error').text("Please accept terms and condition");
        if (!termconditioncheckbox.hasClass("error")) {
            termconditioncheckbox.addClass("error");
        }
        $("#validationTermCondition").show();
        return;
    }
    else {
        $('.msg-error').text('');
        termconditioncheckbox.removeClass("error");
        $("#validationTermCondition").hide();
    }


    var frm = $("#frmSignUp");
    if (frm.valid()) {
        var fd = new FormData();
        var file_data = $('input[type="file"]')[0].files;
        for (var i = 0; i < file_data.length; i++) {
            fd.append("file_" + i, file_data[i]);
        }
        var other_data = frm.serializeArray();
        $.each(other_data, function (key, input) {
            fd.append(input.name, input.value);
        });

        $.ajax(
            {
                url: "/Account/Register",
                type: "POST",
                data: fd,
                contentType: false,
                processData: false,
                success: function (data) {
                    grecaptcha.reset();
                    if (data !== undefined && data !== null) {
                        if (data.status) {
                            frm[0].reset();
                            $(".dvdismiss").show();
                            $(".dvdismiss").html(data.message);
                            
                        }
                        else {
                            frm[0].reset();
                            $(".dvdismiss").show();
                            $(".dvdismiss").html(data.message)
                            
                        }
                    }
                },
                error: function (data) {
                    grecaptcha.reset();
                }
            });

    }
});


