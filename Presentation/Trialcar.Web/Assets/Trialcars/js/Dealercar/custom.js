﻿// DealerCar Add Form Scripts Start

function fndrpCarbrandChange(model, year, trimprop, _this) {
    var currentVal = $(_this).find('option:selected').val();
    if (model.length > 0) {
        var drpCarModel = $("#" + model);
        bindCarModel(currentVal, drpCarModel);
    }
    if (year.length > 0) {

        var drpmodelyear = $("#" + year);
        bindCarYear(currentVal, 0, drpmodelyear);
    }
    if (trimprop.length > 0) {
        var drptrimprop = $("#" + trimprop);
        bindCartrimProp(currentVal, 0, 0, drptrimprop);
    }

}

function fndrpCarModelChange(brand, year, trimprop, _this) {
    var drpCarbrand = $("#" + brand);
    var drpmodelyear = $("#" + year);
    var drptrimprop = $("#" + trimprop);

    var carbrand_val = drpCarbrand.find('option:selected').val();
    var currentVal = $(_this).find('option:selected').val();

    bindCarYear(carbrand_val, currentVal, drpmodelyear);
    bindCartrimProp(carbrand_val, currentVal, 0, drptrimprop);
}

function fndrpCarYearChange(brand, model, year, trimprop, _this) {
    var drpCarbrand = $("#" + brand);
    var drpCarModel = $("#" + model);
    var drpmodelyear = $("#" + year);

    var drptrimprop = $("#" + trimprop);

    var carbrand_val = drpCarbrand.find('option:selected').val();
    var carmodel_val = drpCarModel.find('option:selected').val();
    var currentVal = $(_this).find('option:selected').val();

    bindCartrimProp(carbrand_val, carmodel_val, currentVal, drptrimprop);

}

function fndrpCarTrimPropChange(fueltype, drivetrain, transmision,  _this) {
    var drpfueltype = $("#" + fueltype);
    var drpdrivetrain = $("#" + drivetrain);
    var drptransmision = $("#" + transmision);
  
    var currentVal = $(_this).find('option:selected').val();

    bindotherfields(currentVal);

}

function bindCarModel(carbrand_val, drpCarModel) {
    $.ajax({
        type: "GET",
        async: false,
        url: '/api/CommonAPI/GetCarModel/' + carbrand_val, // we are calling json method
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (Data) {
            loadDropdownonSuccess(Data.Data.modelList, drpCarModel, '', 'Select Model');
        },
        failure: function () {
            loadDropdownonFailure(drpCarModel);
        }
    });
}

function bindCarYear(carbrand_val, carmodel_val, drpmodelyear) {
    $.ajax({
        type: "GET",
        async: false,
        url: '/api/CommonAPI/GetCarYear/' + carbrand_val + '/' + carmodel_val,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (Data) {
            loadDropdownonSuccess(Data.Data.modelList, drpmodelyear, '', 'Select Year');
        },
        failure: function () {
            loadDropdownonFailure(drpmodelyear);
        }
    });
}

function bindCartrimProp(carbrand_val, carmodel_val, caryear_val, drptrimprop) {
    $.ajax({
        type: "GET",
        async: false,
        url: '/api/CommonAPI/GetCarTrimProp/' + carbrand_val + '/' + carmodel_val + '/' + caryear_val,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (Data) {
            loadDropdownonSuccess(Data.Data.modelList, drptrimprop, 'SelectListItem', 'Select Trim')
        },
        failure: function () {
            loadDropdownonFailure(drp);
        }
    });
}

function loadDropdownonSuccess(Data, drp, drptype = '', selecttext = "Select") {
    drp.html('');
    drp.append($('<option value>' + selecttext + '</option>'));
    if (drptype.length > 0) {
        $.each(Data, function () {
            drp.append($("<option     />").val(this.Value).text(this.Text));
        });
    }
    else {
        $.each(Data, function () {
            drp.append($("<option     />").val(this.Id).text(this.Name));
        });
    }
    ResetBeforeTrimChosen();
}

function loadDropdownonFailure(drp, selecttext = "Select") {
    drp.html('');
    drp.append($('<option value>' + selecttext + '</option>'));
    ResetBeforeTrimChosen();
}

function bindotherfields(selectedval) {
    $.ajax({
        type: "GET",
        async: false,
        url: '/api/CommonAPI/Getpropertybytrim/' + selectedval,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (Data) {
            if (Data !== 'undefined' && Data !== null) {
             
                FillUpAfterTrimChosen(Data.Data.model);
            }
            else {
                ResetBeforeTrimChosen();
            }
        },
        failure: function () {
            ResetBeforeTrimChosen();
        }
    });
}

function FillUpAfterTrimChosen(values) {
    $("input[name='priceperday']").val(values.Price);

    $("#txtfueltype").val(values.FuelTypeName);
    $("#hdnFueltypeId").val(values.FuelTypeId);

    $("#txttransmision").val(values.TransmissionName);
    $("#txtenginename").val(values.EngineName);
    $("#hdnTransmissionId").val(values.TransmissionId);

    $("#txtdrivetrain").val(values.DriveTypeName);
    $("#hdndrivetrainId").val(values.DriveTypeId);
    $("#hdnCarDetailId").val(values.Id);
}

function carlistsorting(This) {
    var url = window.location.pathname;
    var parms = window.location.search;
    var selectedvalue = $(This).val();
    var redirecturl = "";
    if (parms !== undefined && parms !== "") {
        redirecturl = parms.replace("&Sb=1", "").replace("&Sb=2", "").replace("&Sb=3", "").replace("&Sb=4", "").replace("&Sb=0", "");
        redirecturl = redirecturl.replace("?Sb=1", "").replace("?Sb=2", "").replace("?Sb=3", "").replace("?Sb=4", "").replace("?Sb=0", "");
        if (redirecturl.indexOf("?") === -1)
            redirecturl = url + redirecturl + "?Sb=" + selectedvalue;
        else
            redirecturl = url + redirecturl + "&Sb=" + selectedvalue;
    }
    else
        redirecturl = url + "?Sb=" + selectedvalue;
    if (selectedvalue !== undefined && selectedvalue !== "") {
        location.href = redirecturl;
    }
}

function ResetBeforeTrimChosen() {
    $("input[name='priceperday']").val('');

    $("#txtfueltype").val('');
    $("#hdnFueltypeId").val('');

    $("#txttransmision").val('');
    $("#txtenginename").val('');
    $("#hdnTransmissionId").val('');

    $("#txtdrivetrain").val('');
    $("#hdndrivetrainId").val('');
    $("#hdnCarDetailId").val('');
}

function messageSubmit(isActiveUser) {    //alert('msgsubmit');    var msgValue = $('#msg_txt_usr').val();    if (msgValue !== null && msgValue.length > 0 && msgValue.trim() !== "") {        var formData = $('#frmSendMessage').serialize();        $.ajax({            type: "POST",            url: "/DealerCar/SendMessage",            data: formData,            dataType: "json"        }).done(function (data) {            if (!isActiveUser) {                if (data.from === "PopulateMessages") {                    var date = new Date();                    var time = date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });                    var innerDiv = $("<div></div>").text(time).addClass("float_right");                    var innerDiv2 = $("<div></div>").text(data.message).addClass("msg_div");                    var upperDiv = $("<div></div>").addClass("msg_right_div").append(innerDiv).append(innerDiv2);                    $("#msg_content_div").append(upperDiv);                } else if (data.msg) {                    $('#send_msg_result').addClass("bg-success").removeClass("text-danger").html(data.msg);                    //$('<span class="bg-success">' + data.msg + '</span>').insertBefore('#frmSendMessage');                } else if (data.err) {                    $('#send_msg_result').addClass("text-danger").removeClass("bg-success").html(data.err);                    //$('<span class="text-danger">' + data.err + '</span>').insertBefore('#frmSendMessage');                }            }        }).fail(function () {            $('#send_msg_result').addClass("text-danger").removeClass("bg-success").html("Problem While Send Message");            //$('<span class="text-danger">Problem While Send Message.</span>').insertBefore('#frmSendMessage');        });    }    else if ($('#fromAction').val() === "carDetail") {        $('#send_msg_result').addClass("text-danger").removeClass("bg-success").html("Please Enter Message");    }    $('#msg_txt_usr').val(null);    $('#msg_txt_usr').focus();    /*var dealerId = $("#toDealerId").val();    chatHub.server.hubPopulateMessages(dealerId).fail(function (err) {        console.log('Send method failed: ' + err);    });*/}

function buyNowCar(cId) {

    $.ajax({
        type: 'GET',
        url: '/CarBuy/' + cId,
        dataType: "html"

    }).done(function (result) {
        $('#CarBuyHoldOutput').html(result);
        $('#BuyNowHoldNowDetailForm').modal('show');
    }).fail(function () {
        alert("Car buy process failed");
    });
}

function holdNowCar(cId) {

    $.ajax({
        type: 'GET',
        url: '/CarOnHold/' + cId,
        dataType: "html"

    }).done(function (result) {
        $('#CarBuyHoldOutput').html(result);
        $('#BuyNowHoldNowDetailForm').modal('show');
    }).fail(function () {
        alert("Car hold process failed");
    });
}




function soldNowCar(cId) {
    debugger;
    if (confirm("Are you sure you want to sold this car?")) {
        $.ajax(
            {
                url: "/SoldCar/" + cId,
                type: "GET",
                success: function (data) {
                    if (data != undefined && data != null) {
                        if (data.IsSuccess) {
                            alert("Car sold successfully");
                            window.location.href = window.location.href;
                        }
                        else {

                            alert("Car sold process failed");
                        }
                    }
                },
                error: function (data) {
                    debugger;
                    alert("Car sold process failed");
                }
            });
    }
}

function deleteCar(cId) {
    debugger;
    if (confirm("Are you sure you want to delete this car?")) {
        $.ajax(
            {
                url: "/SoldCar/" + cId,
                type: "GET",
                success: function (data) {
                    if (data != undefined && data != null) {
                        if (data.IsSuccess) {
                            alert("Car has been deleted successfully");
                            window.location.href = window.location.href;
                        }
                        else {

                            alert("Delete process failed");
                        }
                    }
                },
                error: function (data) {
                    debugger;
                    alert("Delete process failed");
                }
            });
    }
}
// DealerCar Add Form Scripts End