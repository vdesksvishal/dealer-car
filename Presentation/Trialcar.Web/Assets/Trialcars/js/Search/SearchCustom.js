﻿
$(function () {
    $('#drpCarbrand').trigger('change');
    if (DealerCarViewModel.CB > 0) {
        $('#drpCarModel').val(DealerCarViewModel.CM);
    }

    destroyCurrentModelObject();
});

function destroyCurrentModelObject() {
    delete(DealerCarViewModel);
}
