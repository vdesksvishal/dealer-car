﻿$(document).ready(function () {

    // Added by mayur
    //  to hide rating text for fxss plugin
    //$("div.fxss_rate_text").hide();

    var currRating = $('#dealerCarRating').val();
    var ul_stars = $('ul#stars').children('li.star');
    for (i = 0; i < currRating; i++) {
        $(ul_stars[i]).addClass('selected');
    }
    $('div.commentReplyBlock').hide();

    /* 1. Visualizing things on Hover - See next part for action on click */
    $('#stars li').on('mouseover', function () {
        var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

        // Now highlight all the stars that's not after the current hovered star
        $(this).parent().children('li.star').each(function (e) {
            if (e < onStar) {
                $(this).addClass('hover');
            }
            else {
                $(this).removeClass('hover');
            }
        });

    }).on('mouseout', function () {
        $(this).parent().children('li.star').each(function (e) {
            $(this).removeClass('hover');
        });
    });


    /* 2. Action to perform on click */
    $('#stars li').on('click', function () {
        var onStar = parseInt($(this).data('value'), 10); // The star currently selected
        var stars = $(this).parent().children('li.star');

        for (i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass('selected');
        }

        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass('selected');
        }

        // added by mayur
        var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
        $('#dealerCarRating').val(ratingValue);
        
        // JUST RESPONSE (Not needed)
        /*
        var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
        var msg = "";
        if (ratingValue > 1) {
            msg = "Thanks! You rated this " + ratingValue + " stars.";
        }
        else {
            msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
        }
        responseMessage(msg);
        */
    });

    $('a.replyLink').on('click', function () {
        $("div.commentReplyBlock").hide();
        $("a.replyLink").show();
        $(this).hide();        
        $(this).next("div.commentReplyBlock").show();
    });
});

function addCommentSuccess(data) {

    if ($("span#noRatingMsg").text()) {
        $("span#noRatingMsg").hide();
    }
    var msgSpan = $(this).next("span.CarCommentResult");
    msgSpan.addClass(data.cls);
    msgSpan.text(data.msg);

    if (data.avgRatings) {
        $(".rating-container").rate({
            length: 5,
            value: data.avgRatings,
            readonly: true,
            size: '30px',
            text: false,
        });
    }

    if (data.commentId) {
        $(this).children("input[name='Id']").val(data.commentId);
    }

    if (data.totalComments) {
        $("#totalCommentsContainer").html(data.totalComments);
    }
}

/*
function responseMessage(msg) {
    $('.success-box').fadeIn(200);
    $('.success-box div.text-message').html("<span>" + msg + "</span>");
}
*/