﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Trialcar.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);
            routes.MapRoute("HomeIndex", "", defaults: new { controller = "Home", action = "Index" ,Login=UrlParameter.Optional });
            routes.MapRoute("HomeLogin", "Login/", defaults: new { controller = "Home", action = "Index", LogOut = true });
            routes.MapRoute("About", "About/", defaults: new { controller = "Home", action = "About" });
            routes.MapRoute("Contact", "Contact/", defaults: new { controller = "Home", action = "Contact" });

            #region Acccount

            routes.MapRoute("Register", "Account/Register/", defaults: new { controller = "Account", action = "Register" });
            routes.MapRoute("Login", "Account/Login/", defaults: new { controller = "Account", action = "Login" });
            routes.MapRoute("Logout", "Account/Logout/", defaults: new { controller = "Account", action = "Logout" });
            routes.MapRoute("ConfirmEmail", "Account/ConfirmEmail/", defaults: new { controller = "Account", action = "ConfirmEmail" });
            routes.MapRoute("DealerSetting", "Account/UpdateSettings/", defaults: new { controller = "Account", action = "UpdateSettings" });
            routes.MapRoute("DealerProfile", "Account/UpdateProfile/", defaults: new { controller = "Account", action = "UpdateProfile" });
            routes.MapRoute("UpdatePassword", "Account/UpdatePassword/", defaults: new { controller = "Account", action = "UpdatePassword" });
            routes.MapRoute("ForgotPassword", "Account/ForgotPassword/", defaults: new { controller = "Account", action = "ForgotPassword" });
            routes.MapRoute("ResetPassword", "Account/ResetPassword/{id}", defaults: new { controller = "Account", action = "ResetPassword", id = UrlParameter.Optional });
            routes.MapRoute("UserPaymentSourceList", "Account/UserPaymentSourceList/", defaults: new { controller = "Account", action = "UserPaymentSourceList" });
            routes.MapRoute("VerifyBankAccount", "Account/VerifyBankAccount/", defaults: new { controller = "Account", action = "VerifyBankAccount" });

            routes.MapRoute("UserConnectSourceList", "Account/UserConnectSourceList/", defaults: new { controller = "Account", action = "UserConnectSourceList" });
            routes.MapRoute("AddConnectAccounts", "Account/AddConnectAccounts/", defaults: new { controller = "Account", action = "AddConnectAccounts" });
            #endregion

            #region General Car Search
            routes.MapRoute("FilterSearch", "Home/MiniSearch/", defaults: new { controller = "Home", action = "MiniSearch" });
            routes.MapRoute("FilterSidebar", "Search/MiniSearch/", defaults: new { controller = "Search", action = "MiniSearch" });
            routes.MapRoute("Searchresult", "Search/", defaults: new { controller = "Search", action = "SearchResult" });
            routes.MapRoute("HomePageFilter", "Search/HomeFilter/", defaults: new { controller = "Search", action = "HomeFilter" });
            routes.MapRoute("SearchPageLeftFilter", "Search/SearchLeftFilter/", defaults: new { controller = "Search", action = "SearchLeftFilter" });
            routes.MapRoute("MygarageLeftFilter", "Search/MygarageLeftFilter/", defaults: new { controller = "Search", action = "MygarageLeftFilter" });
            routes.MapRoute("CarlistSortBy", "Search/CarlistSortBy/", defaults: new { controller = "Search", action = "CarlistSortBy" });

            #endregion

            #region Dealer Car
            routes.MapRoute("MyGarage", "mygarage/", defaults: new { controller = "DealerCar", action = "Index" });
            routes.MapRoute("MySoldCar", "mysoldcar/", defaults: new { controller = "DealerCar", action = "DealerSoldCar" });
            routes.MapRoute("AddCarInGarage", "AddCarInGarage/", defaults: new { controller = "DealerCar", action = "AddUpdateCar" });
            routes.MapRoute("UpdateCarOfGarage", "UpdateCarOfGarage/{Cid}", defaults: new { controller = "DealerCar", action = "AddUpdateCar" , Cid =UrlParameter.Optional});

            routes.MapRoute("DealerCarCreate", "DealerCar/Create/", defaults: new { controller = "DealerCar", action = "Create" });
            routes.MapRoute("DuplicationCheckForVIN", "DealerCar/CheckForDuplication", defaults: new { controller = "DealerCar", action = "CheckForDuplication" });
            routes.MapRoute("LeftSideBar", "DealerCar/MiniSearch/", defaults: new { controller = "DealerCar", action = "MiniSearch" });
            routes.MapRoute("DealerCarUpdate", "DealerCar/AddUpdateCar/{Id}", defaults: new { controller = "DealerCar", action = "AddUpdateCar", Id = UrlParameter.Optional });
            routes.MapRoute("DealerCarDetail", "CarDetail/{CId}", defaults: new { controller = "DealerCar", action = "CarDetail", CId = UrlParameter.Optional });
            routes.MapRoute("AddUpdateCarComment", "DealerCar/AddUpdateComment", defaults: new { controller = "DealerCar", action = "AddUpdateCarComment" });
            routes.MapRoute("AddUpdateCarCommentReply", "DealerCar/AddUpdateCommentReply", defaults: new { controller = "DealerCar", action = "AddUpdateCarCommentReply" });
            routes.MapRoute("CarBuy", "CarBuy/{CId}", defaults: new { controller = "DealerCar", action = "CarBuy", Id=UrlParameter.Optional });
            routes.MapRoute("CarOnHold", "CarOnHold/{CId}", defaults: new { controller = "DealerCar", action = "CarOnHold", Id = UrlParameter.Optional });
            routes.MapRoute("SoldCar", "SoldCar/{CId}", defaults: new { controller = "DealerCar", action = "SoldCar", Id = UrlParameter.Optional });
            routes.MapRoute("RemoveCarToCompare", "DealerCar/AddToCompare", defaults: new { controller = "DealerCar", action = "AddToCompare" });
            routes.MapRoute("AddCarToCompare", "DealerCar/RemoveToCompare", defaults: new { controller = "DealerCar", action = "RemoveToCompare" });
            routes.MapRoute("CampareCar", "DealerCar/Compare", defaults: new { controller = "DealerCar", action = "Compare" });
            routes.MapRoute("MessageDealer", "DealerCar/SendMessage", defaults: new { controller = "DealerCar", action = "SendMessage" });
            routes.MapRoute("DealerMessages", "Dealer/Messages", defaults: new { controller = "DealerCar", action = "Messages" });
            routes.MapRoute("DealerPopulateMessages", "Dealer/PopulateMessages/{dealerId}", defaults: new { controller = "DealerCar", action = "PopulateMessages" });
            routes.MapRoute("DealerPopulateMessagesByName", "Dealer/PopulateMessagesByName/{userName}", defaults: new { controller = "DealerCar", action = "PopulateMessagesByName" });
            routes.MapRoute("DeleteCarImages", "DealerCar/DeleteCarImage", defaults: new { controller = "DealerCar", action = "DeleteCarImage" });
            routes.MapRoute("BuyNowHoldNowDetail", "Dealer/BuyNowHoldNowDetail", defaults: new { controller = "DealerCar", action = "BuyNowHoldNowDetail" });
            routes.MapRoute("DealerDetail", "Dealer/Details/{dealerId}", defaults: new { controller = "DealerCar", action = "DealerDetail" });
            #endregion

            #region Car Listing
            routes.MapRoute("CarListing", "Shared/CarListing/", defaults: new { controller = "Shared", action = "CarListing" });
            #endregion

            routes.MapRoute("AllcontentPage", "{*Pagename}", defaults: new { controller = "ContentPage", action = "Index", id = UrlParameter.Optional });

        }
    }
}
