﻿using System.Web;
using System.Web.Optimization;

namespace Trialcar.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Assets/Scripts/jquery-{version}.js"
                        //, "~/Assets/Scripts/jquery.unobtrusive-ajax.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Assets/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Assets/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Assets/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/Defaultcss").Include(
                      "~/Assets/Content/bootstrap.css",
                      "~/Assets/Content/site.css"));

            #region TrialCars
            bundles.Add(new ScriptBundle("~/bundles/TrialCarsScripts").Include(
                       "~/Assets/Trialcars/js/interface.js",
                       "~/Assets/Trialcars/switcher/js/switcher.js",
                       "~/Assets/Trialcars/js/bootstrap-slider.min.js",
                       "~/Assets/Trialcars/js/slick.min.js",
                       "~/Assets/Trialcars/js/owl.carousel.min.js"));

            bundles.Add(new StyleBundle("~/Content/TrialCarscss").Include(
                      "~/Assets/Content/bootstrap.css",
                      "~/Assets/Trialcars/css/style.css",
                      "~/Assets/Trialcars/css/owl.carousel.css",
                      "~/Assets/Trialcars/css/owl.transitions.css",
                      "~/Assets/Trialcars/css/slick.css",
                      "~/Assets/Trialcars/css/bootstrap-slider.min.css",
                      "~/Assets/Trialcars/css/font-awesome.min.css",
                      "~/Assets/Trialcars/switcher/css/switcher.css",
                      "~/Assets/Trialcars/switcher/css/blue.css"));

            #region Custom
            bundles.Add(new ScriptBundle("~/bundles/TrialCarsCustomScripts").Include(
                "~/Assets/Trialcars/js/Account/Register.js"
                , "~/Assets/Trialcars/js/Account/Login.js"
                , "~/Assets/Trialcars/js/Dealercar/custom.js"
                , "~/Assets/Trialcars/js/Account/UpdateProfile.js"
                , "~/Assets/Trialcars/js/Account/UpdatePassword.js"
                , "~/Assets/Trialcars/js/Account/ForgotPassword.js"
                , "~/Assets/Trialcars/js/Account/ResetPassword.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/ModelPopUpScripts").Include(
              "~/Assets/Trialcars/js/ModelPopUp/ModelPopUpReset.js"
              ));
            #endregion

            #endregion TrialCars

            #region Admin
            bundles.Add(new StyleBundle("~/Content/AdminStyle").Include(
                     "~/Assets/Content/bootstrap.css",
                     "~/Assets/Trialcars/css/font-awesome.min.css",
                     "~/Assets/Content/ionicons.min.css",
                     "~/Assets/Admin/css/AdminLTE.min.css",
                     "~/Assets/Admin/css/skins/_all-skins.min.css",
                     "~/Assets/Content/bootstrap-datepicker.min.css",
                     "~/Assets/Content/daterangepicker.css",
                     "~/Assets/Content/bootstrap3-wysihtml5.min.css"));
            bundles.Add(new ScriptBundle("~/bundles/AdminScripts").Include(
                       "~/Assets/Scripts/jquery-ui.min.js",
                       "~/Assets/Scripts/bootstrap.min.js",
                       "~/Assets/Scripts/jquery.sparkline.min.js",
                       "~/Assets/Scripts/moment.min.js",
                       "~/Assets/Scripts/daterangepicker.js",
                       "~/Assets/Scripts/bootstrap-datepicker.min.js",
                       "~/Assets/Scripts/bootstrap3-wysihtml5.all.min.js",
                       "~/Assets/Scripts/jquery.slimscroll.min.js",
                       "~/Assets/Scripts/fastclick.js",
                       "~/Assets/Admin/js/adminlte.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/AdminUserScripts").Include(
                       "~/Assets/Admin/js/User/UpdateUser.js"));

            #endregion Admin

        }
    }
}
