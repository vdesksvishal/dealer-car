﻿using Incite.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Trialcar.Web.Areas.Admin.Controllers
{
    [Authorize(Roles ="Admin")]
    //  [Authorize]
    public class ManageCarObjectController : Controller 
    {
        private readonly IImportExportService _importExportService;
        public ManageCarObjectController(IImportExportService importExportService)
        {
            _importExportService = importExportService;
        }
        // GET: Admin/ManageCarObject
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Index(HttpPostedFileBase file)
        {

            if (file.ContentLength > 0)
            {
                string _FileName = Path.GetFileName(file.FileName);
                string _path = Path.Combine(Server.MapPath("~/UploadedFiles/Temp"), _FileName);
                file.SaveAs(_path);
               var model= await _importExportService.ImportCarDetails(_path);
            }
            return View();
        } 
    }
}
