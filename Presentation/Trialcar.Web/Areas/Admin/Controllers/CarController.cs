﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Incite.Core.Domain;
using Incite.Services;

namespace Trialcar.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CarController : Controller
    {
        private readonly IDealerCarService _dealerCarService;

        public CarController(IDealerCarService dealerCarService)
        {
            _dealerCarService = dealerCarService;
        }
        // GET: Admin/DealerCar
        public ActionResult Index()
        {
            List<DealerCarDetails> lst = new List<DealerCarDetails>();
            lst = _dealerCarService.GetAllDealerCarListing().ToList();
            return View(lst);
        }
    }
}