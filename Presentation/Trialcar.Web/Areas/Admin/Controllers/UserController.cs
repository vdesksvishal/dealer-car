﻿using Incite.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Incite.Services.ViewModels;
using Incite.Core.Domain;
using PagedList;
using Incite.Core.Infrastructure;

namespace Trialcar.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {

        private readonly IUsersService _userService;
        public UserController(IUsersService userService)
        {
            _userService = userService;
        }
        // GET: Admin/User
        public ActionResult Index(int Page = 1)
        {
            
            //IPagedList<AppUsers> lst = null;
            //var model = _userService.GetAllUsers();
            //lst = model.ToPagedList(Page, (int)SiteEnum.PageSetting.PageSize);
            List<AppUsers> lst = new List<AppUsers>();
            lst = _userService.GetAllUsers();
            return View(lst);

        }

        public JsonResult UpdateUserStatus(string UserGuid, string Status)
        {
            var result = _userService.UpdateUserStatus(UserGuid, Status);
            return new JsonResult() { Data = new { status = result.IsSuccess, message = result.Message }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult UpdateUserLockoutStatus(string UserGuid, string Status)
        {
            var result = _userService.UpdateUserLockoutStatus(UserGuid, Status);
            return new JsonResult() { Data = new { status = result.IsSuccess, message = result.Message }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}