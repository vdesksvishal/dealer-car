﻿using Incite.Data.Repositories;
using Incite.Services;
using Incite.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Trialcar.Web.Helper;

namespace Trialcar.Web.Controllers
{
    public class HomeController : Controller
    {
        #region Private Member Variables
        private readonly IUsersService _usersService;
        private readonly IDealerCarService _dealerCarService;
        #endregion Private Member Variables
        #region Ctor
        public HomeController(IUsersService usersService, IDealerCarService CarDetailService)
        {
            _usersService = usersService;
            _dealerCarService = CarDetailService;
        }
        #endregion Ctor
        public ActionResult Index(bool LogOut=false)
        {
            if (LogOut)
            {
                ViewBag.Login = false;
            }
            return View();
        }

        public ActionResult About()
        {
            var test= _usersService.GetAllUsers();
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            _usersService.InsatllRecored();
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Authorize]
        public PartialViewResult MiniSearch()
        {
            var model = _dealerCarService.PrepareDealarCarViewModel();
            return PartialView("_FilterSearch",model);
        }
    }
}