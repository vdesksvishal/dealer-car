﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Helpers;
using System.Web.Mvc;
using Incite.Core.Domain;
using Incite.Core.Interfaces;
using Incite.Data;
using Incite.Data.Repositories;
using Incite.Services;
using Incite.Services.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Stripe;
using Trialcar.Web.Helper;
using Trialcar.Web.Infrastructure;

namespace Trialcar.Web.Controllers
{
    public class AccountController : Controller
    {
        #region Private Member Variables
        private readonly IUsersService _userService;
        private readonly IAppUsersRepository _appUsersRepository;
        private readonly IEmailTemplateService _emailTemplateService;
        private readonly IPaymentService _paymentService;
        #endregion Private Member Variables
        public AccountController(IUsersService usersService, IEmailTemplateService emailTemplateService, IAppUsersRepository appUsersRepository, IPaymentService paymentService)
        {
            _userService = usersService;
            _appUsersRepository = appUsersRepository;
            _emailTemplateService = emailTemplateService;
            _paymentService = paymentService;

        }
        #region OLD Code
        ////
        //// GET: /Account/Login
        //[AllowAnonymous]
        //public ActionResult Login(string returnUrl)
        //{
        //    ViewBag.ReturnUrl = returnUrl;
        //    return View();
        //}

        ////
        //// POST: /Account/Login
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }

        //    // This doesn't count login failures towards account lockout
        //    // To enable password failures to trigger account lockout, change to shouldLockout: true
        //    //var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
        //    //switch (result)
        //    //{
        //    //    case SignInStatus.Success:
        //    //        return RedirectToLocal(returnUrl);
        //    //    case SignInStatus.LockedOut:
        //    //        return View("Lockout");
        //    //    case SignInStatus.RequiresVerification:
        //    //        return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
        //    //    case SignInStatus.Failure:
        //    //    default:
        //    //        ModelState.AddModelError("", "Invalid login attempt.");
        //    //        return View(model);
        //    //}
        //    return View();
        //}




        ////
        //// GET: /Account/Register
        //[AllowAnonymous]
        //public ActionResult Register()
        //{
        //    return View();
        //}

        ////
        //// POST: /Account/Register
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Register(RegisterViewModel model)
        //{
        //    //if (ModelState.IsValid)
        //    //{
        //    //    var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
        //    //    var result = await UserManager.CreateAsync(user, model.Password);
        //    //    if (result.Succeeded)
        //    //    {
        //    //        await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);

        //    //        // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
        //    //        // Send an email with this link
        //    //        // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
        //    //        // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
        //    //        // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

        //    //        return RedirectToAction("Index", "Home");
        //    //    }
        //    //    AddErrors(result);
        //    //}

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //}


        //// GET: /Account/ForgotPassword
        //[AllowAnonymous]
        //public ActionResult ForgotPassword()
        //{
        //    return View();
        //}

        ////
        //// POST: /Account/ForgotPassword
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = await UserManager.FindByNameAsync(model.Email);
        //        if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
        //        {
        //            // Don't reveal that the user does not exist or is not confirmed
        //            return View("ForgotPasswordConfirmation");
        //        }

        //        // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
        //        // Send an email with this link
        //        // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
        //        // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
        //        // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
        //        // return RedirectToAction("ForgotPasswordConfirmation", "Account");
        //    }

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //} 
        #endregion

        #region NEW Code

        #region Registration 


        [ChildActionOnly]
        public PartialViewResult Register()
        {
            var model = new RegisterViewModel()
            {
                StateList = DropDownListHelper.BindStateList()
            };

            return PartialView(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public JsonResult Register(RegisterViewModel model)
        {
            var resultmodel = new ProcessViewModel()
            {
                IsSuccess = true
            };
            if (string.IsNullOrWhiteSpace(Request.Form["g-recaptcha-response"]))
            {
                ModelState.AddModelError("", "Please complete CAPTCHA");
                resultmodel.IsSuccess = false;
                resultmodel.Message = "Please complete CAPTCHA.";
            }

            ReCaptcha.ReCaptchaResponse reCaptchaResponse = ReCaptcha.VerifyCaptcha(WebConfigurationManager.AppSettings["CapthaSecretKey"], Request.Form["g-recaptcha-response"]);

            if (!reCaptchaResponse.success)
            {
                ModelState.AddModelError("", "Google Captcha Validation Failed.");
                resultmodel.IsSuccess = false;
                resultmodel.Message = "Google Captcha Validation Failed.";
            }

            if (resultmodel.IsSuccess)
            {
                HttpFileCollectionBase files = Request.Files;
                string fileName = string.Empty;
                if (files.Count > 0 && files[0] != null && files[0].ContentLength > 0)
                {
                    model.UploadProfilePicture = files[0];
                }
                var result = _userService.NewUser(model);
                resultmodel.IsSuccess = result.IsSuccess;
                resultmodel.Message = result.Message;
            }


            return new JsonResult() { Data = new { status = resultmodel.IsSuccess, message = resultmodel.Message }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }
        #endregion


        #region Update Settings

        #region Stripe's Create/Get Customer Accounts
        public ActionResult VerifyBankAccount()
        {
            var model = new VerifyBankAccount()
            {
            };

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult VerifyBankAccount(VerifyBankAccount model)
        {
            int currentUserId = Infrastructure.Authentication.CurrentUserId ?? default(int);
            var result = _paymentService.VerifyCustomer(currentUserId, model);

            //Return Json result
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public ActionResult UserPaymentSourceList()
        {
            int currentUserId = Infrastructure.Authentication.CurrentUserId ?? default(int);
            var userProfile = _userService.GetUserProfile(currentUserId);
            var model = _paymentService.GetUserPaymentSourceList(userProfile.User.Email);
            return View(model);
        }

        [HttpGet]
        public ActionResult UpdateSettings()
        {
            //CurrentUserId to get user profile
            int currentUserId = Infrastructure.Authentication.CurrentUserId ?? default(int);

            var userProfile = _userService.GetUserProfile(currentUserId);
            UpdateSettingsViewModel model = new UpdateSettingsViewModel();
            
            model.CustomerEmailId = userProfile.User.Email;
            model.AppUserId = currentUserId;
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult UpdateSettings(UpdateSettingsViewModel model)
        {
            //Add customer to stripe
            var result = _paymentService.CreateCustomer(model);
            
            //Return Json result
            return Json(result, JsonRequestBehavior.AllowGet);
            
        }
        #endregion

        #region Stripe's Create/Get Connect Accounts
        public ActionResult UserConnectSourceList()
        {
            int currentUserId = Infrastructure.Authentication.CurrentUserId ?? default(int);
            var userProfile = _userService.GetUserProfile(currentUserId);
            var model = _paymentService.GetConnectUserPaymentSourceList(userProfile.User.Email);
            return View(model);
        }


        public ActionResult AddConnectAccounts()
        {
            //CurrentUserId to get user profile
            int currentUserId = Infrastructure.Authentication.CurrentUserId ?? default(int);

            var userProfile = _userService.GetUserProfile(currentUserId);
            AddConnectAccountsViewModel model = new AddConnectAccountsViewModel();

            model.CustomerEmailId = userProfile.User.Email;
            model.AppUserId = currentUserId;
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult AddConnectAccounts(AddConnectAccountsViewModel AddConnectAccountsViewModel)
        {
            AddConnectAccountsViewModel.IP= Request.UserHostAddress; //get user host address
            var model = _paymentService.CreateConnect(AddConnectAccountsViewModel);
            //Return Json result
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion

        #region Login

        //[HttpGet]
        //[AllowAnonymous]
        //public ActionResult Login(string returnUrl)
        //{
        //    LoginViewModel model = new LoginViewModel();
        //    _UsersService.PrepareModel(model);
        //    model.ReturnUrl = returnUrl;
        //    return View(model.pageViewName, model);
        //}

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public JsonResult Login(LoginViewModel model)
        {
            Authentication.IdentitySignout();
            OverlayManager.cleareOverlay();
            var result = _userService.ValidateUser(model.Email, model.LoginPassword);
            if (result.Process.IsSuccess)
            {
                Authentication.IdentitySignin(result.user, result.user.Id.ToString(), result.user.UsersRoles.ToList());
                System.Web.Security.FormsAuthentication.SetAuthCookie(result.user.UserName, true);
            }
            else
            {
                ModelState.AddModelError("", result.Process.Message);
            }
            return new JsonResult() { Data = new { status = result.Process.IsSuccess, message = result.Process.Message }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        #endregion

        #region Logout

        [Authorize]
        public ActionResult Logout()
        {
            Authentication.IdentitySignout();
            OverlayManager.cleareOverlay();
            System.Web.Security.FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        #endregion

        #region Confirm Email

        [AllowAnonymous]
        public ActionResult ConfirmEmail(Guid uid)
        {
            var IsConfirmed = _userService.ConfirmEmail(uid);
            ViewBag.Status = IsConfirmed;
            return View();
        }
        #endregion

        #region Update Profile

        [HttpGet]
        public ActionResult UpdateProfile()
        {
            //CurrentUserId to get user profile
            int currentUserId = Infrastructure.Authentication.CurrentUserId ?? default(int);

            //Call GetUserProfile to fille form
            var userProfile = _userService.GetUserProfile(currentUserId);

            //Fill State Dropdownlist
            userProfile.StateList = DropDownListHelper.BindStateList();

            //Return view
            return View(userProfile);
        }

        [HttpPost]
        public ActionResult UpdateProfile(UserProfileViewModel model)
        {
            //CurrentUserId to get user profile
            int currentUserId = Infrastructure.Authentication.CurrentUserId ?? default(int);

            //Get file from request
            HttpFileCollectionBase files = Request.Files;
            string fileName = string.Empty;
            if (files.Count > 0 && files[0] != null && files[0].ContentLength > 0)
            {
                model.UploadProfilePicture = files[0];
            }

            //Call update profile and return result ProcessViewModel
            var result = _userService.UpdateUserProfile(model, currentUserId);

            //Return Json result
            return new JsonResult() { Data = new { status = result.IsSuccess, message = result.Message }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }




        #endregion

        #region Update Password

        [HttpGet]
        public ActionResult UpdatePassword()
        {
            //Decalre Variable
            var updatePassword = new UpdatePasswordViewModel();

            //Return view
            return View(updatePassword);
        }

        [HttpPost]
        public ActionResult UpdatePassword(UpdatePasswordViewModel model)
        {
            //CurrentUserId to get user profile
            int currentUserId = Infrastructure.Authentication.CurrentUserId ?? default(int);

            //Call UpdatePassword service
            var result = _userService.UpdatePassword(model, currentUserId);

            //Return Json result
            return new JsonResult() { Data = new { status = result.IsSuccess, message = result.Message }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        #endregion

        #region Forgot Password
        [AllowAnonymous]
        [HttpPost]
        public ActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            //Call forgotPassword service
            var result = _userService.ForgotPassword(model.FEmailAddress);

            //Return Json result
            return new JsonResult() { Data = new { status = result.IsSuccess, message = result.Message }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        #endregion

        #region Reset Password

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ResetPassword(string uid)
        {
            ResetPasswordViewModel model = new ResetPasswordViewModel();

            if (!string.IsNullOrWhiteSpace(uid))
                model.uid = new Guid(uid);

            return PartialView(model);
        }


        [AllowAnonymous]
        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            //Call UpdatePassword service
            var result = _userService.ResetPassword(model);
            //Return Json result
            return new JsonResult() { Data = new { status = result.IsSuccess, message = result.Message }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        #endregion

        #endregion


    }
}