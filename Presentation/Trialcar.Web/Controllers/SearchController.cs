﻿using Incite.Data.Repositories;
using Incite.Services;
using Incite.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Trialcar.Web.Helper;

namespace Trialcar.Web.Controllers
{
    [Authorize]
    public class SearchController : Controller
    {
        #region  Private Member Variables
        private readonly IDealerCarService _DealerCarService;
        private readonly ICarSearchService _CarSearchService;
        #endregion
        #region ctor
        public SearchController(IDealerCarService dealerCarService, ICarSearchService carSearchService)
        {
            _DealerCarService = dealerCarService;
            _CarSearchService = carSearchService;
        }
        #endregion
        // GET: Search
        public ActionResult SearchResult()
        {
            return View();
        }
        [ChildActionOnly]
        public PartialViewResult HomeFilter()
        {
            var model = _CarSearchService.PrepareHomeCareFilter();
            return PartialView(model);
        }
        [ChildActionOnly]
        public PartialViewResult SearchLeftFilter(CarFilterViewModel request)
        {
            var model = _CarSearchService.PrepareHomeCareFilter(request);
            return PartialView(model);
        }
        [ChildActionOnly]
        public PartialViewResult MygarageLeftFilter(CarFilterViewModel request)
        {
            var model = _CarSearchService.PrepareHomeCareFilter(request);
            return PartialView(model);
        }
        [ChildActionOnly]
        public PartialViewResult CarlistSortBy(CarFilterViewModel request)
        {
            var model = _CarSearchService.PrepareSortby(request);
            return PartialView(model);
        }
        
    }
}