﻿using Incite.Services;
using Incite.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Incite.Core.Domain;
using Incite.Core.Infrastructure;

namespace Trialcar.Web.Controllers
{
    [Authorize]
    public class SharedController : Controller
    {
        #region  Private Member Variables
        private readonly IDealerCarService _dealerCarService;
        private readonly IUsersService _userService;
        private readonly IPaymentService _paymentService;

        #endregion

        public SharedController(IUsersService usersService, IDealerCarService DealerCarService, IPaymentService paymentService)
        {
            _userService = usersService;
            _dealerCarService = DealerCarService;
            _paymentService = paymentService;
        }

       

        #region Car Listing
        public PartialViewResult CarListing(CarFilterViewModel args, bool IsDealerSearch, int dealerId = 0, bool IsSold = false, int Page = 1)
        {
            IPagedList<DealerCarDetails> lst = null;
            AppUsers appUsers = _dealerCarService.getUserById(dealerId);
            long currentUserId;
            if (dealerId > 0)
            {
                currentUserId = dealerId;
            } else
            {
                currentUserId = Infrastructure.Authentication.CurrentUserId ?? default(int);
            }

            ViewBag.IsPaymetList = true;
            var userProfile = _userService.GetUserProfile(Convert.ToInt32(currentUserId));
            var userpaymentList = _paymentService.GetUserPaymentSourceList(userProfile.User.Email);
            if(userpaymentList != null && userpaymentList.Data != null && userpaymentList.Count() > 0)
            {
                ViewBag.IsPaymetList = false;
            }
            


            var model = _dealerCarService.GetDealerCarListing(currentUserId, args, IsDealerSearch, IsSold);

            lst = model.ToPagedList(Page, (int)SiteEnum.PageSetting.PageSize);
            return PartialView("_CarListing", Tuple.Create(lst, appUsers));
        }
        #endregion
    }
}