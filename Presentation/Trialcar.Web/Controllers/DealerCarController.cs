﻿using Incite.Core.Domain;
using Incite.Core.Infrastructure;
using Incite.Services;
using Incite.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Trialcar.Web.Controllers
{
    [Authorize]
    public class DealerCarController : Controller
    {
        #region  Private Member Variables
        private readonly IDealerCarService _dealerCarService;
        private readonly IPaymentService _paymentService;
        #endregion

        #region Add car to compare
        public ActionResult AddToCompare()
        {
            dynamic showMessageString = string.Empty;
            try
            {

                System.Collections.Specialized.NameValueCollection nameValue = Request.Params;
                string dealerCarDetailId = nameValue.Get("dealerCarDetailId");
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                DealerCarDetails carDetails = _dealerCarService.GetCarDetailById(int.Parse(dealerCarDetailId));

                if (carDetails.IsOnHold == false && carDetails.IsSold == false)
                {
                    if (context.Session["dealerCarDetailId"] == null)
                    {
                        //dealerIds[0] = dealerCarDetailId;
                        context.Session["dealerCarDetailId"] = dealerCarDetailId;
                        showMessageString = new
                        {
                            msg = "Add Car To Compare Successfully."
                        };
                    }
                    else
                    {
                        string carDetailSession = context.Session["dealerCarDetailId"].ToString();
                        string[] strCarDetailIds = carDetailSession.Split(',');
                        if (strCarDetailIds.Count() < 3)
                        {
                            context.Session["dealerCarDetailId"] = context.Session["dealerCarDetailId"] + "," + dealerCarDetailId;
                            showMessageString = new
                            {
                                msg = "Add Car To Compare Successfully."
                            };
                        }
                        else
                        {
                            showMessageString = new
                            {
                                err = "Maximum 3 Cars Compared."
                            };
                        }

                    }
                }
                else
                {
                    showMessageString = new
                    {
                        err = "Sold Cars and Cars On Hold Cannot Be Compared."
                    };
                }

            }
            catch (Exception e)
            {
                showMessageString = new
                {
                    err = "Problem While Adding Car To Compare."
                };
            }

            return Json(showMessageString, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Remove car from compare
        public ActionResult RemoveToCompare()
        {
            dynamic showMessageString = string.Empty;
            try
            {
                System.Collections.Specialized.NameValueCollection nameValue = Request.Params;
                string dealerCarDetailId = nameValue.Get("dealerCarDetailId");

                System.Web.HttpContext context = System.Web.HttpContext.Current;

                if (context.Session["dealerCarDetailId"] != null)
                {
                    string carDetailSession = context.Session["dealerCarDetailId"].ToString();
                    string[] strCarDetailIds = carDetailSession.Split(',');
                    if (strCarDetailIds.Contains(dealerCarDetailId))
                    {
                        var list = new List<string>(strCarDetailIds);
                        list.Remove(dealerCarDetailId);
                        strCarDetailIds = list.ToArray();
                    }
                    if (strCarDetailIds.Count() > 0)
                        context.Session["dealerCarDetailId"] = String.Join(",", strCarDetailIds);
                    else
                        context.Session["dealerCarDetailId"] = null;
                }
                showMessageString = new
                {
                    msg = "Remove Car From Compare Successfully."
                };
            }
            catch (Exception e)
            {
                showMessageString = new
                {
                    err = "Problem While Removing Car From Compare."
                };
            }

            return Json(showMessageString, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Car Compare
        public ActionResult Compare()
        {

            System.Web.HttpContext context = System.Web.HttpContext.Current;
            IList<DealerCarDetails> carList = new List<DealerCarDetails>();
            IList<CarAccessories> carAccessories = _dealerCarService.GetCarAccessories();

            if (context.Session["dealerCarDetailId"] != null)
            {
                string carDetailSession = context.Session["dealerCarDetailId"].ToString();
                string[] strCarDetailIds = carDetailSession.Split(',');

                foreach (string strId in strCarDetailIds)
                {
                    if (int.Parse(strId) > 0)
                    {
                        DealerCarDetails model = _dealerCarService.GetCarDetailById(int.Parse(strId));
                        carList.Add(model);
                    }
                }
            }

            return View(Tuple.Create(carList, carAccessories));
        }
        #endregion


        public DealerCarController(IDealerCarService DealerCarService, IPaymentService PaymentService)
        {
            _dealerCarService = DealerCarService;
            _paymentService = PaymentService;
        }
        // GET: DealerCar

        public ActionResult Index(DealerCarViewModel model = null)
        {
            return View(model);
        }

        #region Left Side Search Criteria
        public ActionResult MiniSearch(CarFilterViewModel args)
        {
            var model = _dealerCarService.PrepareDealarCarViewModel();
            return PartialView("_LeftSideSearchPanel", model);
        }

        #endregion

        #region Add and Update Car
        public ActionResult AddUpdateCar(string Cid)
        {
            int CId = 0;
            if (!string.IsNullOrEmpty(Cid))
                CId = int.Parse(EncryptDecryptHelper.Decrypt(Cid));
            var model = _dealerCarService.PrepareDealarCarViewModel(CId);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddUpdateCar(DealerCarViewModel model)
        {
            model.AppUserId = Infrastructure.Authentication.CurrentUserId;
            var result = _dealerCarService.AddOrUpdateCar(model);

            return RedirectToAction("Index");
        }

        #endregion

        #region Delete Car Images

        [HttpPost]
        public ActionResult DeleteCarImage(string imageId)
        {
            ProcessViewModel resultmodel = new ProcessViewModel();
            resultmodel = _dealerCarService.DeleteCarImage(imageId);
            return new JsonResult() { Data = new { status = resultmodel.IsSuccess, message = resultmodel.Message }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        #endregion


        #region Add or Update Dealer Car Comment        
        public ActionResult AddUpdateCarComment(DealerCommentNRatingViewModel dealerCommentVM)
        {
            dynamic showMessageString = string.Empty;
            if ((dealerCommentVM.Rating == 0) && (dealerCommentVM.Comment == null || dealerCommentVM.Comment.Trim() == ""))
            {
                showMessageString = new
                {
                    cls = "text-danger",
                    msg = "Please Rate or Comment."
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            else
            {
                dealerCommentVM.DealerId = Infrastructure.Authentication.CurrentUserId.Value;
                AppUsers ratingUser = _dealerCarService.addOrUpdateDealerCarComment(dealerCommentVM);

                float totRating = 0.0F, count = 0.0F;
                double avgRating = 0.0;
                int dealerCommentId = 0;

                if (ratingUser.DealerComments.Count > 0)
                {
                    foreach (var item in ratingUser.DealerComments)
                    {
                        totRating += item.Rating;
                        count++;

                    }
                    avgRating = Math.Round((totRating / count), 1);
                }

                if (dealerCommentVM.Id == 0)
                {
                    dealerCommentId = _dealerCarService.countDealerComments();

                    showMessageString = new
                    {
                        cls = "bg-success",
                        msg = "Rating done successfully.",
                        avgRatings = avgRating,
                        commentId = dealerCommentId
                    };
                }
                else
                {
                    showMessageString = new
                    {
                        cls = "bg-success",
                        msg = "Rating done successfully.",
                        avgRatings = avgRating
                    };
                }

                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Add or Update Dealer Car Comment Reply      
        public ActionResult AddUpdateCarCommentReply(DealerCommentNRatingViewModel dealerCommentVM)
        {
            //_dealerCarService.addOrUpdateDealerCarComment(dealerCommentVM);
            //return Content("Replied successfully.");
            dynamic showMessageString = string.Empty;
            if (dealerCommentVM.CommentReply == null || dealerCommentVM.CommentReply.Trim() == "")
            {
                showMessageString = new
                {
                    cls = "text-danger",
                    msg = "Please Reply to Comment."
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            else
            {
                _dealerCarService.addOrUpdateDealerCarCommentReply(dealerCommentVM);
                showMessageString = new
                {
                    cls = "bg-success",
                    msg = "Replied successfully."
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        
        #region Remote Validation
        public JsonResult CheckForDuplication(string vin, string PreviousVIN)
        {
            if (vin == PreviousVIN)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            if (_dealerCarService.IsVinExist(vin))
            {
                return Json(SiteConstants.VIN_AVAILABLE, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Car Detail View
        public ActionResult CarDetail(string cId)
        {
            int CId = 0;
            if (!string.IsNullOrEmpty(cId))
                CId = int.Parse(EncryptDecryptHelper.Decrypt(cId));

            int.Parse(EncryptDecryptHelper.Decrypt(cId));
            //model.AppUserId = Infrastructure.Authentication.CurrentUserId;
            //var result = _dealerCarService.InsertRecord(model);
            var model = _dealerCarService.GetCarDetailById(CId);
            return View(model);
        }

       #endregion

        #region Car Buy
        public ActionResult CarBuy(string cId)
        {
            int CId = 0;
            if (!string.IsNullOrEmpty(cId))
                CId = int.Parse(EncryptDecryptHelper.Decrypt(cId));

            BuyNowHoldNowDetailViewModel model = new BuyNowHoldNowDetailViewModel();
            //CurrentUserId to get user profile
            int currentUserId = Infrastructure.Authentication.CurrentUserId ?? default(int);
            model.PaymentTypes = _paymentService.GetCustomerPaymentTypes(currentUserId);
            model.DealerCarDetailId = CId;
            model.IsOnHold = false;
            //_dealerCarService.CarBuy(cid, currentUserId);
            //return Json(true, JsonRequestBehavior.AllowGet);
            return View("BuyNowHoldNowDetail", model);
        }
        [HttpPost]
        public JsonResult BuyNowHoldNowDetail(BuyNowHoldNowDetailViewModel model)
        {
            if (model.PaymentTypeId == null)
                return null;

            int currentUserId = Infrastructure.Authentication.CurrentUserId ?? default(int);
            model.BuyerId = currentUserId;
            var result = _paymentService.ProcessBuyNowHoldNow(model);

            //Return Json result
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Car OnHold
        public ActionResult CarOnHold(string cId)
        {
            int CId = 0;
            if (!string.IsNullOrEmpty(cId))
                CId = int.Parse(EncryptDecryptHelper.Decrypt(cId));
            BuyNowHoldNowDetailViewModel model = new BuyNowHoldNowDetailViewModel();
            //CurrentUserId to get user profile
            int currentUserId = Infrastructure.Authentication.CurrentUserId ?? default(int);
            model.PaymentTypes = _paymentService.GetCustomerPaymentTypes(currentUserId);
            model.DealerCarDetailId = CId;
            model.IsOnHold = true;
            //_dealerCarService.CarOnHold(cid, currentUserId);
            //return Json(true, JsonRequestBehavior.AllowGet);
            return View("BuyNowHoldNowDetail", model);
        }

        #endregion

        #region Send Message To Dealer
        public ActionResult SendMessage(DealerMessagesViewModel dealerMessagesVM)
        {
            dynamic showMessageString = string.Empty;
            if (dealerMessagesVM.Message.Trim() != "")
            {
                try
                {
                    dealerMessagesVM.FromDealerId = Infrastructure.Authentication.CurrentUserId.Value;
                    //AppUsers toUser = _dealerCarService.getUserById(dealerMessagesVM.ToDealerId);

                    _dealerCarService.InsertMessage(dealerMessagesVM);

                    if (dealerMessagesVM.FromAction == "PopulateMessages")
                    {
                        showMessageString = new
                        {
                            //sender = toUser.UserName,
                            message = dealerMessagesVM.Message,
                            from = "PopulateMessages"
                        };
                    }
                    else
                    {
                        showMessageString = new
                        {
                            msg = "Send Message Successfully"
                        };
                    }

                }
                catch (Exception e)
                {
                    showMessageString = new
                    {
                        err = "Problem While Send Message"
                    };
                }
            }
            else if (dealerMessagesVM.FromAction != "PopulateMessages")
            {
                showMessageString = new
                {
                    err = "Please Enter Message"
                };
            }

            return Json(showMessageString, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Dealer Messages
        public ActionResult Messages()
        {
            //var model = _dealerCarService.GetCarDetailById(CId);
            int currLoginDealerId = Infrastructure.Authentication.CurrentUserId.Value;
            //AppUsers dealer = _dealerCarService.getUserById(currLoginDealerId);
            //IList<DealerMessages> model = _dealerCarService.GetAllMessages(currLoginDealerId);
            IList<AppUsers> dealerIdList = _dealerCarService.getDealerIdsOfMessages(currLoginDealerId);
            //return View(Tuple.Create(dealer, model, dealerIdList));
            return View(dealerIdList);
        }
        #endregion

        #region Populate Messages
        public ActionResult PopulateMessages(int dealerId)
        {
            int currLoginDealerId = Infrastructure.Authentication.CurrentUserId.Value;
            IList<DealerMessages> model = _dealerCarService.GetAllMessages(currLoginDealerId, dealerId);
            AppUsers dealer = _dealerCarService.getUserById(dealerId);
            //return View(Tuple.Create(dealer, model));
            return View(Tuple.Create(dealer, model));
        }
        #endregion

        #region Populate Messages from User Name
        public ActionResult PopulateMessagesByName(string userName)
        {
            int dealerId = _dealerCarService.getUserIdByName(userName);
            int currLoginDealerId = Infrastructure.Authentication.CurrentUserId.Value;
            IList<DealerMessages> model = _dealerCarService.GetAllMessages(currLoginDealerId, dealerId);
            AppUsers dealer = _dealerCarService.getUserById(dealerId);
            //return View(Tuple.Create(dealer, model));
            return View("PopulateMessages", Tuple.Create(dealer, model));
        }
        #endregion

        #region Get Dealer sold car listing
        public ActionResult DealerSoldCar()
        {
            return View();
        }
        #endregion

        #region Sold Dealer Car
        [HttpGet]
        public ActionResult SoldCar(string cId)
        {
            var result = new ProcessViewModel();

            int CId = 0;
            if (!string.IsNullOrEmpty(cId))
                CId = int.Parse(EncryptDecryptHelper.Decrypt(cId));

            result = _dealerCarService.CarSold(CId);

            //Return Json result
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}