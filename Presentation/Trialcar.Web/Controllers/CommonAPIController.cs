﻿using Incite.Core.Domain;
using Incite.Data.Repositories;
using Incite.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Mvc;

namespace Trialcar.Web.Controllers
{
    public class CommonAPIController : ApiController
    {
        #region Private Variable
        private readonly IDealerCarService _dealerCarService;
        #endregion

        public CommonAPIController()
        {
            _dealerCarService = Incite.Dependency.PluginLocator.Resolve<IDealerCarService>();
        }
        public JsonResult GetCarModel(int CarmakerVal = 0)
        {
            var list = _dealerCarService.CarModelListbyMaker(CarmakerVal);
            return new JsonResult() { Data = new { modelList = list }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public JsonResult GetCarYear(int CarmakerVal = 0, int carModelVal = 0)
        {
            var elist = _dealerCarService.CarYearList(CarmakerVal, carModelVal);
            var list = elist.OrderBy(s => s.Name);
            return new JsonResult() { Data = new { modelList = list }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetCarTrimProp(int CarmakerVal = 0, int carModelVal = 0, int CarYearVal = 0)
        {
            var list = _dealerCarService.CarTrimPropList(CarmakerVal, carModelVal, CarYearVal);
            return new JsonResult() { Data = new { modelList = list }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult Getpropertybytrim(int trimval = 0)
        {

            var cardetailmodel = _dealerCarService.GetDetailbyTrim(trimval);

            if (cardetailmodel != null)
            {
                var result = new
                {
                    Id = cardetailmodel.Id,
                    DriveTypeId = cardetailmodel.DriveType.Id,
                    DriveTypeName = cardetailmodel.DriveType.DriveTypeName,
                    FuelTypeId = cardetailmodel.FuelType.Id,
                    FuelTypeName = cardetailmodel.FuelType.FuelTypeName,
                    TransmissionId = cardetailmodel.Transmission.Id,
                    TransmissionName = cardetailmodel.Transmission.TransmissionName,
                };
                return new JsonResult() { Data = new { model = result }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            return new JsonResult() { Data = null, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public HttpResponseMessage GetCarDetailFromVINAPI(string vinNumber)
        {
            var dbCar = _dealerCarService.GetCarDetailByVinApi(vinNumber);
            
            if(dbCar!=null)
            return Request.CreateResponse(HttpStatusCode.OK, dbCar, Configuration.Formatters.JsonFormatter);
            else
                return Request.CreateResponse(HttpStatusCode.NotFound, dbCar, Configuration.Formatters.JsonFormatter);
        }
    }
}