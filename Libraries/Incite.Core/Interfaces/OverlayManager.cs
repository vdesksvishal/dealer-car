﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Incite.Core.Interfaces
{
    public class OverlayManager
    {
        public static bool IsUserFirstTimelogin
        {
            get
            {
                if (HttpContext.Current.Session["firsttimelogin"] != null)
                    return (bool)HttpContext.Current.Session["FirstTimeLogin"];
                else
                    return false;
            }
            set
            {
                HttpContext.Current.Session["FirstTimeLogin"] = value;
            }
        }
        public static void cleareOverlay()
        {
            HttpContext.Current.Session.Abandon();
        }
        public static string UserVisitedPages
        {
            get
            {
                if (HttpContext.Current.Session["uservisitedpage"] != null)
                    return HttpContext.Current.Session["uservisitedpage"].ToString();
                return string.Empty;
            }
            set
            {

                if (HttpContext.Current.Session["uservisitedpage"] == null)
                    HttpContext.Current.Session["uservisitedpage"] = value;
                else
                {
                    string currentSession = HttpContext.Current.Session["uservisitedpage"].ToString();
                    currentSession += "," + value;
                    HttpContext.Current.Session["uservisitedpage"] = currentSession;
                }
            }
        }
        public static bool allowOverlay(string PageName)
        {
            bool result = false;
            if (IsUserFirstTimelogin && (string.IsNullOrEmpty(UserVisitedPages) || !UserVisitedPages.Split(',').Any(x => x == PageName)))
            {
                result = true;
                UserVisitedPages = PageName;
            }

            return result;
        }
    }
}
