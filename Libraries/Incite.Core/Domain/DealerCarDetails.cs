﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class DealerCarDetails : Entity<int>
    {
        public DealerCarDetails()
        {
            EngineType = new EngineType();
            User = new AppUsers();
            CarDetails = new CarDetails();
            CarAccessories = new List<DealerCarAccessories>();
            CarImages = new List<DealerCarImages>();
        }

        public virtual int AppUserId { get; set; }
        public virtual int CarDetailsId { get; set; }
        public virtual int EngineTypeId { get; set; }

        public virtual AppUsers User { get; set; }
        public virtual CarDetails CarDetails { get; set; }
        public virtual EngineType EngineType { get; set; }
        public virtual decimal Miles { get; set; }
        public virtual int PriceType {get;set;}
        public virtual int OwnershipType { get; set; }
        public virtual int TitleType { get; set; }
        public virtual int CarConditionType { get; set; }
        public virtual string VIN { get; set; }
        public virtual string VehicleOverview { get; set; }

        public virtual IList<DealerCarAccessories> CarAccessories { get; set; }
        public virtual IList<DealerCarImages> CarImages { get; set; }
        public virtual DateTime CreatedTs { get; set; }
        public virtual decimal Price { get; set; }
        public virtual DateTime UpdatedTs { get; set; }


        public virtual bool IsOnHold { get; set; }
        public virtual bool IsSold { get; set; }
    }
}
