﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class EngineType : Entity<int>
    {
        public EngineType()
        {
            dealerCarDetails = new List<DealerCarDetails>();
        }
        
        public virtual string EngineTypeName { get; set; }

        public virtual IList<DealerCarDetails> dealerCarDetails { get; set; }
    }
}
