﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class CarDetails : Entity<int>
    {
        public CarDetails()
        {
            FuelType = new FuelType();
            Transmission = new Transmission();
            DriveType = new DriveType();

            Maker = new CarMaker();
            Model = new CarModel();
            Year = new CarYear();

            dealerCarDetails = new List<DealerCarDetails>();
        }
        
        public virtual int CarMakerId { get; set; }
        public virtual int CarModelId { get; set; }
        public virtual int CarYearId { get; set; }

        public virtual int FuelTypeId { get; set; }
        public virtual int TransmissionId { get; set; }
        public virtual int DriveTypeId { get; set; }


        public virtual long IndexId { get; set; }
        public virtual string Trim { get; set; }
        public virtual decimal Price { get; set; }
        public virtual byte[] ColorsExterior { get; set; }
        public virtual byte[] ColorsInterior { get; set; }
        public virtual int Length { get; set; }
        public virtual float Width { get; set; }
        public virtual float Height { get; set; }
        public virtual float WheelBase { get; set; }
        public virtual int CurbWeight { get; set; }
        public virtual string Cylinders { get; set; }
        public virtual float EngineSize { get; set; }
        public virtual int HP { get; set; }
        public virtual int RPM { get; set; }
        public virtual int Torque { get; set; }
        public virtual int TorqueRPM { get; set; }
        public virtual float FuelTankCap { get; set; }
        public virtual int CombinedMPG { get; set; }
        public virtual string EPAMileage { get; set; }
        public virtual string RangeMiles { get; set; }
        public virtual string Engine { get; set; }
        public virtual string BodyType { get; set; }
        public virtual CarMaker Maker { get; set; }
        public virtual CarModel Model { get; set; }
        public virtual CarYear Year { get; set; }

        public virtual FuelType FuelType { get; set; }
        public virtual Transmission Transmission { get; set; }
        public virtual DriveType DriveType { get; set; }

        public virtual IList<DealerCarDetails> dealerCarDetails { get; set; }


    }
}
