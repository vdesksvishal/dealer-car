﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class DealerCommentNRating : Entity<int>
    {
        public DealerCommentNRating()
        {
            appUsers = new AppUsers();
            ratingUsers = new AppUsers();
        }
        public virtual int DealerId { get; set; }
        public virtual int RatingForDealerId { get; set; }
        public virtual string Comment { get; set; }
        public virtual string CommentReply { get; set; }
        public virtual int Rating { get; set; }
        public virtual DateTime CreatedTs { get; set; }
        public virtual AppUsers appUsers { get; set; }
        public virtual AppUsers ratingUsers { get; set; }
    }
}
