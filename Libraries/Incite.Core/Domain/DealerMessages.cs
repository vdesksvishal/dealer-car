﻿using System;

namespace Incite.Core.Domain
{
    public class DealerMessages : Entity<int>
    {
        public DealerMessages()
        {
            fromDealer = new AppUsers();
            toDealer = new AppUsers();
        }
        public virtual int FromDealerId { get; set; }
        public virtual int ToDealerId { get; set; }        
        public virtual string Message { get; set; }
        public virtual DateTime CreatedTs { get; set; }
        public virtual AppUsers fromDealer { get; set; }
        public virtual AppUsers toDealer { get; set; }
    }
}
