﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class MileFilterData : Entity<int>
    {
        public virtual string Zipcod { get; set; }
        public virtual string Mile { get; set; }
        public virtual byte[] Record { get; set; }

    }
}
