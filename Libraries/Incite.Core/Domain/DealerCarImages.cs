﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class DealerCarImages : Entity<int>
    {
        public DealerCarImages()
        {
            dealerCarDetails = new DealerCarDetails();
        }
        public virtual string NormalImagePath { get; set; }
        public virtual string ThumbImagePath { get; set; }
        public virtual string LargeImagePath { get; set; }

        public virtual int DealerCarDetailsId { get; set; }

        public virtual bool IsDeleted { get; set; }

        public virtual DealerCarDetails dealerCarDetails { get; set; }
    }
}
