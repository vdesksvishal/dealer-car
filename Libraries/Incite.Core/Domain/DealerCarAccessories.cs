﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{

    public class DealerCarAccessories : Entity<int>
    {
        public DealerCarAccessories()
        {
            dealerCarDetails = new DealerCarDetails();
            carAccessories = new CarAccessories();
        }
        public virtual int CarAccessoriesId { get; set; }
        public virtual int DealerCarDetailsId { get; set; }
        public virtual bool IsAccessoriesAvailable { get; set; }

        public virtual DealerCarDetails dealerCarDetails { get; set; }
        public virtual CarAccessories carAccessories { get; set; }
       

    }
}
