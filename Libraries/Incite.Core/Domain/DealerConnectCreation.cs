﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class DealerConnectCreation:Entity<int>
    {
        public DealerConnectCreation()
        {
            //User = new AppUsers();
        }
        public virtual int AppUserId { get; set; } //Dealer ID
        public virtual string ActId { get; set; }
        public virtual string ExternalId { get; set; }
        public virtual bool IsVerified { get; set; }

        public virtual AppUsers User { get; set; }
    }
}
