﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class DriveType : Entity<int>
    {
        public DriveType()
        {
            carDetails = new List<CarDetails>();
        }
        public virtual string DriveTypeName { get; set; }

        public virtual IList<CarDetails> carDetails { get; set; }
    }
}
