﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class AppUsers : Entity<int>
    {
        public AppUsers()
        {
            UsersRoles = new List<AppUsersRole>();
            this.UserGuid = Guid.NewGuid();
            dealerCarDetails = new List<DealerCarDetails>();
            DealerComments = new List<DealerCommentNRating>();
            //dealerCustomerCreation = new DealerCustomerCreation();
            //dealerConnectCreation = new DealerConnectCreation();
        }
        public virtual string UserName { get; set; }
        public virtual string Password { get; set; }
        public virtual string Email { get; set; }
        public virtual bool IsEmailConfirmed { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual bool IsLockout { get; set; }
        public virtual DateTime? LockoutDate { get; set; }
        public virtual Guid UserGuid { get; set; }
        //By default flag= 1, Sent Reset Password link flag=0, Reset Successfully flag=1
        public virtual bool IsResetPassword { get; set; }

        public virtual IList<AppUsersRole> UsersRoles { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual IList<DealerCarDetails> dealerCarDetails { get; set; }
        public virtual IList<DealerCommentNRating> DealerComments { get; set; }

        public virtual DealerCustomerCreation dealerCustomerCreation { get; set; }

        public virtual IList<DealerConnectCreation> dealerConnectCreation { get; set; }
    }
}