﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public  class Transmission : Entity<int>
    {
        public Transmission()
        {
            carDetails = new List<CarDetails>();
        }
        
        public virtual string TransmissionName { get; set; }

        public virtual IList<CarDetails> carDetails { get; set; }
    }
}
