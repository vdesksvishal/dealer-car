﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class CarOrderDetail : Entity<int>
    {
        public CarOrderDetail()
        {

        }

        public virtual int DealerCarDetailId { get; set; }
        public virtual int BuyerID { get; set; }
        public virtual int SellerID { get; set; }
        public virtual int TransactionType { get; set; }
        public virtual int PaymentStatus { get; set; }
        public virtual string TransactionId { get; set; }
        public virtual bool IsOnHold { get; set; }

        public virtual DealerCarDetails dealerCarDetails { get; set; }

        public virtual AppUsers BuyerDealer { get; set; }
        public virtual AppUsers SellerDealer { get; set; }



    }
}
