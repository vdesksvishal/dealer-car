﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class DealerCustomerCreation: Entity<int>
    {
        public DealerCustomerCreation()
        {
            CustomerPaymentTypes = new List<CustomerPaymentTypes>();
        }
        public virtual int AppUserId { get; set; } //Dealer ID
        public virtual string CustomerId { get; set; }
        public virtual bool IsVerified { get; set; }

        public virtual AppUsers User { get; set; }
        public virtual IList<CustomerPaymentTypes> CustomerPaymentTypes { get; set; }
    }
}
