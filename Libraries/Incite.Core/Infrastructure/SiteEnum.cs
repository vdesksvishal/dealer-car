﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Infrastructure
{
    public class SiteEnum
    {
        public enum Roles
        {
            Dealer = 1,
            Admin = 2,
        }
        public enum CarDistance
        {
            AllMiles = 1,
            Miles10 = 2,
            Miles20 = 3,
            Miles30 = 4,
            Miles40 = 5,
            Miles50 = 6,
            Miles75 = 7,
            Miles100 = 8,
            Miles150 = 9,
            Miles200 = 10,
            Miles300 = 11,
            Miles500 = 12


        }
        public enum CarMilage
        {
            AnyMiles = 1,
            Under15000 = 2,
            Under30000 = 3,
            Under45000 = 4
        }
        public enum CarType
        {
            New = 1,
            Used = 2,
            Certified = 3
        }
        public enum EngineType
        {
            Gasoline = 1,
            Diesel = 2,
            Electric = 3,
            Hybrid = 4
        }

        public enum TitleType
        {
            Attached = 1,
            OnHand = 2,
            Reconstructed = 3
        }

        public enum CarConditionType
        {
            RetailReady = 1,
            WorkshopRequired =2,
            TransmissionIssue = 3,
            EngineIssue = 4,
            Scrap = 5
        }

        public enum PriceType
        {
            DealerPrice = 1,
            RetailPrice = 2,
            FixPrice = 3
        }

        public enum PageSetting
        {
            PageSize = 25
        }

        public enum CustomerPaymentType
        {
            BankAccount=1,
            CreditCard=2
        }
    }
}
