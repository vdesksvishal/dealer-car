﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Infrastructure
{
    public static class SiteConstants
    {
        public static string ConfirmEmailURL = "{0}/Account/ConfirmEmail/?uid={1}";

        public static string ForgotPasswordEmailURL = "{0}/?uid={1}";

        #region Message
        public static string Registration_Success = "Registration Successfull! Email is sent to your given email address, please confirm.";
        public static string DealerExist = "Dealer Already Exists";

        public static string Insert_Success = "{0} Inserted Successfully.";
        public static string Insert_Failure = "{0} Insertion Failed,Try Again.";

        public static string Update_Success = "{0} Updated Successfully.";
        public static string Update_Failure = "{0} Update Failed,Try Again.";

        public static string Delete_Success = "{0} Deleted Successfully.";
        public static string Delete_Failure = "{0} Delete Failed,Try Again.";

        public static string VIN_AVAILABLE = "VIN number already exists.";

        public static string ForgotPassword_Success = "Forgot Password - link sent to your given email address, please confirm.";
        public static string ResetPassword_AlreadyResetPassword = "You already reset your password.";

        public static string Bankverify_Success = "Bank Account verfied";
        public static string Bankverify_Failure = "Failed to verify bank account";
        public static string Bankaccount_Success = "Bank Account has been added Successfully!";
        public static string Bankaccount_Failure = "Failed to add bank account";

        public static string BankPayment_Success = "Payment done successfully!";
        public static string BankPayment_Failure = "Failed to process payment";

        public static string DealerCarSold_Success = "{0} Sold Successfully.";
        public static string DealerCarSold_Failure = "{0} Failed, Try Again.";
        #endregion

        #region Price Range
        public static string CAR_PRICE_MIN_RANGE = "0";
        public static string CAR_PRICE_MAX_RANGE = "200000";
        public static string PRICE_SLIDER_STEP = "1000";
        #endregion

        #region Year Range
        public static string YEAR_MIN_RANGE = "1900";
        public static string YEAR_MAX_RANGE = DateTime.UtcNow.Year.ToString();
        public static string YEAR_SLIDER_STEP = "1";
        #endregion
    }
}
