﻿using Incite.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Extention
{
    public static class MyExtentionMethods
    {
        public static string GetEnumName(this int _enumval,dynamic _enumtype)
        {
            return Enum.GetName(_enumtype, _enumval)??string.Empty;
        }
        public static string DecimalFormat(this decimal myNumber)
        {
            var s = string.Format("{0:0.000}", myNumber);

            if (s.EndsWith("00"))
            {
                return ((int)myNumber).ToString();
            }
            else
            {
                return s;
            }
        }

        public static long ConvertDateTimeToTimestamp(DateTime value)
        {
            long epoch = (value.Ticks - 621355968000000000) / 10000000;
            return epoch;
        }
    }
}
