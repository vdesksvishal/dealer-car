﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services.ViewModels
{
    public class VerifyBankAccount
    {
        [Required(ErrorMessage = "Debit Amount is required")]
        public int VerifyDebitAmount { get; set; }
        [Required(ErrorMessage = "Credit Amount is required")]
        public int VerifyCreditAmount { get; set; }

        public string Message { get; set; }
    }
}
