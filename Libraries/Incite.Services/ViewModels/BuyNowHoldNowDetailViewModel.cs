﻿using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services.ViewModels
{
    public class BuyNowHoldNowDetailViewModel
    {
        public int BuyerId { get; set; }
        public int SellerId { get; set; }
        public string Amount { get; set; }
        public int DealerCarDetailId { get; set; }
        public List<CustomerPaymentTypes> PaymentTypes { get; set; }
        [Required(ErrorMessage = "Please select any value")]
        public Nullable<int> PaymentTypeId { get; set; }
        public Nullable<int> PayId { get; set; }
        public bool IsOnHold { get; set; }



    }
}
