﻿using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Incite.Services.ViewModels
{
    public class UserProfileViewModel
    {
        public UserProfileViewModel()
        {
            StateList = new List<SelectListItem>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        [Required(ErrorMessage = "Mobile number is required")]
        public string MobileNumber { get; set; }
        public string AdditionalMobileNumber { get; set; }

        [EmailAddress]
        [Display(Name = "Additional Email Adress")]
        public string AdditionalEmail { get; set; }

        [Required(ErrorMessage = "Admin name is required")]
        public string AdminName { get; set; }

       
        public string ProfileImage { get; set; }
        public string ProfileThmbnailImage { get; set; }

        public AppUsers User { get; set; }

        [Required(ErrorMessage = "License Number is required")]
        public string LicenseNumber { get; set; }

        [Required(ErrorMessage = "Dealership addressline1 is required")]
        public string DealershipAddressLine1 { get; set; }
        public string DealershipAddressLine2 { get; set; }


        [Required(ErrorMessage = "Please select state")]
        public int State { get; set; }

        [Required(ErrorMessage = "Zipcode is required")]
        [Display(Name = "Zipcode")]
        [StringLength(5, ErrorMessage = "Zipcode must be 5 digit long", MinimumLength = 5)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Zipcode must be numeric only")]
        public string Zipcode { get; set; }

        [Required(ErrorMessage = "City is required")]
        public string City { get; set; }

        [Required(ErrorMessage = "Fax is required")]
        public string FAX { get; set; }

        public HttpPostedFileBase UploadProfilePicture { get; set; }
        public List<SelectListItem> StateList { get; set; }
    }
}
