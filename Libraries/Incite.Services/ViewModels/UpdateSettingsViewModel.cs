﻿using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Incite.Services.ViewModels
{
    public class UpdateSettingsViewModel
    {
        public UpdateSettingsViewModel()
        {

        }
        public int AppUserId { get; set; }
        public UserProfile Userprofile { get; set; }
        public Stripe.BankAccountOptions BankAccount { get; set; }
        [Required(ErrorMessage = "Account Holder Name is required")]
        public string AccountHolderName { get; set; }
        [Required(ErrorMessage = "Account Holder Type is required")]
        public string AccountHolderType { get; set; }
        [Required(ErrorMessage = "Account Holder Number is required")]
        public string AccountNumber { get; set; }
        [Required(ErrorMessage = "Country is required")]
        public string Country { get; set; }
        [Required(ErrorMessage = "Currency is required")]
        public string Currency { get; set; }
        [Required(ErrorMessage = "Routing Number is required")]
        public string RoutingNumber { get; set; }
        public string CustomerEmailId { get; set; }
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }
        public string Message { get; set; }
        

    }
}
