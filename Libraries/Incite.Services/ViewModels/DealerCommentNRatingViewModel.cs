﻿using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services.ViewModels
{
    public class DealerCommentNRatingViewModel
    {
        public int Id { get; set; }
        public int DealerId { get; set; }
        public int RatingForDealerId { get; set; }
        public  string Comment { get; set; }
        public  string CommentReply { get; set; }
        public  int Rating { get; set; }
        public  DateTime CreatedTs { get; set; }
    }
}
