﻿using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Incite.Services.ViewModels
{
    public class DealerCarViewModel : ProcessViewModel
    {
        public DealerCarViewModel()
        {
            LocationList = new List<SelectListItem>();
            CarbrandList = new List<CarMaker>();
            CarModelList = new List<CarModel>();
            CarYearList = new List<CarYear>();
            CarTypeList = new List<SelectListItem>();
            CarConditionTypeList = new List<SelectListItem>();
            TitleTypeList = new List<SelectListItem>();
            EngineTypeList = new List<SelectListItem>();
            trimpropList = new List<SelectListItem>();
            fueltypeList = new List<FuelType>();
            drivetrainList = new List<DriveType>();
            transmisionList = new List<Transmission>();
            Accessories = new List<CarAccessoriesViewModel>();
            DealerCarDetailsList = new List<DealerCarDetails>();
            DistanceList = new List<SelectListItem>();
            MilageList = new List<SelectListItem>();
            DisplayCarImagesList = new List<DealerCarImages>();

        }

        #region Filter Properties
        public Nullable<int> Loc { get; set; }
        public List<SelectListItem> LocationList { get; set; }
        public Nullable<int> CB { get; set; }
        public List<CarMaker> CarbrandList { get; set; }
        public Nullable<int> CM { get; set; }
        public List<CarModel> CarModelList { get; set; }

        public List<CarYear> CarYearList { get; set; }

        public Nullable<int> CT { get; set; }
        public List<SelectListItem> CarTypeList { get; set; }
        public List<SelectListItem> TitleTypeList { get; set; }
        public List<SelectListItem> CarConditionTypeList { get; set; }
        public string PR { get; set; }
        public string YR { get; set; }

        public List<DealerCarImages> DisplayCarImagesList { get; set; }
        public Nullable<int> ML { get; set; }
        public List<SelectListItem> MilageList { get; set; }

        public Nullable<int> Dst { get; set; }
        public List<SelectListItem> DistanceList { get; set; }
        #endregion

        #region Form Fields
        public int Id { get; set; }
        public int CarDetailId { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string vehicalorcview { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string EngineName { get; set; }

        [Required(ErrorMessage = "Please select any value")]
        public string trimprop { get; set; }

        [Required(ErrorMessage = "Please select any value")]
        public string BodyType { get; set; }

        public List<SelectListItem> trimpropList { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression(@"^[0-9]\d*(\.\d{1,3})?%?$", ErrorMessage = "Milage must be numeric and can't have more than 3 decimal places")]
        public decimal miles { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string fueltypeName { get; set; }
        public Nullable<int> fueltype { get; set; }
        public List<FuelType> fueltypeList { get; set; }

        [Required(ErrorMessage = "Please select any value")]
        public int price_type { get; set; }
        public List<SelectListItem> price_typeList { get; set; }

        [Required(ErrorMessage = "Please select any value")]
        public int ownership_type { get; set; }

        [Required(ErrorMessage = "Please select any value")]
        public int title_type { get; set; }

        [Required(ErrorMessage = "Please select any value")]
        public int carCondition_type { get; set; }

        [Required(ErrorMessage = "Please select any value")]
        public int modelyear { get; set; }
        public List<SelectListItem> modelyearList { get; set; }

        [Required(ErrorMessage = "Please select any value")]
        public string engine { get; set; }
        public List<SelectListItem> EngineTypeList { get; set; }

        public Nullable<int> drivetrain { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string drivetrainName { get; set; }
        public List<DriveType> drivetrainList { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Remote("DuplicationCheckForVIN", AdditionalFields = "PreviousVIN")]
        public string vin { get; set; }

        public Nullable<int> transmision { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string transmisionName { get; set; }
        public List<Transmission> transmisionList { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression(@"^[0-9]\d*(\.\d{1,3})?%?$", ErrorMessage = "Price must be numeric and can't have more than 3 decimal places")]
        public decimal priceperday { get; set; }

        [Required(ErrorMessage = "Please select any value")]
        public Nullable<int> carbrand { get; set; }

        [Required(ErrorMessage = "Please select any value")]
        public Nullable<int> carmodel { get; set; }

        public int? AppUserId { get; set; }
        public HttpPostedFileBase[] CarImages { get; set; }
        public IList<CarAccessoriesViewModel> Accessories { get; set; }
        public List<SelectListItem> CarAccessoriesList { get; set; }
        public IList<DealerCarDetails> DealerCarDetailsList { get; set; }

        #endregion
    }

    public class CarAccessoriesViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsAccessoriesAvailable { get; set; }
    }

    public class DisplayCarImages
    {
        public int CarImageId { get; set; }
        public string NormalImagePath { get; set; }
        public string ThumbImagePath { get; set; }
        public string LargeImagePath { get; set; }
    }
}
