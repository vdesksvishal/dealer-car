﻿using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Incite.Services;
using System.Web.Mvc;
using Trialcar.Web.Helper;

namespace Incite.Services.ViewModels
{
    public class AddConnectAccountsViewModel
    {
        public AddConnectAccountsViewModel()
        {
            StateList = DropDownListHelper.BindStateList();
        }
        public int AppUserId { get; set; }
        public UserProfile Userprofile { get; set; }
        public string AccountId { get; set; }
        [Required(ErrorMessage = "Account Holder Name is required")]
        public string AccountHolderName { get; set; }
        [Required(ErrorMessage = "Account Holder Type is required")]
        public string AccountHolderType { get; set; }
        [Required(ErrorMessage = "Account Holder Number is required")]
        public string AccountNumber { get; set; }
        [Required(ErrorMessage = "Routing Number is required")]
        public string RoutingNumber { get; set; }
        public string CustomerEmailId { get; set; }
        [Required(ErrorMessage = "Address Line1 is required")]
        public string AddressLine1 { get; set; } // "1234 Main Street";
        [Required(ErrorMessage = "Address Line2 is required")]
        public string AddressLine2 { get; set; } // "1234 Main Street";
        [Required(ErrorMessage = "Address Postal Code Line1 is required")]
        public string AddressPostalCode { get; set; } // = "94111";
        [Required(ErrorMessage = "Address City is required")]
        public string AddressCity { get; set; } // = "San Francisco";
        [Required(ErrorMessage = "Address State is required")]
        public string AddressState { get; set; } // = "CA";
        [Required(ErrorMessage = "SSN Last-4 is required")]
        public string SSNLast4 { get; set; } // = "123456789";
        public string IP { get; set; }
        [Required(ErrorMessage = "Please select state")]
        public int State { get; set; }
        public List<SelectListItem> StateList { get; set; }
        [Required(ErrorMessage = "Tax-Id is required")]
        public string EIN { get; set; }
        [Required(ErrorMessage = "PhoneNumber is required")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "Representative's First Name is required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Representative's Last Name is required")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Business Website is required")]
        public string URL { get; set; }
        [Required(ErrorMessage = "Date of Birth is required")]
        public DateTime Dob2 { get; set; }


    }
}
