﻿using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Incite.Services.ViewModels
{
    public class CarFilterViewModel
    {
        public CarFilterViewModel()
        {
            
            DistanceList = new List<SelectListItem>();
            CarMakerList = new List<SelectListItem>();
            CarModelList = new List<SelectListItem>();
            MilageList = new List<SelectListItem>();
            CarTypeList = new List<SelectListItem>();
            LocationList = new List<SelectListItem>();
            drivetrainList = new List<SelectListItem>();
            TranmissionTypeList=new List<SelectListItem>();
            PriceTypeList = new List<SelectListItem>();
        }

        public Nullable<int> Loc { get; set; }
        public List<SelectListItem> LocationList { get; set; }
        public Nullable<int> CB { get; set; }
        public List<SelectListItem> CarMakerList { get; set; }
        public Nullable<int> CM { get; set; }
        public List<SelectListItem> CarModelList { get; set; }

        public Nullable<int> CT { get; set; }
        public List<SelectListItem> CarTypeList { get; set; }
        public string PR { get; set; }
        public string YR { get; set; }

        public Nullable<int> ML { get; set; }
        public List<SelectListItem> MilageList { get; set; }

        public Nullable<int> Dst { get; set; }
        public List<SelectListItem> DistanceList { get; set; }
        
        public List<SelectListItem> Sortbylist { get; set; }

        public Nullable<int> Sb { get; set; }

        public List<SelectListItem> drivetrainList { get; set; }
        public Nullable<int> DT { get; set; }
        
        public List<SelectListItem> TranmissionTypeList { get; set; }
        public Nullable<int> TT { get; set; }
        public List<SelectListItem> PriceTypeList { get; set; }

        public Nullable<int> PT { get; set; }

    }
}
