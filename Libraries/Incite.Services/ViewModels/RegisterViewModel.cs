﻿using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
//using System.Web.WebPages.Html;

namespace Incite.Services.ViewModels
{
    public class RegisterViewModel
    {
        public RegisterViewModel()
        {
            StateList = new List<SelectListItem>();
        }

        [Required(ErrorMessage = "Dealership name is required")]
        public string DealershipName { get; set; }

        [Required(ErrorMessage = "License number is required")]
        public string LicenseNumber { get; set; }


        [Required(ErrorMessage = "Mobile number is required")]
        [StringLength(10, ErrorMessage = "Mobile Number must be 10 digit long", MinimumLength = 10)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Muobile Number must be numeric")]
        public string MobileNumber { get; set; }

        [StringLength(10, ErrorMessage = "Mobile Number must be 10 digit long", MinimumLength = 10)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Muobile Number must be numeric")]
        public string AdditionalMobileNumber { get; set; }

        [Required(ErrorMessage = "Email address is required")]
        [EmailAddress]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [EmailAddress]
        [Display(Name = "Additional Email Adress")]
        public string AdditionalEmailAdress { get; set; }

        [Required(ErrorMessage = "Dealership addressline1 is required")]
        public string DealershipAddressLine1 { get; set; }

        public string DealershipAddressLine2 { get; set; }

        public string FAX { get; set; }

        [Required(ErrorMessage = "Admin name is required")]
        public string AdminName { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(25, ErrorMessage = "Must be between 6 and 25 characters", MinimumLength = 6)]
        [RegularExpression("^(?=.*\\d).{6,25}$", ErrorMessage = " Password must be between 6 and 25 characters long and include at least one numeric digit.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm password is required")]
        [StringLength(255, ErrorMessage = "Must be between 6 and 25 characters", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Zipcode is required")]
        [Display(Name = "Zipcode")]
        [StringLength(5, ErrorMessage = "Zipcode must be 5 digit long", MinimumLength = 5)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Zipcode must be numeric only")]

        public string ZIPCODE { get; set; }

        [Required(ErrorMessage = "Please select state")]
        public int State { get; set; }
        public List<SelectListItem> StateList { get; set; }

        [Required(ErrorMessage = "City is required")]
        public string City { get; set; }

        public HttpPostedFileBase UploadProfilePicture { get; set; }
    }


}
