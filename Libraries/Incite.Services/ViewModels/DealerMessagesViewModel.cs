﻿using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services.ViewModels
{
    public class DealerMessagesViewModel
    {
        public int Id { get; set; }
        public int FromDealerId { get; set; }
        public int ToDealerId { get; set; }
        public  string Message { get; set; }
        public DateTime CreatedTs { get; set; }
        public string FromAction { get; set; }
    }
}
