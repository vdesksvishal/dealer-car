﻿using Incite.Core.Domain;
using Incite.Services.EzcmdHelper;
using Incite.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Incite.Services
{
    public interface IDealerCarService
    {
        List<CarMaker> CarmakerList();
        List<CarModel> CarModelList();
        DealerCarViewModel PrepareDealarCarViewModel(int carid = 0);
        ProcessViewModel AddOrUpdateCar(DealerCarViewModel model);
        List<CarModel> CarModelListbyMaker(int CarmakerVal);
        List<CarYear> CarYearList(int CarmakerVal, int carModelVal);
        List<SelectListItem> CarTrimPropList(int CarmakerVal, int carModelVal, int CarYearVal);
        List<FuelType> FuelTypeList();
        List<DriveType> DriveTypeList();
        List<Transmission> TransmissionList();
        CarDetails GetDetailbyTrim(int trim);
        IList<DealerCarDetails> GetDealerCarListing(long UserId, CarFilterViewModel filter, bool IsDealerSearch, bool IsSold);
        bool IsVinExist(string vinnumber);

        DealerCarDetails GetCarDetailById(int CId);

        AppUsers addOrUpdateDealerCarComment(DealerCommentNRatingViewModel dealerCommentNRating);
        void addOrUpdateDealerCarCommentReply(DealerCommentNRatingViewModel dealerCommentReply);

        int countDealerComments();
        VinApiViewModel GetCarDetailByVinApi(string vin);

        bool CarBuy(int cId, int usrrId);

        bool CarOnHold(int cId, int usrrId);
        List<CarAccessories> GetCarAccessories();

        void InsertMessage(DealerMessagesViewModel dealerMessagesVM);

        IList<DealerMessages> GetAllMessages(int currLoginDealerId, int dealerId);

        AppUsers getUserById(int id);

        IList<AppUsers> getDealerIdsOfMessages(int dealerId);

        int getUserIdByName(string userName);

        ProcessViewModel DeleteCarImage(string imageId);

        IList<DealerCarDetails> GetAllDealerCarListing();

        ProcessViewModel CarSold(int CId);
        List<MileFilterRecored> Getnerbyzip(string rang, AppUsers CurrentUsers);
    }
}
