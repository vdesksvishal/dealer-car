﻿using Incite.Core.Domain;
using Incite.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services
{
    public interface IEmailTemplateService
    {
       
        bool ProcessedToEmail(AppUsers model);

        bool ProcessedToEmailForgotPassword(AppUsers model);

        bool ProcessedToEmailUserActivate(AppUsers model);
    }
}
