﻿using Incite.Core.Domain;
using Incite.Services.ViewModels;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services
{
    public interface IPaymentService
    {
        #region Stripe's Create/Get Customer Account
        ProcessViewModel CreateCustomer(UpdateSettingsViewModel model);
        ProcessViewModel VerifyCustomer(int AppUserId, VerifyBankAccount model);
        StripeList<IPaymentSource> GetUserPaymentSourceList(string UserEmail);
        #endregion

        #region Stripe's Create/Get Connect Account
        StripeList<IExternalAccount> GetConnectUserPaymentSourceList(string UserEmail);
        ProcessViewModel CreateConnect(AddConnectAccountsViewModel model);
        #endregion

        #region Common Methods
        List<CustomerPaymentTypes> GetCustomerPaymentTypes(int BuyerId);

        ProcessViewModel ProcessBuyNowHoldNow(BuyNowHoldNowDetailViewModel model);
        #endregion
    }
}
