﻿using Incite.Core.Domain;
using Incite.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services
{
    public interface IUsersService
    {
        List<AppUsers> GetAllUsers();
        void InsatllRecored();

        ProcessViewModel NewUser(RegisterViewModel model);
        UserViewModel ValidateUser(string email, string Password);
        bool ConfirmEmail(Guid uid);

        UserProfileViewModel GetUserProfile(int currentUSerId);

        ProcessViewModel UpdateUserProfile(UserProfileViewModel model, int currentUSerId);

        ProcessViewModel UpdatePassword(UpdatePasswordViewModel model, int currentUSerId);

        ProcessViewModel ForgotPassword(string email);

        ProcessViewModel ResetPassword(ResetPasswordViewModel model);

        ProcessViewModel UpdateUserStatus(string userguid, string status);

        ProcessViewModel UpdateUserLockoutStatus(string userguid, string status);
        
    }
}
