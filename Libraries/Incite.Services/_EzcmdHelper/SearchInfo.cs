﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services.EzcmdHelper
{
    public class SearchInfo
    {
        public string zip_code { get; set; }
        public string country_code { get; set; }
        public string distance_radius { get; set; }
        public string unit { get; set; }
        public int records_total { get; set; }
        public int records_start { get; set; }
        public string records_limit { get; set; }
    }
}
