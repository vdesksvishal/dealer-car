﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services.EzcmdHelper
{
    public class SearchResult
    {
        public string id { get; set; }
        public string country_code { get; set; }
        public string country_name { get; set; }
        public string postal_code { get; set; }
        public string place_name { get; set; }
        public string place_name_string { get; set; }
        public string state { get; set; }
        public string province { get; set; }
        public string community { get; set; }
        public string coords { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
        public object _version_ { get; set; }
        public string address { get; set; }
        public double distance { get; set; }
    }
}
