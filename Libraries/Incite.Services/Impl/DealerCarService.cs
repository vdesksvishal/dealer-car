﻿using Incite.Core.Domain;
using Incite.Core.Extention;
using Incite.Core.Infrastructure;
using Incite.Data;
using Incite.Data.Repositories;
using Incite.Services.EzcmdHelper;
using Incite.Services.ViewModels;
using NHibernate;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Trialcar.Web.Helper;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Incite.Services
{
    public class DealerCarService : IDealerCarService
    {
        #region Private Variable
        private readonly ICarMakerRepository _CarMakerRepository;
        private readonly ICarModelRepository _CarModelRepository;
        private readonly ICarYearRepository _CarYearRepository;
        private readonly IDealerCarDetailsRepository _DealerCarDetailsRepository;
        private readonly IAppUsersRepository _appUsersRepository;
        private readonly ICarDetailsRepository _carDetailsRepository;
        private readonly IEngineTypeRepository _engineTypeRepository;
        private readonly IFuelTypeRepository _fuelTypeRepository;
        private readonly IDriveTypeRepository _driveTypeRepository;
        private readonly ITransmissionRepository _transmissionRepository;
        private readonly ICarAccessoriesRepository _carAccessoriesRepository;
        private readonly IDealerCarAccessoriesRepository _dealerCarAccessoriesRepository;
        private readonly IDealerCarImagesRepository _dealerCarImagesRepository;
        private readonly IDealerCommentNRatingRepository _dealerCommentNRatingRepository;
        private readonly IMileFilterDataRepository _mileFilterDataRepository;
        private readonly ICarOrderDetailRepository _carOrderDetailRepository;
        private readonly ICarOrderOnHoldRepository _carOrderOnHoldRepository;
        private readonly IDealerMessagesRepository _dealerMessagesRepository;
        private readonly IUserProfileRepository _userProfileRepository;
        #endregion

        #region ctor
        public DealerCarService(ICarMakerRepository CarMakerRepository
            , ICarModelRepository CarModelRepository
            , IDealerCarDetailsRepository DealerCarDetailsRepository
            , IAppUsersRepository appUsersRepository
            , ICarDetailsRepository carDetailsRepository
            , IEngineTypeRepository engineTypeRepository
            , ICarYearRepository carYearRepository
            , IFuelTypeRepository fuelTypeRepository
            , IDriveTypeRepository driveTypeRepository
            , ITransmissionRepository transmissionRepository
            , ICarAccessoriesRepository carAccessoriesRepository
            , IDealerCarAccessoriesRepository dealerCarAccessoriesRepository
            , IDealerCarImagesRepository dealerCarImagesRepository
            , IDealerCommentNRatingRepository dealerCommentNRatingRepository
            , IMileFilterDataRepository mileFilterDataRepository
            , ICarOrderDetailRepository carOrderDetailRepository
            , ICarOrderOnHoldRepository carOrderOnHoldRepository
            , IDealerMessagesRepository dealerMessagesRepository
            , IUserProfileRepository userProfileRepository)
        {
            _CarMakerRepository = CarMakerRepository;
            _CarModelRepository = CarModelRepository;
            _DealerCarDetailsRepository = DealerCarDetailsRepository;
            _appUsersRepository = appUsersRepository;
            _carDetailsRepository = carDetailsRepository;
            _engineTypeRepository = engineTypeRepository;
            _CarYearRepository = carYearRepository;
            _fuelTypeRepository = fuelTypeRepository;
            _driveTypeRepository = driveTypeRepository;
            _transmissionRepository = transmissionRepository;
            _carAccessoriesRepository = carAccessoriesRepository;
            _dealerCarAccessoriesRepository = dealerCarAccessoriesRepository;
            _dealerCarImagesRepository = dealerCarImagesRepository;
            _dealerCommentNRatingRepository = dealerCommentNRatingRepository;
            _mileFilterDataRepository = mileFilterDataRepository;
            _carOrderDetailRepository = carOrderDetailRepository;
            _carOrderOnHoldRepository = carOrderOnHoldRepository;
            _dealerMessagesRepository = dealerMessagesRepository;
            _userProfileRepository = userProfileRepository;

        }
        #endregion

        #region Dropdown load Services
        [UnitOfWork]
        public List<CarMaker> CarmakerList()
        {
            var listitem = _CarMakerRepository.GetAll().ToList();
            return listitem;
        }

        [UnitOfWork]
        public List<CarModel> CarModelList()
        {
            var listitem = _CarModelRepository.GetAll().ToList();
            return listitem;

        }

        [UnitOfWork]
        public List<FuelType> FuelTypeList()
        {
            var listitem = _fuelTypeRepository.GetAll().ToList();
            return listitem;

        }

        [UnitOfWork]
        public List<Core.Domain.DriveType> DriveTypeList()
        {
            var listitem = _driveTypeRepository.GetAll().ToList();
            return listitem;

        }

        [UnitOfWork]
        public List<Transmission> TransmissionList()
        {
            var listitem = _transmissionRepository.GetAll().ToList();
            return listitem;

        }
        #endregion

        #region Prepare and Bind Model


        [UnitOfWork]
        public List<CarAccessories> GetCarAccessories()
        {
            var carAccessoriesList = _carAccessoriesRepository.GetAll().ToList();
            return carAccessoriesList;
        }

        public DealerCarViewModel PrepareDealarCarViewModel(int carid = 0)
        {
            DealerCarViewModel resulte = new DealerCarViewModel();
            resulte.LocationList = DropDownListHelper.BindLocationList();
            resulte.CarTypeList = DropDownListHelper.BindCarTypeList();
            resulte.TitleTypeList = DropDownListHelper.BindTitleTypeList();
            resulte.CarConditionTypeList = DropDownListHelper.BindCarConditionList();
            resulte.price_typeList = DropDownListHelper.BindPrice_TypeList();
            resulte.EngineTypeList = DropDownListHelper.BindenginetypeList();
            resulte.CarbrandList = this.CarmakerList();
            resulte.fueltypeList = this.FuelTypeList();
            resulte.drivetrainList = this.DriveTypeList();
            resulte.transmisionList = this.TransmissionList();
            resulte.DistanceList = DropDownListHelper.BindDistanceList();
            resulte.MilageList = DropDownListHelper.BindMilageList();
            if (carid > 0)
            {
                var _dealerCarDetails = _DealerCarDetailsRepository.Get(carid);
                if (_dealerCarDetails != null)
                {
                    resulte.CB = _dealerCarDetails.CarDetails.Maker.Id;
                    resulte.CM = _dealerCarDetails.CarDetails.Model.Id;
                    resulte.modelyear = _dealerCarDetails.CarDetails.Year.Id;
                    if (resulte.CB != null)
                        resulte.CarModelList = this.CarModelListbyMaker(resulte.CB ?? 0);
                    if (resulte.CB != null && resulte.CM != null)
                        resulte.CarYearList = this.CarYearList(resulte.CB ?? 0, resulte.CM ?? 0);
                    if (resulte.CB != null && resulte.CM != null && resulte.modelyear > 0)
                        resulte.trimpropList = this.CarTrimPropList(resulte.CB ?? 0, resulte.CM ?? 0, resulte.modelyear);
                    resulte.Id = _dealerCarDetails.Id;
                    resulte.vin = _dealerCarDetails.VIN;
                    resulte.EngineName = _dealerCarDetails.CarDetails.Engine;
                    resulte.BodyType = _dealerCarDetails.CarDetails.BodyType;

                    resulte.CB = _dealerCarDetails.CarDetails.Maker.Id;
                    resulte.CM = _dealerCarDetails.CarDetails.Model.Id;
                    resulte.modelyear = _dealerCarDetails.CarDetails.Year.Id;

                    resulte.vehicalorcview = _dealerCarDetails.VehicleOverview;
                    resulte.trimprop = Convert.ToString(_dealerCarDetails.CarDetails.Id);
                    resulte.miles = Convert.ToDecimal(MyExtentionMethods.DecimalFormat(_dealerCarDetails.Miles));

                    resulte.fueltype = _dealerCarDetails.CarDetails.FuelType.Id;
                    resulte.fueltypeName = _dealerCarDetails.CarDetails.FuelType.FuelTypeName;

                    resulte.price_type = _dealerCarDetails.PriceType;
                    resulte.ownership_type = _dealerCarDetails.OwnershipType;
                    resulte.title_type = _dealerCarDetails.TitleType;
                    resulte.carCondition_type = _dealerCarDetails.CarConditionType;

                    resulte.priceperday = Convert.ToDecimal(MyExtentionMethods.DecimalFormat(_dealerCarDetails.Price));
                    if (_dealerCarDetails != null && _dealerCarDetails.EngineType != null)
                        resulte.engine = Convert.ToString(_dealerCarDetails.EngineType.Id);

                    resulte.drivetrain = _dealerCarDetails.CarDetails.DriveType.Id;
                    resulte.drivetrainName = _dealerCarDetails.CarDetails.DriveType.DriveTypeName;

                    resulte.transmision = _dealerCarDetails.CarDetails.Transmission.Id;
                    resulte.transmisionName = _dealerCarDetails.CarDetails.Transmission.TransmissionName;

                    //var _dealerCarAccessories = _dealerCarAccessoriesRepository.GetAll(x => x.dealerCarDetails == _dealerCarDetails).ToList();
                    //if (_dealerCarAccessories.Any())
                    //    resulte.Accessories = _PrepareAccessoriesListByDealerCarId(_dealerCarAccessories);

                    resulte.CarDetailId = _dealerCarDetails.CarDetails.Id;
                    resulte.CarAccessoriesList = PrepareCarAccessoriesList(_dealerCarDetails.CarAccessories.ToList());

                    resulte.DisplayCarImagesList = GetDealerCarImages(_dealerCarDetails.Id);


                }

            }
            if (resulte.CarAccessoriesList == null || !resulte.CarAccessoriesList.Any())
                resulte.CarAccessoriesList = PrepareCarAccessoriesList();


            return resulte;

        }

        private List<DealerCarImages> GetDealerCarImages(int DealerId)
        {
            List<DealerCarImages> dealerCarImages = _dealerCarImagesRepository.GetAll().Where(x => x.dealerCarDetails.Id == DealerId).ToList();
            dealerCarImages = dealerCarImages.Where(x => x.IsDeleted == false).ToList();
            //List<DealerCarImages> ListCarImages = new List<DealerCarImages>();
            //DealerCarImages dealerCar = new DealerCarImages();
            ////List<string> ListCarImages = new List<string>();
            //if (dealerCarImages != null && dealerCarImages.Count() > 0)
            //{
            //    foreach (var dealerCarImage in dealerCarImages)
            //    {

            //    }
            //    //ListCarImages.AddRange(dealerCarImages.Select(x => new DealerCarImages() { NormalImagePath = x.NormalImagePath, LargeImagePath = x.LargeImagePath, ThumbImagePath =x.ThumbImagePath }));
            //    //ListCarImages.AddRange(dealerCarImages.Select(x => x.ThumbImagePath));

            //}
            return dealerCarImages;
        }

        private List<SelectListItem> PrepareCarAccessoriesList(List<DealerCarAccessories> list = null)
        {
            var allcarAccessories = _carAccessoriesRepository.GetAll();
            List<SelectListItem> result = new List<SelectListItem>();
            if (allcarAccessories != null)
            {
                result.AddRange(allcarAccessories.Select(x => new SelectListItem() { Text = x.Accessories, Value = x.Id.ToString(), Selected = false }));
            }
            if (list != null)
            {
                foreach (var carassessories in list)
                {
                    if (result.FirstOrDefault(x => Convert.ToInt32(x.Value) == carassessories.carAccessories.Id) != null)
                        result.FirstOrDefault(x => Convert.ToInt32(x.Value) == carassessories.carAccessories.Id).Selected = carassessories.IsAccessoriesAvailable;
                }
            }
            return result;

        }

        #endregion

        #region Controlelr Services
        public int countDealerComments()
        {
            return _dealerCommentNRatingRepository.GetAll().Count();
        }

        [UnitOfWork]
        public AppUsers addOrUpdateDealerCarComment(DealerCommentNRatingViewModel dealerCommentNRating)
        {
            DealerCommentNRating nRating = new DealerCommentNRating();
            var appuser = _appUsersRepository.Get(dealerCommentNRating.DealerId);
            var ratingUser = _appUsersRepository.Get(dealerCommentNRating.RatingForDealerId);
            //var cardetail = _DealerCarDetailsRepository.Get(dealerCommentNRating.DealerCarDetailsId);
            nRating.Id = dealerCommentNRating.Id;
            nRating.appUsers = appuser;
            nRating.ratingUsers = ratingUser;

            //nRating.dealerCarDetails = cardetail;
            nRating.Rating = dealerCommentNRating.Rating;
            nRating.Comment = dealerCommentNRating.Comment;
            nRating.CommentReply = dealerCommentNRating.CommentReply;
            nRating.CreatedTs = DateTime.Now;
            //nRating.DealerId = dealerCommentNRating.DealerId;
            //nRating.DealerCarDetailsId = dealerCommentNRating.DealerCarDetailsId;

            _dealerCommentNRatingRepository.InsertOrUpdate(nRating);

            return ratingUser;
        }

        [UnitOfWork]
        public void addOrUpdateDealerCarCommentReply(DealerCommentNRatingViewModel dealerCommentReply)
        {
            DealerCommentNRating nRating = new DealerCommentNRating();
            var appuser = _appUsersRepository.Get(dealerCommentReply.DealerId);
            var ratingUser = _appUsersRepository.Get(dealerCommentReply.RatingForDealerId);
            //var cardetail = _DealerCarDetailsRepository.Get(dealerCommentReply.DealerCarDetailsId);
            nRating.appUsers = appuser;
            nRating.ratingUsers = ratingUser;
            //nRating.dealerCarDetails = cardetail;
            nRating.Id = dealerCommentReply.Id;
            nRating.Comment = dealerCommentReply.Comment;
            nRating.Rating = dealerCommentReply.Rating;
            nRating.CommentReply = dealerCommentReply.CommentReply;
            nRating.CreatedTs = dealerCommentReply.CreatedTs;

            _dealerCommentNRatingRepository.InsertOrUpdate(nRating);
        }

        public DealerCarDetails GetCarDetailById(int CId)
        {
            DealerCarDetails _dealerCarDetails = _DealerCarDetailsRepository.Get(CId);
            return _dealerCarDetails;
        }
        public VinApiViewModel GetCarDetailByVinApi(string vin)
        {
            VinApiViewModel result = null;
            var dbCar = _DealerCarDetailsRepository.Get(x => x.VIN == vin);
            if (dbCar == null)
            {
                string url = "https://vpic.nhtsa.dot.gov/api/vehicles/decodevinvaluesextended/" + vin + "?format=json";
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    var tmp = client.GetAsync(url).Result;
                    if (tmp.IsSuccessStatusCode)
                    {
                        try
                        {
                            var jsonresult = tmp.Content.ReadAsStringAsync();
                            _VpicHelper.RootObject rootObject = JsonConvert.DeserializeObject<_VpicHelper.RootObject>(jsonresult.Result);
                            var apiresult = rootObject.Results.FirstOrDefault();
                            result = new VinApiViewModel();
                            result.Make = apiresult.Make;
                            result.Model = apiresult.Model;
                            result.ModelYear = apiresult.ModelYear;
                            result.Series = apiresult.Series;
                            if (!string.IsNullOrEmpty(result.Make))
                            {
                                var getmaker = _CarMakerRepository.Get(x => x.Name.ToLowerInvariant() == result.Make.ToLowerInvariant());
                                if (getmaker != null)
                                    result.MakerId = getmaker.Id;
                            }
                            if (!string.IsNullOrEmpty(result.Model))
                            {
                                var getmodel = _CarModelRepository.Get(x => x.Name.ToLowerInvariant() == result.Model.ToLowerInvariant());
                                if (getmodel != null)
                                    result.ModelId = getmodel.Id;
                            }
                            if (!string.IsNullOrEmpty(result.ModelYear))
                            {
                                var getyear = _CarYearRepository.Get(x => x.Name.ToString().ToLowerInvariant() == result.ModelYear.ToLowerInvariant());
                                if (getyear != null)
                                    result.YearId = getyear.Id;
                            }


                        }
                        catch (Exception ex)
                        {

                        }
                        // return Request.CreateResponse(HttpStatusCode.OK, result.Result, Configuration.Formatters.JsonFormatter);
                    }
                }
                catch (Exception err)
                {
                    // return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error", Configuration.Formatters.JsonFormatter);
                }
            }
            else
            {
                result = new VinApiViewModel();
                result.Make = dbCar.CarDetails.Maker.Name;
                result.Model = dbCar.CarDetails.Model.Name;
                result.ModelYear = dbCar.CarDetails.Year.Name.ToString();
                result.Series = dbCar.CarDetails.Trim;
                result.MakerId = dbCar.CarDetails.Maker.Id;
                result.ModelId = dbCar.CarDetails.Model.Id;
                result.YearId = dbCar.CarDetails.Year.Id;
                result.TrimId = dbCar.CarDetails.Id;
                result.EngineId = dbCar.EngineTypeId;
            }

            return result;
        }


        [UnitOfWork]
        public ProcessViewModel AddOrUpdateCar(DealerCarViewModel model)
        {
            ProcessViewModel result = new ProcessViewModel();
            if (model.Id <= 0)
                if (IsVinExist(model.vin))
                {
                    result.IsSuccess = false;
                    result.Message = "Car already exists";
                    return result;
                }

            DealerCarDetails _dealerCarDetails;
            if (model.Id <= 0)
                _dealerCarDetails = new DealerCarDetails();
            else
                _dealerCarDetails = _DealerCarDetailsRepository.Get(model.Id);

            var user = _appUsersRepository.Get(model.AppUserId.Value);
            var cardetail = _carDetailsRepository.Get(model.CarDetailId);
            int enginetypeId = Convert.ToInt16(model.engine);
            var enginetypeDetail = _engineTypeRepository.Get(x => x.Id == enginetypeId);

            _dealerCarDetails.CarDetails = cardetail;
            _dealerCarDetails.EngineType = enginetypeDetail;
            _dealerCarDetails.User = user;
            _dealerCarDetails.OwnershipType = model.ownership_type;
            _dealerCarDetails.TitleType = model.title_type;
            _dealerCarDetails.CarConditionType = model.carCondition_type;
            _dealerCarDetails.PriceType = model.price_type;
            _dealerCarDetails.VehicleOverview = model.vehicalorcview;
            _dealerCarDetails.VIN = model.vin;
            _dealerCarDetails.CreatedTs = DateTime.UtcNow;
            _dealerCarDetails.Miles = model.miles;
            _dealerCarDetails.Price = model.priceperday;





            if (model.Id <= 0)
            {
                foreach (var Accessories in model.CarAccessoriesList)
                {
                    var carAccessorie = _carAccessoriesRepository.Get(Convert.ToInt16(Accessories.Value));
                    if (carAccessorie != null)
                    {
                        DealerCarAccessories _dealerCarAccessories = new DealerCarAccessories();
                        _dealerCarAccessories.carAccessories = carAccessorie;
                        _dealerCarAccessories.dealerCarDetails = _dealerCarDetails;
                        _dealerCarAccessories.IsAccessoriesAvailable = Accessories.Selected;
                        _dealerCarAccessoriesRepository.Insert(_dealerCarAccessories);
                    }
                }
            }
            else
            {
                foreach (var Accessories in model.CarAccessoriesList)
                {
                    var getcurrentAccessories = _dealerCarDetails.CarAccessories.FirstOrDefault(x => x.carAccessories.Id == Convert.ToInt16(Accessories.Value));
                    if (getcurrentAccessories != null)
                    {
                        getcurrentAccessories.IsAccessoriesAvailable = Accessories.Selected;
                        _dealerCarAccessoriesRepository.Update(getcurrentAccessories);
                    }
                    else
                    {
                        var carAccessorie = _carAccessoriesRepository.Get(Convert.ToInt16(Accessories.Value));
                        if (carAccessorie != null)
                        {
                            DealerCarAccessories _dealerCarAccessories = new DealerCarAccessories();
                            _dealerCarAccessories.carAccessories = carAccessorie;
                            _dealerCarAccessories.dealerCarDetails = _dealerCarDetails;
                            _dealerCarAccessories.IsAccessoriesAvailable = Accessories.Selected;
                            _dealerCarAccessoriesRepository.Insert(_dealerCarAccessories);
                        }
                    }

                }
            }

            #region Need to un-comment when image issue of edit resolve

            //if (model.CarImages != null && model.CarImages.Count() > 0 && _dealerCarDetails.CarImages.Count > 0)
            //{
            //    foreach (var item in _dealerCarDetails.CarImages)
            //    {
            //        _dealerCarImagesRepository.Delete(item.Id);
            //    }
            //}

            //if (model.Id > 0)
            //{
            //    DeleteCarImages(model.Id);
            //}
            #endregion

            if (model.CarImages != null && model.CarImages[0] != null)
            {
                long imgId = 0;
                int counter = 1;
                //imgId = model.DisplayCarImagesList.Count() + 1;
                foreach (var item in model.CarImages)
                {
                    imgId = MyExtentionMethods.ConvertDateTimeToTimestamp(DateTime.UtcNow) + counter;
                    DealerCarImages _dealerCarImages = new DealerCarImages();
                    if (insertupdateCarImages(item, _dealerCarDetails.Id, int.Parse(imgId.ToString())))
                    {
                        _dealerCarImages.NormalImagePath = @"\UploadedFiles\DealerCar\" + _dealerCarDetails.Id + @"\" + imgId.ToString() + "_Normal.jpeg";
                        _dealerCarImages.dealerCarDetails = _dealerCarDetails;
                        _dealerCarImages.LargeImagePath = string.Empty;
                        _dealerCarImages.ThumbImagePath = @"\UploadedFiles\DealerCar\" + _dealerCarDetails.Id + @"\" + imgId.ToString() + "_Small.jpeg";
                        _dealerCarImagesRepository.Insert(_dealerCarImages);
                    }
                    counter++;
                }
            }


            if (model.Id <= 0)
                _DealerCarDetailsRepository.Insert(_dealerCarDetails);
            else
                _DealerCarDetailsRepository.Update(_dealerCarDetails);

            return result;

        }

        [UnitOfWork]
        public List<CarModel> CarModelListbyMaker(int CarmakerVal)
        {
            List<CarModel> carModels = new List<CarModel>();
            var query = _CarMakerRepository.Get(CarmakerVal);
            if (query != null)
            {
                var listitem = _carDetailsRepository.GetAll(x => x.Maker == query);
                carModels = listitem.Select(x => x.Model).Distinct().ToList();
            }
            return carModels;

        }

        [UnitOfWork]
        public List<CarYear> CarYearList(int CarmakerVal, int carModelVal)
        {
            List<CarYear> carYear = new List<CarYear>();
            var listitem = _carDetailsRepository.GetAll();
            var carmaker = _CarMakerRepository.Get(CarmakerVal);
            if (carmaker != null)
            {
                listitem = listitem.Where(x => x.Maker == carmaker);
                carYear = listitem.Select(x => x.Year).Distinct().ToList();
            }
            if (carModelVal > 0)
            {
                var carmodel = _CarModelRepository.Get(carModelVal);
                if (carmodel != null)
                {
                    listitem = listitem.Where(x => x.Model == carmodel);
                    carYear = listitem.Select(x => x.Year).Distinct().ToList();
                }
            }
            return carYear;

        }

        [UnitOfWork]
        public List<SelectListItem> CarTrimPropList(int CarmakerVal, int carModelVal, int CarYearVal)
        {
            var trim = new List<SelectListItem>();
            IQueryable<CarDetails> listitem = null;
            var carmaker = _CarMakerRepository.Get(CarmakerVal);
            if (carmaker != null)
            {
                listitem = _carDetailsRepository.GetAll().Where(x => x.Maker == carmaker);
            }
            if (carModelVal > 0)
            {
                var carmodel = _CarModelRepository.Get(carModelVal);
                if (carmodel != null)
                {
                    listitem = listitem.Where(x => x.Model == carmodel);
                }
            }
            if (CarYearVal > 0)
            {
                var caryear = _CarYearRepository.Get(CarYearVal);
                if (caryear != null)
                {
                    listitem = listitem.Where(x => x.Year == caryear);

                }
            }
            if (listitem != null)
            {
                var gropbylist = listitem.GroupBy(x => new { x.Trim, x.Id });
                trim.AddRange(gropbylist.ToList().Select(x => new SelectListItem() { Text = x.Key.Trim, Value = x.Key.Id.ToString() }));
            }
            return trim;
        }

        [UnitOfWork]
        public CarDetails GetDetailbyTrim(int trim)
        {
            var model = _carDetailsRepository.Get(x => x.Id == trim);
            return model;

        }


        public IList<DealerCarDetails> GetDealerCarListing(long UserId, CarFilterViewModel filter, bool IsDealerSearch, bool IsSold)
        {
            var dealercarList = new List<DealerCarDetails>();
            var user = _appUsersRepository.Get(x => x.Id == UserId);
            if (user != null)
            {
                IQueryable<DealerCarDetails> DealerCarListQuery;


                if (IsDealerSearch)
                {
                    DealerCarListQuery = _DealerCarDetailsRepository.GetAll(x => x.User == user && x.IsSold == IsSold);
                }
                else
                {
                    DealerCarListQuery = _DealerCarDetailsRepository.GetAll(x => x.User != user && x.IsOnHold == false && x.IsSold == false);
                    if (DealerCarListQuery != null && DealerCarListQuery.Count() > 0)
                    {
                        DealerCarListQuery = DealerCarListQuery.ToList().Where(x => x.User != null && x.User.dealerConnectCreation != null && x.User.dealerConnectCreation.Count() > 0).AsQueryable();

                    }
                }
                //DealerCarListQuery = DealerCarListQuery.Where(x => x.User.dealerConnectCreation != null);
                if (filter != null)
                {

                    if (filter.CB != null && filter.CB > 0)
                    {
                        var carmakers = _CarMakerRepository.Get(x => x.Id == filter.CB);
                        DealerCarListQuery = DealerCarListQuery.Where(x => x.CarDetails.Maker == carmakers);
                    }
                    if (filter.CM != null && filter.CM > 0)
                    {
                        var carmodels = _CarModelRepository.Get(x => x.Id == filter.CM);
                        DealerCarListQuery = DealerCarListQuery.Where(x => x.CarDetails.Model == carmodels);
                    }
                    if (filter.CT != null && filter.CT > 0)
                    {
                        DealerCarListQuery = DealerCarListQuery.Where(x => x.OwnershipType == filter.CT);
                    }
                    if (filter.DT != null && filter.DT > 0)
                    {
                        var drivetype = _driveTypeRepository.Get(x => x.Id == filter.DT);
                        DealerCarListQuery = DealerCarListQuery.Where(x => x.CarDetails.DriveType == drivetype);
                    }
                    if (filter.TT != null && filter.TT > 0)
                    {
                        if (filter.TT == 1)
                            DealerCarListQuery = DealerCarListQuery.Where(x => x.CarDetails.Transmission.TransmissionName.ToLower().Contains("automatic"));
                        if (filter.TT == 2)
                            DealerCarListQuery = DealerCarListQuery.Where(x => x.CarDetails.Transmission.TransmissionName.ToLower().Contains("manual"));
                    }
                    if (filter.ML != null && filter.ML > 0)
                    {
                        switch (filter.ML)
                        {
                            case 2:
                                DealerCarListQuery = DealerCarListQuery.Where(x => x.Miles > 200000);
                                break;
                            default:
                                DealerCarListQuery = DealerCarListQuery.Where(x => x.Miles <= filter.ML.Value);
                                break;
                        }
                    }
                    if (filter.PT != null && filter.PT > 0)
                    {
                        DealerCarListQuery = DealerCarListQuery.Where(x => x.PriceType == filter.PT.Value);
                    }
                    if (filter.YR != null && !string.IsNullOrEmpty(filter.YR))
                    {
                        int minyear = int.Parse(filter.YR.Split(',').FirstOrDefault());
                        int maxyear = int.Parse(filter.YR.Split(',').LastOrDefault());

                        DealerCarListQuery = DealerCarListQuery.Where(x => x.CarDetails.Year.Name >= minyear && x.CarDetails.Year.Name <= maxyear);
                    }
                    if (filter.PR != null && !string.IsNullOrEmpty(filter.PR))
                    {
                        decimal minvalue = Convert.ToDecimal(filter.PR.Split(',').FirstOrDefault());
                        decimal maxvalue = Convert.ToDecimal(filter.PR.Split(',').LastOrDefault());
                        DealerCarListQuery = DealerCarListQuery.Where(x => x.Price >= minvalue && x.Price <= maxvalue);
                    }
                    if (filter.Dst != null && filter.Dst > 0)
                    {
                        List<MileFilterRecored> ziplist = Getnerbyzip(filter.Dst.Value.ToString(), user);
                        dealercarList = DealerCarListQuery.ToList();
                        DealerCarListQuery = dealercarList.Where(x => x.User != null && x.User.UserProfile != null && ziplist.Any(z => z.zipcode == x.User.UserProfile.Zipcode)).AsQueryable();
                    }
                    dealercarList = DealerCarListQuery.ToList();
                }
                //return dealercarList = DealerCarListQuery.OrderByDescending(x => x.CreatedTs).ToList();

                if (DealerCarListQuery != null && DealerCarListQuery.ToList().Count() > 0)
                {
                    switch (filter.Sb)
                    {
                        case 1:
                            dealercarList = DealerCarListQuery.OrderBy(x => x.Price).ToList();
                            break;
                        case 2:
                            dealercarList = DealerCarListQuery.OrderByDescending(x => x.Price).ToList();
                            break;
                        case 3:
                            dealercarList = DealerCarListQuery.OrderByDescending(x => x.CreatedTs).ToList();
                            break;
                        case 4:
                            dealercarList = DealerCarListQuery.OrderBy(x => x.CreatedTs).ToList();
                            break;
                        default:
                            dealercarList = DealerCarListQuery.OrderBy(x => x.CarDetails.Maker.Name).OrderBy(x => x.CarDetails.Model.Name).ToList();
                            break;

                    }
                }
            }
            return dealercarList.ToList();
        }

        [UnitOfWork]
        public bool IsVinExist(string vinnumber)
        {
            bool IsExist = false;
            var data = _DealerCarDetailsRepository.Get(x => x.VIN.Equals(vinnumber) && !x.IsSold);
            if (data != null)
            {
                IsExist = true;
            }
            return IsExist;
        }
        private void DeleteCarImages(int DelarCarid)
        {
            string DeleteCarPath = @"~\UploadedFiles\DealerCar\" + DelarCarid;
            bool DeleteDelarCarFolderexists = System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(DeleteCarPath));
            if (DeleteDelarCarFolderexists)
            {
                System.IO.Directory.Delete(DeleteCarPath);
            }
        }
        public static bool Resize_Picture(string Org, string Des, int FinalWidth, int FinalHeight, int ImageQuality, string FileName)
        {
            System.Drawing.Bitmap NewBMP;
            System.Drawing.Graphics graphicTemp;
            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(Org);
            Des = Des + "\\" + FileName;


            int iWidth;
            int iHeight;
            if ((FinalHeight == 0) && (FinalWidth != 0))
            {
                iWidth = FinalWidth;
                iHeight = (bmp.Size.Height * iWidth / bmp.Size.Width);
            }
            else if ((FinalHeight != 0) && (FinalWidth == 0))
            {
                iHeight = FinalHeight;
                iWidth = (bmp.Size.Width * iHeight / bmp.Size.Height);
            }
            else
            {
                iWidth = FinalWidth;
                iHeight = FinalHeight;
            }

            NewBMP = new System.Drawing.Bitmap(iWidth, iHeight);
            graphicTemp = System.Drawing.Graphics.FromImage(NewBMP);
            graphicTemp.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
            graphicTemp.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            graphicTemp.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphicTemp.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphicTemp.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            graphicTemp.DrawImage(bmp, 0, 0, iWidth, iHeight);
            graphicTemp.Dispose();
            System.Drawing.Imaging.EncoderParameters encoderParams = new System.Drawing.Imaging.EncoderParameters();
            System.Drawing.Imaging.EncoderParameter encoderParam = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, ImageQuality);
            encoderParams.Param[0] = encoderParam;
            System.Drawing.Imaging.ImageCodecInfo[] arrayICI = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();
            for (int fwd = 0; fwd <= arrayICI.Length - 1; fwd++)
            {
                if (arrayICI[fwd].FormatDescription.Equals("JPEG"))
                {
                    using (MemoryStream memory = new MemoryStream())
                    {
                        using (FileStream fs = new FileStream(Des, FileMode.Create, FileAccess.ReadWrite))
                        {
                            NewBMP.Save(memory, arrayICI[fwd], encoderParams);
                            byte[] bytes = memory.ToArray();
                            fs.Write(bytes, 0, bytes.Length);
                        }
                    }
                }
            }

            NewBMP.Dispose();
            bmp.Dispose();

            return true;
        }

        private bool insertupdateCarImages(HttpPostedFileBase image, int DelarCarid, int DealerCarImgid)
        {
            bool bNormal = false;
            bool bSmall = false;
            if (DelarCarid > 0)
            {
                string RootfolderPath = @"~\UploadedFiles";
                bool rootexists = System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(RootfolderPath));
                if (!rootexists)
                    System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(RootfolderPath));
                string CarfolderPath = @"~\UploadedFiles\DealerCar";
                bool CarFolderexists = System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(CarfolderPath));
                if (!CarFolderexists)
                    System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(CarfolderPath));

                string CarPath = @"~\UploadedFiles\DealerCar\" + DelarCarid;
                bool DelarCarFolderexists = System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(CarPath));
                if (!DelarCarFolderexists)
                    System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(CarPath));

                string Original = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath(CarPath)) + DealerCarImgid.ToString() + "_Original.jpeg";
                image.SaveAs(Original);
                //image.SaveAs(System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath(CarPath), image.FileName));
                bNormal = Resize_Picture(Original, System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath(CarPath)), 900, 560, 90, DealerCarImgid.ToString() + "_Normal.jpeg"); //Nomral image for car detail
                bSmall = Resize_Picture(Original, System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath(CarPath)), 433, 325, 70, DealerCarImgid.ToString() + "_Small.jpeg"); //Small size image for car listing

            }
            return (bNormal && bSmall);
        }


        public List<MileFilterRecored> Getnerbyzip(string rang, AppUsers CurrentUsers)
        {

            List<MileFilterRecored> result = new List<MileFilterRecored>();
            var userProfile = _userProfileRepository.Get(x => x.User == CurrentUsers);
            string zip = userProfile.Zipcode;

            string unit = "Miles";
            if (!string.IsNullOrEmpty(zip))
            {
                var chechrecord = _mileFilterDataRepository.Get(c => c.Zipcod == zip && c.Mile == rang);
                if (chechrecord != null)
                {
                    result = JsonConvert.DeserializeObject<List<MileFilterRecored>>(Encoding.ASCII.GetString(chechrecord.Record));
                }
                else
                {
                    List<SearchResult> getalllist = new List<SearchResult>();
                    var list = GetAllZip(zip, "US", rang, unit, getalllist, 0, 0);
                    List<MileFilterRecored> filterlist = list.GroupBy(g => new { g.postal_code, g.distance }).Select(x => new MileFilterRecored { zipcode = x.Key.postal_code, distance = Math.Round(x.Key.distance, 2).ToString() }).ToList();
                    result = filterlist.Distinct().ToList();
                    try
                    {
                        var json = JsonConvert.SerializeObject(result);
                        MileFilterData mileFilterData = new MileFilterData();
                        mileFilterData.Zipcod = zip;
                        mileFilterData.Mile = rang;
                        mileFilterData.Record = Encoding.ASCII.GetBytes(json);
                        _mileFilterDataRepository.Insert(mileFilterData);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }

            return result;

        }
        private List<SearchResult> GetAllZip(string zc, string cc, string rang, string unit, List<SearchResult> ziplist, int pagesize, int totalcount)
        {

            string url = @"https://ezcmd.com/apps/api_geo_postal_codes/nearby_locations_by_zip_code/ccdaf99dcea2470b9aac78236bade846/403?zip_code=" + zc + "&country_code=" + cc + "&within=" + rang + "&unit=" + unit + "&start=" + pagesize.ToString();
            WebRequest request = WebRequest.Create(url);
            WebResponse response = request.GetResponse();
            Stream data = response.GetResponseStream();
            StreamReader reader = new StreamReader(data);
            string responseFromServer = reader.ReadToEnd();
            if (!string.IsNullOrEmpty(responseFromServer))
            {
                RootObject results = JsonConvert.DeserializeObject<RootObject>(responseFromServer);
                if (results.success)
                {
                    ziplist.AddRange(results.search_results);
                    totalcount = results.search_info.records_total;
                    if (totalcount > 100)
                    {
                        if ((results.search_info.records_total - results.search_info.records_start) > 100)
                        {
                            pagesize = pagesize + 100;
                            GetAllZip(zc, cc, rang, unit, ziplist, pagesize, totalcount);
                        }

                    }
                }

            }
            return ziplist;
        }

        [UnitOfWork]
        public bool CarBuy(int cId, int currentUserId)
        {
            //declaration
            CarOrderDetail carOrderDetail = new CarOrderDetail();

            var buyerUser = _appUsersRepository.Get(x => x.Id == currentUserId);
            var dealerCarDetail = _DealerCarDetailsRepository.Get(cId);

            carOrderDetail = new CarOrderDetail
            {
                IsOnHold = false,
                SellerDealer = dealerCarDetail.User,
                BuyerDealer = buyerUser,
                dealerCarDetails = dealerCarDetail
            };

            _carOrderDetailRepository.Insert(carOrderDetail);

            return true;
        }

        [UnitOfWork]
        public bool CarOnHold(int cId, int currentUserId)
        {
            //declaration
            CarOrderDetail carOrderDetail = new CarOrderDetail();
            CarOrderOnHold carOrderOnHold = new CarOrderOnHold();

            var buyerUser = _appUsersRepository.Get(x => x.Id == currentUserId);
            var dealerCarDetail = _DealerCarDetailsRepository.Get(cId);

            //call from web setting value
            var partialPaymentPerc = int.Parse(WebConfigurationManager.AppSettings["OnHoldPaymentPerc"]);
            int timeToOnHoldCar = int.Parse(WebConfigurationManager.AppSettings["TimeToOnHoldCar"]);

            //calculate partial and pending amount 
            decimal totalPayment = dealerCarDetail.Price;
            decimal parcialPayment = (totalPayment * partialPaymentPerc) / 100;
            decimal restPayment = totalPayment - parcialPayment;

            carOrderDetail = new CarOrderDetail
            {
                IsOnHold = false,
                SellerDealer = dealerCarDetail.User,
                BuyerDealer = buyerUser,
                dealerCarDetails = dealerCarDetail
            };

            _carOrderDetailRepository.Insert(carOrderDetail);

            carOrderOnHold = new CarOrderOnHold
            {
                TimeToHold = DateTime.UtcNow.AddDays(timeToOnHoldCar),
                PartialAmountPaid = parcialPayment,
                RestAmount = restPayment,
                carOrderDetail = carOrderDetail
            };

            _carOrderOnHoldRepository.Insert(carOrderOnHold);

            return true;
        }

        #endregion Controlelr Services
        #region Insert Dealer Message To Database
        [UnitOfWork]
        public void InsertMessage(DealerMessagesViewModel dealerMessagesVM)
        {
            DealerMessages dealerMessage = new DealerMessages();
            AppUsers fromDealer = _appUsersRepository.Get(dealerMessagesVM.FromDealerId);
            AppUsers toDealer = _appUsersRepository.Get(dealerMessagesVM.ToDealerId);

            dealerMessage.Message = dealerMessagesVM.Message;
            dealerMessage.CreatedTs = DateTime.Now;

            dealerMessage.fromDealer = fromDealer;
            dealerMessage.toDealer = toDealer;

            _dealerMessagesRepository.Insert(dealerMessage);
        }
        #endregion
        #region Get all dealer messages
        readonly string _connString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        public IList<DealerMessages> GetAllMessages(int currLoginDealerId, int dealerId)
        {
            var messages = new List<DealerMessages>();
            using (var connection = new SqlConnection(_connString))
            {
                connection.Open();
                using (var command = new SqlCommand(@"SELECT [Id],[Message],[CreatedTs],[FromDealerId],[ToDealerId] FROM [dbo].[DealerMessages] WHERE ([FromDealerId] = " + currLoginDealerId + " AND [ToDealerId] = " + dealerId + ") OR ([FromDealerId] = " + dealerId + " AND [ToDealerId] = " + currLoginDealerId + ") ORDER BY [CreatedTs]", connection))
                {
                    command.Notification = null;

                    //var dependency = new SqlDependency(command);
                    //dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    //SqlDependency.Start(_connString);

                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        try
                        {
                            AppUsers fromUser = _appUsersRepository.Get((int)reader["FromDealerId"]);
                            AppUsers toUser = _appUsersRepository.Get((int)reader["ToDealerId"]);
                            messages.Add(item: new DealerMessages { Id = (int)reader["Id"], Message = (string)reader["Message"], FromDealerId = (int)reader["FromDealerId"], ToDealerId = (int)reader["ToDealerId"], CreatedTs = Convert.ToDateTime(reader["CreatedTs"]), fromDealer = fromUser, toDealer = toUser });
                        }
                        catch (Exception e)
                        {
                            e.ToString();
                        }
                    }
                }

            }
            return messages;


        }
        /*
        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                ChatHub chatHub = new ChatHub();
                chatHub.hubPopulateMessages();
            }
        }*/
        #endregion

        public AppUsers getUserById(int id)
        {
            return _appUsersRepository.Get(id);
        }

        public IList<AppUsers> getDealerIdsOfMessages(int dealerId)
        {
            IList<AppUsers> messageDealerIds = new List<AppUsers>();
            using (var connection = new SqlConnection(_connString))
            {
                connection.Open();
                using (var command = new SqlCommand(@"SELECT DISTINCT([FromDealerId]) FROM [dbo].[DealerMessages]
                        WHERE ([FromDealerId] = " + dealerId + " OR [ToDealerId] = " + dealerId + ") AND [FromDealerId] <> " + dealerId +
                       "UNION SELECT DISTINCT([ToDealerId]) FROM [dbo].[DealerMessages]" +
                       " WHERE ([FromDealerId] = " + dealerId + "  OR [ToDealerId] = " + dealerId + ") AND [ToDealerId] <> " + dealerId, connection))
                {
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        try
                        {
                            AppUsers appUsers = _appUsersRepository.Get((int)reader["FromDealerId"]);
                            messageDealerIds.Add(appUsers);
                        }
                        catch (Exception e)
                        {
                            e.ToString();
                        }
                    }
                }
            }
            return messageDealerIds;
        }

        public int getUserIdByName(string userName)
        {
            int userId = 0;
            using (var connection = new SqlConnection(_connString))
            {
                connection.Open();
                using (var command = new SqlCommand(@"SELECT [Id] FROM [dbo].[AppUsers] WHERE [UserName] = '" + userName + "'", connection))
                {
                    //var reader = command.ExecuteReader();
                    var reader = command.ExecuteScalar();
                    userId = int.Parse(reader.ToString());
                }
            }
            return userId;
        }

        [UnitOfWork]
        public ProcessViewModel DeleteCarImage(string imageId)
        {
            ProcessViewModel result = new ProcessViewModel();
            var carImages = _dealerCarImagesRepository.Get(int.Parse(imageId));
            try
            {
                if (carImages != null)
                {
                    carImages.IsDeleted = true;

                    //Call Update function
                    _dealerCarImagesRepository.Update(carImages);

                    if (File.Exists(System.Web.HttpContext.Current.Server.MapPath(carImages.ThumbImagePath)))
                    {
                        File.Delete(System.Web.HttpContext.Current.Server.MapPath(carImages.ThumbImagePath));
                    }

                    if (File.Exists(System.Web.HttpContext.Current.Server.MapPath(carImages.NormalImagePath)))
                    {
                        File.Delete(System.Web.HttpContext.Current.Server.MapPath(carImages.NormalImagePath));
                    }

                    //bool DelarCarFolderexists = System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(carImages.ThumbImagePath));

                    //Result - ProcessViewModel
                    result.IsSuccess = true;
                    result.Message = string.Format(SiteConstants.Delete_Success, "Image");
                }
                else
                {
                    //Result - ProcessViewModel
                    result.IsSuccess = true;
                    result.Message = string.Format(SiteConstants.Delete_Failure, "Image");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                result.IsSuccess = false;
                result.Message = string.Format(SiteConstants.Delete_Failure, "Image");
            }
            return result;
        }

        [UnitOfWork]
        public IList<DealerCarDetails> GetAllDealerCarListing()
        {
            return _DealerCarDetailsRepository.GetAll().ToList();
        }

        [UnitOfWork]
        public ProcessViewModel CarSold(int CId)
        {
            ProcessViewModel result = new ProcessViewModel();
            try
            {
                var _dealerCarDetails = _DealerCarDetailsRepository.Get(CId);
                _dealerCarDetails.IsSold = true;
                _DealerCarDetailsRepository.Update(_dealerCarDetails);

                //Result - ProcessViewModel
                result.IsSuccess = true;
                result.Message = string.Format(SiteConstants.DealerCarSold_Success, "Car");
            }
            catch (Exception)
            {
                //Result - ProcessViewModel
                result.IsSuccess = false;
                result.Message = string.Format(SiteConstants.DealerCarSold_Failure, "Car");
            }
            return result;
        }
    }
}
