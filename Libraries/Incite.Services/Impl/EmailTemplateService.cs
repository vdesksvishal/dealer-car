﻿using Incite.Core.Domain;
using Incite.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Helpers;

namespace Incite.Services
{
    public class EmailTemplateService : IEmailTemplateService
    {

        public bool ProcessedToEmail(AppUsers model)
        {
            bool result = false;
            try
            {
                string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                string URL = string.Format(SiteConstants.ConfirmEmailURL, baseUrl, model.UserGuid);
                string body = "<br/><br/>We are excited to tell you that your account is" +
                              " successfully created. After verified by admin you will be able to login in application. We will notify you via Email.";
                string SendToEmail = model.Email;
                string SendEmailSubject = "Dealerinventorynetwork Account Confirmation";
                string SendEmailBody = "";
                SendEmailBody += body;
                WebMail.SmtpServer = WebConfigurationManager.AppSettings["mailserver"];
                WebMail.SmtpPort = Convert.ToInt32(WebConfigurationManager.AppSettings["mailPort"]);
                WebMail.EnableSsl = bool.Parse(WebConfigurationManager.AppSettings["mailSSL"]);
                WebMail.UserName = WebConfigurationManager.AppSettings["mailUserName"];
                WebMail.Password = WebConfigurationManager.AppSettings["mailPassword"];
                string FromEmail = WebConfigurationManager.AppSettings["Frommail"];
                WebMail.Send(to: SendToEmail, subject: SendEmailSubject, body: SendEmailBody, from: FromEmail, isBodyHtml: true);
                result = true;
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public bool ProcessedToEmailUserActivate(AppUsers model)
        {
            bool result = false;
            try
            {
                string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                string URL = baseUrl;
                string body = "<br/><br/>We are excited to tell you that your account created successfully." +
                              "<br/><br/> Please click on the below link to login" +
                              " <br/><br/><a href='" + URL + "'>" + "Clike Here" + "</a> ";
                string SendToEmail = model.Email;
                string SendEmailSubject = "Dealerinventorynetwork Account Confirmation";
                string SendEmailBody = "";
                SendEmailBody += body;
                WebMail.SmtpServer = WebConfigurationManager.AppSettings["mailserver"];
                WebMail.SmtpPort = Convert.ToInt32(WebConfigurationManager.AppSettings["mailPort"]);
                WebMail.EnableSsl = bool.Parse(WebConfigurationManager.AppSettings["mailSSL"]);
                WebMail.UserName = WebConfigurationManager.AppSettings["mailUserName"];
                WebMail.Password = WebConfigurationManager.AppSettings["mailPassword"];
                string FromEmail = WebConfigurationManager.AppSettings["Frommail"];
                WebMail.Send(to: SendToEmail, subject: SendEmailSubject, body: SendEmailBody, from: FromEmail, isBodyHtml: true);
                result = true;
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public bool ProcessedToEmailForgotPassword(AppUsers model)
        {
            bool result = false;
            try
            {
                string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                string URL = string.Format(SiteConstants.ForgotPasswordEmailURL, baseUrl, model.UserGuid);
                string body = "<br/><br/> Please click on the below link to reset your account password" +
                              " <br/><br/><a href='" + URL + "'>" + "Clike Here" + "</a> ";
                string SendToEmail = model.Email;
                string SendEmailSubject = "Dealerinventorynetwork Account Confirmation";
                string SendEmailBody = "";
                SendEmailBody += body;
                WebMail.SmtpServer = WebConfigurationManager.AppSettings["mailserver"];
                WebMail.SmtpPort = Convert.ToInt32(WebConfigurationManager.AppSettings["mailPort"]);
                WebMail.EnableSsl = bool.Parse(WebConfigurationManager.AppSettings["mailSSL"]);
                WebMail.UserName = WebConfigurationManager.AppSettings["mailUserName"];
                WebMail.Password = WebConfigurationManager.AppSettings["mailPassword"];
                string FromEmail = WebConfigurationManager.AppSettings["Frommail"];
                WebMail.Send(to: SendToEmail, subject: SendEmailSubject, body: SendEmailBody, from: FromEmail, isBodyHtml: true);
                result = true;
            }
            catch (Exception ex)
            {

            }
            return result;
        }

    }
}
