﻿using ClosedXML.Excel;
using Incite.Core.Domain;
using Incite.Data;
using Incite.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services
{
    public class ImportExportService : IImportExportService
    {
        #region Private Member Variables
        private readonly ICarMakerRepository _carMakerRepository;
        private readonly ICarModelRepository _carModelRepository;
        private readonly ICarYearRepository _carYearRepository;
        private readonly IDriveTypeRepository _driveTypeRepository;
        private readonly IEngineTypeRepository _engineTypeRepository;
        private readonly IFuelTypeRepository _fuelTypeRepository;
        private readonly ITransmissionRepository _transmissionRepository;
        private readonly ICarDetailsRepository _carDetailsRepository;
        #endregion Private Member Variables
        #region Ctor
        public ImportExportService(ICarMakerRepository carMakerRepository,
            ICarModelRepository carModelRepository,
            ICarYearRepository carYearRepository,
            IDriveTypeRepository driveTypeRepository,
            IEngineTypeRepository engineTypeRepository,
            IFuelTypeRepository fuelTypeRepository,
            ITransmissionRepository transmissionRepository,
            ICarDetailsRepository carDetailsRepository)
        {
            _carMakerRepository = carMakerRepository;
            _carModelRepository = carModelRepository;
            _carYearRepository = carYearRepository;
            _driveTypeRepository = driveTypeRepository;
            _engineTypeRepository = engineTypeRepository;
            _fuelTypeRepository = fuelTypeRepository;
            _transmissionRepository = transmissionRepository;
            _carDetailsRepository = carDetailsRepository;
        }
        #endregion Ctor

        #region enum
        /// <summary>
        /// Enum replicates column order of excel doc which have car/year/model and many more details
        /// </summary>
        public enum CarDetailSpecColumn
        {
            //Naming
            MakerParent = 0,
            ModelParent = 1,
            YearParent = 2,
            Index = 3,
            MakerName = 4,
            ModelName = 5,
            Year = 6,
            Trim = 7,
            Engine = 8,
            //Price and colors
            Price = 9,
            ColorsExterior = 10,
            ColorsInterior = 11,
            //Dimensions 
            BodyType = 13,
            Length = 14,
            Width = 15,
            Height = 16,
            Wheelbase = 17,
            CurbWeight = 18,
            //Engine & Transamission
            Cylinders = 19,
            EngineSize = 20,
            HorsePower = 21,
            HorsePowerRPM = 22,
            TorqueLBS = 23,
            TorqueRPM = 24,
            DriveType = 25,
            Transmission = 26,
            //Fuel & Mileage
            EngineType = 27,
            FuelType = 28,
            FuelTankCap = 29,
            CombineMPG = 30,
            EPAMileage = 31,
            Range = 32
        };
        #endregion enum

        #region Public Methods
        /// <summary>
        /// Add car details to database
        /// </summary>
        //[UnitOfWork]
        public Task<bool> ImportCarDetails(String Path)
        {
            bool isCarDetailsAddedWithError = false;
            string columnname = string.Empty;
            string column = "";
            int i = 0;
            List<string> ErrLineNo = new List<string>();
            //Path = @"C:\Anand\CarDetailsTemp.xlsx";
            try
            {
                if (string.IsNullOrWhiteSpace(Path))
                    return Task.FromResult(false);

                List<CarMaker> _carMakerList = _carMakerRepository.GetAll().ToList();
                List<CarModel> _carModelList = _carModelRepository.GetAll().ToList();
                List<CarYear> _carYearList = _carYearRepository.GetAll().ToList();
                List<DriveType> _driveTypeList = _driveTypeRepository.GetAll().ToList();
                List<EngineType> _engineTypeList = _engineTypeRepository.GetAll().ToList();
                List<FuelType> _fuelTypeList = _fuelTypeRepository.GetAll().ToList();
                List<Transmission> _transmissionList = _transmissionRepository.GetAll().ToList();
                //------------------------------------------------------------------------ Supported extensions are '.xlsx', '.xslm', '.xltx' and '.xltm'."------------------------------------------------------------------------
                using (XLWorkbook workBook = new XLWorkbook(Path))
                {
                    IXLWorksheet workSheet = workBook.Worksheet(1);

                    //Loop through the Worksheet rows.

                    foreach (IXLRow row in workSheet.Rows())
                    {
                        i++;

                        //For data entry,Index(ID) column not suppose to be blank and non-integer
                        long Index = 0;
                        if (!row.IsEmpty() && long.TryParse((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Index).Value.ToString(), out Index))
                        {

                            CarDetails carDetails = _carDetailsRepository.Get(x => x.IndexId == Index);
                            if (carDetails == null)
                                carDetails = new CarDetails();

                            //Reference keys entry
                            string MakerName = (row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.MakerName).Value.ToString();

                            carDetails.Maker = _carMakerList.FirstOrDefault(x => x.Name.ToUpperInvariant() == MakerName.ToUpperInvariant());
                            if (carDetails.Maker == null || carDetails.Maker.Id == 0)
                            {
                                carDetails.Maker = new CarMaker();
                                carDetails.Maker.Name = MakerName;
                                _carMakerList.Add(carDetails.Maker);
                            }
                            string ModelName = (row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.ModelName).Value.ToString();
                            carDetails.Model = _carModelList.FirstOrDefault(x => x.Name.ToUpperInvariant() == ModelName.ToUpperInvariant());
                            if (carDetails.Model == null || carDetails.Model.Id == 0)
                            {
                                carDetails.Model = new CarModel();
                                carDetails.Model.Name = ModelName;
                                _carModelList.Add(carDetails.Model);
                            }

                            string YearName = (row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Year).Value.ToString();
                            int year = 0;
                            if (int.TryParse(YearName, out year))
                            {
                                carDetails.Year = _carYearList.FirstOrDefault(x => x.Name == int.Parse(YearName));
                                if (carDetails.Year == null || carDetails.Year.Id == 0)
                                {
                                    carDetails.Year = new CarYear();
                                    carDetails.Year.Name = int.Parse(YearName);
                                    _carYearList.Add(carDetails.Year);
                                }
                            }
                            string DriverTypeName = (row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.DriveType).Value.ToString();
                            carDetails.DriveType = _driveTypeList.FirstOrDefault(x => x.DriveTypeName.ToUpperInvariant() == DriverTypeName.ToUpperInvariant());
                            if (carDetails.DriveType == null || carDetails.DriveType.Id == 0)
                            {
                                carDetails.DriveType = new DriveType();
                                carDetails.DriveType.DriveTypeName = DriverTypeName;
                                _driveTypeList.Add(carDetails.DriveType);
                            }
                            string FuelTypeName = (row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.FuelType).Value.ToString();
                            carDetails.FuelType = _fuelTypeList.FirstOrDefault(x => x.FuelTypeName.ToUpperInvariant() == FuelTypeName.ToUpperInvariant());
                            if (carDetails.FuelType == null || carDetails.FuelType.Id == 0)
                            {
                                carDetails.FuelType = new FuelType();
                                carDetails.FuelType.FuelTypeName = FuelTypeName;
                                _fuelTypeList.Add(carDetails.FuelType);
                            }

                            string TransmissionName = (row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Transmission).Value.ToString();
                            carDetails.Transmission = _transmissionList.FirstOrDefault(x => x.TransmissionName.ToUpperInvariant() == TransmissionName.ToUpperInvariant());
                            if (carDetails.Transmission == null || carDetails.Transmission.Id == 0)
                            {
                                carDetails.Transmission = new Transmission();
                                carDetails.Transmission.TransmissionName = TransmissionName;
                                _transmissionList.Add(carDetails.Transmission);
                            }



                            //carDetails.Model = InsertCarModel((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.ModelName).Value.ToString());
                            //carDetails.Year = InsertCarYear((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Year).Value.ToString());
                            // carDetails.DriveType = InsertCarDriveType((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.DriveType).Value.ToString());
                            // carDetails.FuelType = InsertCarFuelType((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.FuelType).Value.ToString());
                            //carDetails.Transmission = InsertCarTransmission((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Transmission).Value.ToString());

                            column = "Trim";
                            //Car details entry
                            if (!string.IsNullOrWhiteSpace((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Trim).Value.ToString()))
                            {
                                try
                                {


                                    carDetails.Trim = (row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Trim).Value.ToString().Trim();

                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debug.WriteLine("\n\n\n\nCarDetails.Trim 213\n" + ex);
                                    throw;
                                }
                            }

                           column = "Engine";
                            if (!string.IsNullOrWhiteSpace((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Engine).Value.ToString()))
                            {
                                try
                                {

                                    carDetails.Engine = (row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Engine).Value.ToString().Trim();

                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debug.WriteLine("\n\n\n\nEngine 237\n" + ex);
                                    throw;
                                }
                            } 

                            column = "Price";
                            int Price = 0;
                            if (int.TryParse((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Price).Value.ToString(), out Price))
                            {
                                carDetails.Price = Price;
                            }

                           



                            if (int.TryParse((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Price).Value.ToString(), out Price))
                            {
                                carDetails.Price = Price;
                            }

                            column = "Index";
                            carDetails.IndexId = Index;
                            column = "ColorsExterior";
                            if (!string.IsNullOrWhiteSpace((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.ColorsExterior).Value.ToString()))
                            {
                                try
                                {


                                    carDetails.ColorsExterior = Encoding.ASCII.GetBytes((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.ColorsExterior).Value.ToString().Trim());

                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debug.WriteLine("\n\n\n\nCarDetails Color 237\n" + ex);

                                }
                            }
                            column = "ColorsIxterior";
                            if (!string.IsNullOrWhiteSpace((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.ColorsInterior).Value.ToString()))
                            {
                                try
                                {
                                    carDetails.ColorsInterior = Encoding.ASCII.GetBytes((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.ColorsInterior).Value.ToString().Trim());
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debug.WriteLine("\n\n\n\nCar Details Color Interior 250\n" + ex);

                                }
                            }


                            column = "BodyType";
                            if (!string.IsNullOrWhiteSpace((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.BodyType).Value.ToString()))
                            {
                                try
                                {

                                    carDetails.BodyType = (row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.BodyType).Value.ToString().Trim();

                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debug.WriteLine("\n\n\n\nEngine 237\n" + ex);
                                    throw;
                                }
                            }

                            column = "Length";
                            int Length = 0;
                            if (int.TryParse((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Length).Value.ToString(), out Length))
                            {
                                carDetails.Length = Length;
                            }
                            column = "Width";
                            float Width = 0;
                            if (float.TryParse((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Width).Value.ToString(), out Width))
                            {
                                carDetails.Width = Width;
                            }
                            column = "Height";
                            float Height = 0;
                            if (float.TryParse((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Height).Value.ToString(), out Height))
                            {
                                carDetails.Height = Height;
                            }
                            column = "WheelBase";
                            float WheelBase = 0;
                            if (float.TryParse((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Wheelbase).Value.ToString(), out WheelBase))
                            {
                                carDetails.WheelBase = WheelBase;
                            }
                            column = "Curbweight";
                            int CurbWeight = 0;
                            if (int.TryParse((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.CurbWeight).Value.ToString(), out CurbWeight))
                            {
                                carDetails.CurbWeight = CurbWeight;
                            }
                            column = "Cylinders";
                            if (!string.IsNullOrWhiteSpace((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Cylinders).Value.ToString()))
                            {
                                try
                                {


                                    carDetails.Cylinders = (row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Cylinders).Value.ToString().Trim();

                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debug.WriteLine("\n\n\n\nCylinder line 296\n" + ex);

                                }
                            }
                            column = "EngineSize";
                            float EngineSize = 0;
                            if (float.TryParse((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.EngineSize).Value.ToString(), out EngineSize))
                            {
                                carDetails.EngineSize = EngineSize;
                            }
                            column = "HorsePower";
                            int HorsePower = 0;
                            if (int.TryParse((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.HorsePower).Value.ToString(), out HorsePower))
                            {
                                carDetails.HP = HorsePower;
                            }
                            column = "HorsePowerRPM";
                            int HorsePowerRPM = 0;
                            if (int.TryParse((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.HorsePowerRPM).Value.ToString(), out HorsePowerRPM))
                            {
                                carDetails.RPM = HorsePowerRPM;
                            }
                            column = "Torque";
                            int Torque = 0;
                            if (int.TryParse((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.TorqueLBS).Value.ToString(), out Torque))
                            {
                                carDetails.Torque = Torque;
                            }
                            column = "TorqueRPM";
                            int TorqueRPM = 0;
                            if (int.TryParse((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.TorqueRPM).Value.ToString(), out TorqueRPM))
                            {
                                carDetails.TorqueRPM = TorqueRPM;
                            }
                            column = "FuelTankCap";
                            float FuelTankCap = 0;
                            if (float.TryParse((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.FuelTankCap).Value.ToString(), out FuelTankCap))
                            {
                                carDetails.FuelTankCap = FuelTankCap;
                            }
                            column = "CombinedMPG";
                            int CombinedMPG = 0;
                            if (int.TryParse((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.CombineMPG).Value.ToString(), out CombinedMPG))
                            {
                                carDetails.CombinedMPG = CombinedMPG;
                            }
                            column = "EPAMileage";
                            if (!string.IsNullOrWhiteSpace((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.EPAMileage).Value.ToString()))
                            {
                                carDetails.EPAMileage = (row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.EPAMileage).Value.ToString().Trim();
                            }
                            column = "Range";
                            if (!string.IsNullOrWhiteSpace((row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Range).Value.ToString()))
                            {
                                try
                                {


                                    carDetails.RangeMiles = (row.Cells()).ElementAt<IXLCell>((int)CarDetailSpecColumn.Range).Value.ToString().Trim();
                                }
                                catch (Exception ex)
                                {

                                    System.Diagnostics.Debug.WriteLine("\n\n\n\nRange Miles 259\n" + ex);
                                }
                            }
                            column = "InsertRow";
                            try
                            {
                                InsertUpdateCarDetails(carDetails);
                            }
                            catch (Exception ex)
                            {
                                System.Diagnostics.Debug.WriteLine("\n\n\n\nInsert Update Car Details line 369\n" + ex);
                                isCarDetailsAddedWithError = true;
                                ErrLineNo.Add(i.ToString());
                                continue;
                            }


                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error for AddCarDetails
                System.Diagnostics.Debug.WriteLine("\n\n\n\nMain Catch line 383\n" + ex);
                isCarDetailsAddedWithError = true;
                columnname = column;
                int j = i;
                ErrLineNo.Add(i.ToString());
            }
            return Task.FromResult(isCarDetailsAddedWithError);
        }


        #endregion Public Methods

        #region Private Methods
        [UnitOfWork]
        private void InsertUpdateCarDetails(CarDetails carDetails)
        {
            try
            {
                _carDetailsRepository.InsertOrUpdate(carDetails);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("\n\n\n\nCar Details Repo line 405\n" + ex);

            }
        }

        #endregion Private Methods

    }
}
