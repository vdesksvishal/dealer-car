﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Incite.Core.Domain;
using Incite.Core.Infrastructure;
using Incite.Data;
using Incite.Data.Repositories;
using Incite.Services.ViewModels;
using Stripe;
using Trialcar.Web.Helper;
using static Incite.Core.Infrastructure.SiteEnum;
using System.Web.Configuration;

namespace Incite.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly ICustomerPaymentTypesRepository _customerPaymentTypesRepository;
        private readonly IDealerConnectCreationRepository _dealerConnectCreationRepository;
        private readonly IDealerCustomerCreationRepository _dealerCustomerCreationRepository;
        private readonly IAppUsersRepository _appUsersRepository;
        private readonly IDealerCarDetailsRepository _dealerCarDetailsRepository;
        private readonly ICarOrderDetailRepository _carOrderDetailRepository;
        private readonly ICarOrderOnHoldRepository _carOrderOnHoldRepository;

        public const string COUNTRY = "US";
        public const string CURRENCY = "usd";
        public const string TOKEN = "sk_test_mKzyuApPVcvY6egwzQIBzryb";

        #region ctor
        public PaymentService(ICustomerPaymentTypesRepository CustomerPaymentTypesRepository
            , IDealerConnectCreationRepository DealerConnectCreationRepository
            , IDealerCustomerCreationRepository DealerCustomerCreationRepository
            , IAppUsersRepository appUsersRepository
            , IDealerCarDetailsRepository DealerCarDetailsRepository
            , ICarOrderDetailRepository CarOrderDetailRepository
            , ICarOrderOnHoldRepository CarOrderOnHoldRepository)

        {
            _customerPaymentTypesRepository = CustomerPaymentTypesRepository;
            _dealerConnectCreationRepository = DealerConnectCreationRepository;
            _dealerCustomerCreationRepository = DealerCustomerCreationRepository;
            _appUsersRepository = appUsersRepository;
            _dealerCarDetailsRepository = DealerCarDetailsRepository;
            _carOrderDetailRepository = CarOrderDetailRepository;
            _carOrderOnHoldRepository = CarOrderOnHoldRepository;
        }
        #endregion


        #region Stripe's Create/Get Customer Account
        [UnitOfWork]
        public ProcessViewModel CreateCustomer(UpdateSettingsViewModel model)
        {
            ProcessViewModel result = new ProcessViewModel();
            result.IsSuccess = false;
            result.Message = SiteConstants.Bankaccount_Failure;
            if (model.AppUserId <= 0)
            {
                return result;
            }

            StripeConfiguration.SetApiKey(TOKEN);
            var custservice = new CustomerService();
            var Customeroptions = new CustomerListOptions { Email = model.CustomerEmailId };
            StripeList<Customer> customers = custservice.List(Customeroptions);

            if (customers.Data.Count() > 0)
            {
                return result;
            }
            ///This is Demo How Strip Api Add Customer By Add Bank account
            //var options = new TokenCreateOptions
            //{
            //    BankAccount = new BankAccountOptions
            //    {
            //        Country = "US",
            //        Currency = "usd",
            //        AccountHolderName = "Jenny Rosen",
            //        AccountHolderType = "individual",
            //        RoutingNumber = "110000000",
            //        AccountNumber = "000123456789"
            //    }


            var options = new TokenCreateOptions
            {
                BankAccount = new BankAccountOptions
                {
                    Country = COUNTRY,
                    Currency = CURRENCY,
                    AccountHolderName = model.AccountHolderName,
                    AccountHolderType = "company",
                    RoutingNumber = model.RoutingNumber,
                    AccountNumber = model.AccountNumber
                }

            };

            //};
            //End demo of add cuteomer accoutn
            // below line i am using direct model banckaccount
            //    var options = new TokenCreateOptions
            //{
            //    BankAccount = model.BankAccount

            //};
            var service = new TokenService();
            Token stripeToken = service.Create(options);
            //check if strip is created bank account or not if is not then token not comming 
            // we reqiered token for add customer
            if (stripeToken.StripeResponse != null)
            {
                var Custoptions = new CustomerCreateOptions
                {
                    Description = model.Description,
                    SourceToken = stripeToken.Id,
                    Email = model.CustomerEmailId

                };
                Customer customer = custservice.Create(Custoptions);
                var appuser = _appUsersRepository.Get(model.AppUserId);
                DealerCustomerCreation dealerCustomerCreation = new DealerCustomerCreation();
                dealerCustomerCreation.CustomerId = customer.Id;
                dealerCustomerCreation.IsVerified = false;
                dealerCustomerCreation.User = appuser;

                CustomerPaymentTypes paymentTypes = new CustomerPaymentTypes();
                paymentTypes.PaymentId = customer.DefaultSourceId;
                paymentTypes.PaymentType = CustomerPaymentType.BankAccount.ToString();
                paymentTypes.StripeId = customer.DefaultSourceId;
                paymentTypes.DealerCustomerCreation = dealerCustomerCreation;

                _customerPaymentTypesRepository.Insert(paymentTypes);
                _dealerCustomerCreationRepository.Insert(dealerCustomerCreation);
                result.IsSuccess = true;
                result.Message = SiteConstants.Bankaccount_Success;

            }
            return result;
        }
        [UnitOfWork]
        public ProcessViewModel VerifyCustomer(int AppUserId, VerifyBankAccount model)
        {
            ProcessViewModel result = new ProcessViewModel();
            result.IsSuccess = false;
            result.Message = SiteConstants.Bankverify_Failure;

            DealerCustomerCreation dealerCustomerCreation = _dealerCustomerCreationRepository.Get(x => x.User != null && x.User.Id == AppUserId);
            CustomerPaymentTypes customerPaymentTypes = _customerPaymentTypesRepository.Get(x => x.DealerCustomerCreation != null && x.DealerCustomerCreation.Id == dealerCustomerCreation.Id);
            StripeConfiguration.SetApiKey(TOKEN);
            var options = new BankAccountVerifyOptions
            {
                Amounts = new long[]
                        {
                        model.VerifyDebitAmount,
                        model.VerifyCreditAmount
                        }
            };

            var service = new BankAccountService();

            BankAccount bankAccount = service.Verify(
             dealerCustomerCreation.CustomerId,
              customerPaymentTypes.PaymentId,
              options
            );

            if (bankAccount.Status == "verified")
            {
                dealerCustomerCreation.IsVerified = true;
                customerPaymentTypes.IsVerified = true;
                _dealerCustomerCreationRepository.Update(dealerCustomerCreation);
                _customerPaymentTypesRepository.Update(customerPaymentTypes);
                result.IsSuccess = true;
                result.Message = SiteConstants.Bankverify_Success;
                return result;
            }

            return null;
        }
        public StripeList<IPaymentSource> GetUserPaymentSourceList(string UserEmail)
        {
            StripeList<IPaymentSource> result = new StripeList<IPaymentSource>();
            StripeConfiguration.SetApiKey(TOKEN);
            var custservice = new CustomerService();
            var Customeroptions = new CustomerListOptions { Email = UserEmail };
            StripeList<Customer> customers = custservice.List(Customeroptions);
            if (customers != null && customers.Data.Count() > 0 && customers.FirstOrDefault() != null && customers.FirstOrDefault().Sources != null)
            {
                result = customers.FirstOrDefault().Sources;
            }
            return result;
        }
        #endregion

        #region Stripe's Create/Get Connect Account
        [UnitOfWork]
        public ProcessViewModel CreateConnect(AddConnectAccountsViewModel model)
        {
            ProcessViewModel result = new ProcessViewModel();
            result.IsSuccess = false;
            result.Message = SiteConstants.Bankaccount_Failure;
            if (model.AppUserId <= 0)
            {
                return result;
            }

            StripeConfiguration.SetApiKey(TOKEN);
            try
            {
                var options = new AccountCreateOptions
                {
                    Email = model.CustomerEmailId,
                    Type = AccountType.Custom,
                    Country = COUNTRY,
                };
                var service = new AccountService();
                Account account = service.Create(options);
                model.AccountId = account.Id;
            }
            catch (Exception ex)
            {
                return result;

            }
            try
            {
                var ExAccountOption = new ExternalAccountCreateOptions()
                {
                    ExternalAccountBankAccount = new AccountBankAccountOptions
                    {
                        Country = COUNTRY,
                        Currency = CURRENCY,
                        AccountHolderName = model.AccountHolderName,
                        AccountHolderType = model.AccountHolderType,
                        RoutingNumber = model.RoutingNumber,
                        AccountNumber = model.AccountNumber
                    }
                };
                var eaService = new ExternalAccountService();
                var cExBankAccount = eaService.Create(model.AccountId, ExAccountOption);


                var appuser = _appUsersRepository.Get(model.AppUserId);
                DealerConnectCreation dealerConnectCreation;
                var currentacount = _dealerConnectCreationRepository.Get(x => x.User == appuser);
                if (currentacount != null)
                    dealerConnectCreation = currentacount;
                else
                {
                    dealerConnectCreation = new DealerConnectCreation();
                    dealerConnectCreation.User = appuser;
                }
                dealerConnectCreation.ActId = cExBankAccount.AccountId;
                dealerConnectCreation.ExternalId = cExBankAccount.Id;

                UpdateConnectAccount(dealerConnectCreation, model);
                dealerConnectCreation.IsVerified = true;
                _dealerConnectCreationRepository.InsertOrUpdate(dealerConnectCreation);

            }
            catch (Exception ex)
            {
                return result;
            }

            //if success entry in table
            result.IsSuccess = true;
            result.Message = SiteConstants.Bankaccount_Success;

            return result;
        }


        private bool UpdateConnectAccount(DealerConnectCreation dealerConnectCreation, AddConnectAccountsViewModel model)
        {
            string [] arr = {"Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado",
                "Connecticut", "Delaware", "District of Columbia", "Florida", "Georgia",
                "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine",
                "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada",
                "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota",
                "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Puerto Rico", "Rhode Island", "South Carolina", "South Dakota",
                "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming" };



            AccountDobOptions dobOptions = new AccountDobOptions()
            {
                Day = model.Dob2.Day,
                Month = model.Dob2.Month,
                Year = model.Dob2.Year
            };
            AccountPayoutScheduleOptions payoutScheduleOptions = new AccountPayoutScheduleOptions()
            {
                DelayDays = "2",
                Interval = "daily",

            };


            var aService = new AccountService(TOKEN);
            var getaccount = aService.Get(dealerConnectCreation.ActId);
            var updateaccount = new AccountUpdateOptions();
            updateaccount.LegalEntity = new AccountLegalEntityOptions();
            updateaccount.LegalEntity.Address = new AddressOptions();

            if (model.AddressLine1 != null)
                updateaccount.LegalEntity.Address.Line1 = model.AddressLine1;
            if (model.AddressLine2 != null)
                updateaccount.LegalEntity.Address.Line2 = model.AddressLine2;
            if (model.AddressPostalCode != null && model.AddressPostalCode.Length == 5)
                updateaccount.LegalEntity.Address.PostalCode = model.AddressPostalCode;
            if (model.AddressCity != null)
                updateaccount.LegalEntity.Address.City = model.AddressCity;
            if(model.State != 0)
            {
                updateaccount.LegalEntity.Address.State = arr[model.State];
            }
            System.Diagnostics.Debug.WriteLine("\n\n\n\n" + arr[model.State] + " " + model.State);
              updateaccount.PayoutSchedule = payoutScheduleOptions;
              updateaccount.PayoutStatementDescriptor = "DealerInventoryNetwork";
              updateaccount.StatementDescriptor = "DealerInventoryNetwork";


            if (model.PhoneNumber != null && model.PhoneNumber.Length == 10)
                updateaccount.LegalEntity.PhoneNumber = model.PhoneNumber;
            if (model.AccountHolderName != null)
            {
                updateaccount.BusinessName = model.AccountHolderName;
                updateaccount.LegalEntity.BusinessName = model.AccountHolderName;
            }

            if (model.URL.Contains("https://") && model.URL.Contains("."))
                updateaccount.BusinessUrl = model.URL;

            if (model.EIN != null && model.EIN.Length == 9)
                updateaccount.LegalEntity.BusinessTaxId = model.EIN;

            if (model.FirstName != null)
                updateaccount.LegalEntity.FirstName = model.FirstName;

            if (model.LastName != null)
                updateaccount.LegalEntity.LastName = model.LastName;

            if (model.Dob2 != null)
                updateaccount.LegalEntity.Dob = dobOptions;

            if (model.SSNLast4 != null && model.SSNLast4.Length == 4)
                updateaccount.LegalEntity.SSNLast4 = model.SSNLast4;

            updateaccount.LegalEntity.Type = "company";

            updateaccount.TosAcceptance = new AccountTosAcceptanceOptions();
            updateaccount.TosAcceptance.Date = DateTime.Now;
            updateaccount.TosAcceptance.Ip = model.IP; //Request.UserHostAddress;

            try
            {
                aService.Update(dealerConnectCreation.ActId, updateaccount);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("01 " + e.StackTrace + "\n\n" + e);
                return false;
            }


            return true;
        }

        public StripeList<IExternalAccount> GetConnectUserPaymentSourceList(string UserEmail)
        {
            StripeList<IExternalAccount> result = new StripeList<IExternalAccount>();
            StripeConfiguration.SetApiKey(TOKEN);
            var acctservice = new AccountService();

            StripeList<Account> connects = acctservice.List(null, null);
            if (connects != null && connects.Data != null && connects.Data.Count() > 0 && connects.Data.Any(x => x.Email == UserEmail) && connects.Data.FirstOrDefault(x => x.Email == UserEmail).ExternalAccounts != null)
            {
                return connects.Data.FirstOrDefault(x => x.Email == UserEmail).ExternalAccounts;

            }
            return null;
        }
        #endregion

        #region Common Methods
        public List<CustomerPaymentTypes> GetCustomerPaymentTypes(int BuyerId)
        {
            DealerCustomerCreation dealerCustomerCreation = _dealerCustomerCreationRepository.Get(x => x.User != null && x.User.Id == BuyerId);
            var customerPaymentTypes = _customerPaymentTypesRepository.GetAll(x => x.DealerCustomerCreation != null && x.DealerCustomerCreation.Id == dealerCustomerCreation.Id).ToList();
            if (customerPaymentTypes != null)
                return customerPaymentTypes;
            return null;
        }

        [UnitOfWork]
        public ProcessViewModel ProcessBuyNowHoldNow(BuyNowHoldNowDetailViewModel model)
        {
            ProcessViewModel result = new ProcessViewModel();
            result.IsSuccess = false;
            result.Message = SiteConstants.BankPayment_Failure;


            var DealerCarDtls = _dealerCarDetailsRepository.Get(x => x.Id == model.DealerCarDetailId);
            if (DealerCarDtls == null)
                return result;

            decimal totalPayment, partialPayment = 0, restPayment = 0;
            int timeToOnHoldCar = 0;
            int amount;
            if (model.IsOnHold) //For OnHold Process--partial payment
            {
                //call from web setting value
                var partialPaymentPerc = int.Parse(WebConfigurationManager.AppSettings["OnHoldPaymentPerc"]);
                timeToOnHoldCar = int.Parse(WebConfigurationManager.AppSettings["TimeToOnHoldCar"]);

                //calculate partial and pending amount 
                totalPayment = DealerCarDtls.Price;
                partialPayment = (totalPayment * partialPaymentPerc) / 100;
                restPayment = totalPayment - partialPayment;
                amount = Convert.ToInt32(partialPayment);
            }
            else ////For BuyNow Process--full payment
            {
                amount = Convert.ToInt32(DealerCarDtls.Price);
            }
            if (amount <= 0)
                return result;

            model.SellerId = DealerCarDtls.User.Id;
            var BuyerCustomer = _dealerCustomerCreationRepository.Get(x => x.User != null && x.User.Id == model.BuyerId);
            var SellerDetails = _dealerConnectCreationRepository.Get(x => x.User != null && x.User.Id == model.SellerId);
            if (BuyerCustomer == null || SellerDetails==null)
                return result;


            #region Stripe's Payment Related
            StripeConfiguration.SetApiKey(TOKEN);
            int PaymentTypeId;
            int.TryParse(model.PaymentTypeId.ToString(), out PaymentTypeId);

            var payment = BuyerCustomer.CustomerPaymentTypes.FirstOrDefault(x => x.Id == PaymentTypeId);
            if (payment == null)
                return result;
            try
            {
                var options1 = new ChargeCreateOptions
                {
                    Amount = (amount * 100),
                    Currency = CURRENCY,
                    SourceId = payment.PaymentId,
                    CustomerId = BuyerCustomer.CustomerId,
                    Destination =new ChargeDestinationCreateOptions
                    {
                        Account=SellerDetails.ActId
                    }
                };

                var service1 = new ChargeService();
                Charge charge = service1.Create(options1);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                return result;
            }

            //#region Balance Fund
            //double AvailableAmount = 0;
            //var bService = new BalanceService();
            //var balance = bService.Get();
            //if (balance != null)
            //{
            //    double converttoammount = 0.0;
            //    if (balance.Available.FirstOrDefault().Amount > 0)
            //    {

            //        converttoammount = Math.Round((double)(balance.Available.FirstOrDefault().Amount) / 100, 2, MidpointRounding.AwayFromZero);

            //    }
            //    AvailableAmount = converttoammount;

            //    if (Convert.ToDouble(amount) >= AvailableAmount)
            //        return result;

            //}
            //#endregion

            //try
            //{
            //    var SellerDetails = _dealerConnectCreationRepository.Get(x => x.User != null && x.User.Id == model.SellerId);
            //    var options = new TransferCreateOptions
            //    {
            //        Amount = amount,
            //        Currency = CURRENCY,
            //        Destination = SellerDetails.ActId
            //    };
            //    var service = new TransferService();
            //    Transfer Transfer = service.Create(options);
            //}
            //catch (Exception ex)
            //{
            //    return result;
            //}

            #endregion
            DealerCarDtls.IsSold = !model.IsOnHold;
            DealerCarDtls.IsOnHold = model.IsOnHold;
            _dealerCarDetailsRepository.Update(DealerCarDtls);


            CarOrderDetail carOrderDetail = new CarOrderDetail();
            CarOrderOnHold carOrderOnHold = new CarOrderOnHold();

            var buyerUser = _appUsersRepository.Get(x => x.Id == model.BuyerId);


            carOrderDetail = new CarOrderDetail
            {
                IsOnHold = model.IsOnHold,
                SellerDealer = DealerCarDtls.User,
                BuyerDealer = buyerUser,
                dealerCarDetails = DealerCarDtls
            };

            _carOrderDetailRepository.Insert(carOrderDetail);

            if (model.IsOnHold)
            {
                carOrderOnHold = new CarOrderOnHold
                {
                    TimeToHold = DateTime.UtcNow.AddDays(timeToOnHoldCar),
                    PartialAmountPaid = partialPayment,
                    RestAmount = restPayment,
                    carOrderDetail = carOrderDetail
                };

                _carOrderOnHoldRepository.Insert(carOrderOnHold);
            }
            result.IsSuccess = true;
            result.Message = SiteConstants.BankPayment_Success;

            return result;
        }

        #endregion
    }
}
