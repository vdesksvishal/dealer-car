﻿using Incite.Data.Repositories;
using Incite.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Trialcar.Web.Helper;

namespace Incite.Services
{
    public class CarSearchService : ICarSearchService
    {
        private readonly ICarMakerRepository _CarMakerRepository;
        private readonly ICarModelRepository _CarModelRepository;
        private readonly IDealerCarService _DealerCarService;

        public CarSearchService(ICarMakerRepository carMakerRepository, ICarModelRepository carModelRepository, IDealerCarService dealerCarService)
        {
            _CarMakerRepository = carMakerRepository;
            _CarModelRepository = carModelRepository;
            _DealerCarService = dealerCarService;
        }

        public CarFilterViewModel PrepareHomeCareFilter(CarFilterViewModel model = null)
        {
            if (model == null)
                model = new CarFilterViewModel();
            model.DistanceList = DropDownListHelper.BindDistanceList();
            model.CarMakerList.Add(new SelectListItem { Text = "Select Brand", Value = "0" });
            var allCarMaker = _CarMakerRepository.GetAll();
            if (allCarMaker.Any())
                model.CarMakerList.AddRange(allCarMaker.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }));
            model.CarModelList.Add(new SelectListItem { Text = "Select Model", Value = "0"});
            model.CarModelList.AddRange(_DealerCarService.CarModelListbyMaker(model.CB ?? 0).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }));
            model.MilageList = DropDownListHelper.BindMilageList();
            model.CarTypeList = DropDownListHelper.BindCarTypeList();
            model.LocationList = DropDownListHelper.BindLocationList();
            model.drivetrainList.Add(new SelectListItem { Text = "Drive Type", Value = "0" });
            model.drivetrainList.AddRange(_DealerCarService.DriveTypeList().Where(x=> !string.IsNullOrEmpty(x.DriveTypeName)).Select(x => new SelectListItem { Text = x.DriveTypeName, Value = x.Id.ToString() }));
            model.TranmissionTypeList.Add(new SelectListItem { Text = "Transmission Type", Value = "0" });
            model.TranmissionTypeList.Add(new SelectListItem { Text = "Automatic", Value = "1" });
            model.TranmissionTypeList.Add(new SelectListItem { Text = "Manual", Value = "2" });
            model.PriceTypeList.Add(new SelectListItem { Text = "Price Type", Value = "0" });
            model.PriceTypeList.AddRange(DropDownListHelper.BindPrice_TypeList().Where(x=>x.Value!=""));
            return model;

        }

        public CarFilterViewModel PrepareSortby(CarFilterViewModel model = null)
        {
            model.Sortbylist = DropDownListHelper.BindSortByList();
            return model;
        }
    }
}
