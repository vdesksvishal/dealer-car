﻿using Incite.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Trialcar.Web.Helper
{
    public static class DropDownListHelper
    {
        public static List<SelectListItem> BindSortByList()
        {
            List<SelectListItem> listitem = new List<SelectListItem>()
            {
                new SelectListItem{Text="A-Z",Value="0"},
                new SelectListItem{Text="Price (Low to High)",Value="1"},
                new SelectListItem{Text="Price (High to Low)",Value="2"},
                new SelectListItem{Text="Newest Items",Value="3"},
                new SelectListItem{Text="Oldest Items",Value="4"},
            };
            return listitem;

        }
        public static List<SelectListItem> BindDistanceList()
        {
            List<SelectListItem> listitem = new List<SelectListItem>()
            {
                new SelectListItem{Text="All Miles",Value="0"},
                new SelectListItem{Text="10 Miles",Value="10"},
                new SelectListItem{Text="20 Miles",Value="20"},
                new SelectListItem{Text="30 Miles",Value="30"},
                new SelectListItem{Text="40 Miles",Value="40"},
                new SelectListItem{Text="50 Miles",Value="50"},
                new SelectListItem{Text="75 Miles",Value="75"},
                new SelectListItem{Text="100 Miles",Value="100"},
                new SelectListItem{Text="150 Miles",Value="150"},
                new SelectListItem{Text="200 Miles",Value="200"},
                new SelectListItem{Text="300 Miles",Value="300"},
                new SelectListItem{Text="500 Miles",Value="500"},
            };
            return listitem;
        }

        public static List<SelectListItem> BindMilageList()
        {
            List<SelectListItem> listitem = new List<SelectListItem>()
            {
                new SelectListItem{Text="Any Miles",Value="0"},
                new SelectListItem{Text="Under 15,000",Value="15000"},
                new SelectListItem{Text="Under 30,000",Value="30000"},
                new SelectListItem{Text="Under 45,000",Value="45000"},
                new SelectListItem{Text="Under 45,000",Value="45000"},
                new SelectListItem{Text="Under 60,000",Value="60000"},
                new SelectListItem{Text="Under 75,000",Value="75000"},
                new SelectListItem{Text="Under 100,000",Value="100000"},
                new SelectListItem{Text="Under 150,000",Value="150000"},
                new SelectListItem{Text="Under 200,000",Value="200000"},
                new SelectListItem{Text="Over 200,000",Value="2"},
            };
            return listitem;
        }
        public static List<SelectListItem> BindCarTypeList()
        {
            List<SelectListItem> listitem = new List<SelectListItem>()
            {
               // new SelectListItem{Text="Type of Car",Value="0"},
                new SelectListItem{Text="Used",Value=Convert.ToString((int)SiteEnum.CarType.Used)},
                new SelectListItem{Text="New",Value=Convert.ToString((int)SiteEnum.CarType.New)},
                new SelectListItem{Text="Certified",Value=Convert.ToString((int)SiteEnum.CarType.Certified)},
            };
            return listitem;
        }

        public static List<SelectListItem> BindTitleTypeList()
        {
            List<SelectListItem> listitem = new List<SelectListItem>()
            {
                new SelectListItem{Text="Car Title",Value="0"},
                new SelectListItem{Text="Attached",Value=Convert.ToString((int)SiteEnum.TitleType.Attached)},
                new SelectListItem{Text="On Hand",Value=Convert.ToString((int)SiteEnum.TitleType.OnHand)},
                new SelectListItem{Text="Reconstructed",Value=Convert.ToString((int)SiteEnum.TitleType.Reconstructed)},
            };
            return listitem;
        }

        public static List<SelectListItem> BindCarConditionList()
        {
            List<SelectListItem> listitem = new List<SelectListItem>()
            {
                new SelectListItem{Text="Car Condition",Value="0"},
                new SelectListItem{Text="Retail Ready",Value=Convert.ToString((int)SiteEnum.CarConditionType.RetailReady)},
                new SelectListItem{Text="Workshop Required", Value=Convert.ToString((int)SiteEnum.CarConditionType.WorkshopRequired) },
                new SelectListItem{Text="Transmission Issue",Value=Convert.ToString((int)SiteEnum.CarConditionType.TransmissionIssue)},
                new SelectListItem{Text="Engine Issue",Value=Convert.ToString((int)SiteEnum.CarConditionType.EngineIssue)},
                 new SelectListItem{Text="For Scrap",Value=Convert.ToString((int)SiteEnum.CarConditionType.Scrap)}
            };
            return listitem;
        }

        public static List<SelectListItem> BindenginetypeList()
        {
            List<SelectListItem> listitem = new List<SelectListItem>()
            {
                new SelectListItem{Text="Select Engine",Value=""},
                new SelectListItem{Text="Gasoline",Value=Convert.ToString((int)SiteEnum.EngineType.Gasoline)},
                new SelectListItem{Text="Diesel",Value=Convert.ToString((int)SiteEnum.EngineType.Diesel)},
                new SelectListItem{Text="Electric",Value=Convert.ToString((int)SiteEnum.EngineType.Electric)},
                new SelectListItem{Text="Hybrid",Value=Convert.ToString((int)SiteEnum.EngineType.Hybrid)},

            };
            return listitem;
        }

        public static List<SelectListItem> BindPrice_TypeList()
        {
            List<SelectListItem> listitem = new List<SelectListItem>()
            {
               // new SelectListItem{Text="Select Price",Value=""},
                new SelectListItem{Text="Dealer Price",Value=Convert.ToString((int)SiteEnum.PriceType.DealerPrice)},
                new SelectListItem{Text="Retail Price",Value=Convert.ToString((int)SiteEnum.PriceType.RetailPrice)},
                new SelectListItem{Text="Fix Price",Value=Convert.ToString((int)SiteEnum.PriceType.FixPrice)},

            };
            return listitem;
        }
        public static List<SelectListItem> BindStateList()
        {
            List<SelectListItem> listitem = new List<SelectListItem>()
            {
                new SelectListItem{Text="Select",Value=""},
                new SelectListItem{Text="Alabama",Value="1"},
                new SelectListItem{Text="Alaska",Value="2"},
                new SelectListItem{Text="Arizona",Value="3"},
                 new SelectListItem{Text="California",Value="4"},
                 new SelectListItem{Text="Colorado",Value="5"},
                 new SelectListItem{Text="Connecticut",Value="6"},
                 new SelectListItem{Text="Delaware",Value="7"},
                 new SelectListItem{Text="Florida",Value="8"},
                 new SelectListItem{Text="Georgia",Value="9"},
                 new SelectListItem{Text="Hawaii",Value="10"},
                 new SelectListItem{Text="Idaho",Value="11"},
                 new SelectListItem{Text="Illinois",Value="12"},
                 new SelectListItem{Text="Indiana",Value="13"},
                 new SelectListItem{Text="Iowa",Value="14"},
                 new SelectListItem{Text="Kansas",Value="15"},
                 new SelectListItem{Text="Kentucky",Value="16"},
                 new SelectListItem{Text="Louisiana",Value="17"},
                 new SelectListItem{Text="Maine",Value="18"},
                 new SelectListItem{Text="Maryland",Value="19"},
                 new SelectListItem{Text="Massachusetts",Value="20"},
                 new SelectListItem{Text="Michigan",Value="21"},
                 new SelectListItem{Text="Minnesota",Value="22"},
                 new SelectListItem{Text="Mississippi",Value="23"},
                 new SelectListItem{Text="Missouri",Value="24"},
                 new SelectListItem{Text="Montana",Value="25"},
                 new SelectListItem{Text="Nebraska",Value="26"},
                 new SelectListItem{Text="Nevada",Value="27"},
                 new SelectListItem{Text="New Hampshire",Value="28"},
                 new SelectListItem{Text="New Jersey",Value="29"},
                 new SelectListItem{Text="New Mexico",Value="30"},
                 new SelectListItem{Text="New York",Value="31"},
                 new SelectListItem{Text="North Carolina",Value="32"},
                 new SelectListItem{Text="North Dakota",Value="33"},
                 new SelectListItem{Text="Ohio",Value="34"},
                 new SelectListItem{Text="Oklahoma",Value="35"},
                 new SelectListItem{Text="Oregon",Value="36"},
                 new SelectListItem{Text="Pennsylvania",Value="37"},
                 new SelectListItem{Text="Rhode Island",Value="38"},
                 new SelectListItem{Text="South Carolina",Value="39"},
                 new SelectListItem{Text="South Dakota",Value="40"},
                 new SelectListItem{Text="Tennessee",Value="41"},
                 new SelectListItem{Text="Texas",Value="42"},
                 new SelectListItem{Text="Utah",Value="43"},
                 new SelectListItem{Text="Vermont",Value="44"},
                 new SelectListItem{Text="Virginia",Value="45"},
                 new SelectListItem{Text="Washington",Value="46"},
                 new SelectListItem{Text="West Virginia",Value="47"},
                 new SelectListItem{Text="Wisconsin",Value="48"},
                 new SelectListItem{Text="Wyoming",Value="49"},
            };
            return listitem;
        }

        public static List<SelectListItem> BindLocationList()
        {
            List<SelectListItem> listitem = new List<SelectListItem>()
            {
                new SelectListItem{Text="Select Location",Value=""},
                new SelectListItem{Text="Location 1",Value="1"},
                new SelectListItem{Text="Location 2",Value="2"},
                new SelectListItem{Text="Location 3",Value="3"},
                new SelectListItem{Text="Location 4",Value="4"},
                new SelectListItem{Text="Location 5",Value="5"},

            };
            return listitem;
        }


    }
}