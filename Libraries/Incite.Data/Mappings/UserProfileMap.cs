﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    public  class UserProfileMap: ClassMap<UserProfile>
    {
        public UserProfileMap()
        {

            Table("UserProfile");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.FirstName).Column("FirstName");
            Map(x => x.LastName).Column("LastName");
            Map(x => x.MobileNumber).Column("MobileNumber");
            Map(x => x.AdditionalMobileNumber).Column("AdditionalMobileNumber");
            Map(x => x.AdditionalEmail).Column("AdditionalEmail");
            Map(x => x.FAX).Column("FAX");
            Map(x => x.AdminName).Column("AdminName");
            Map(x => x.ProfileImage).Column("ProfileImage");
            Map(x => x.ProfileThmbnailImage).Column("ProfileThmbnailImage");
            Map(x => x.State).Column("State");
            Map(x => x.CreateDate).Column("CreateDate");
            Map(x => x.LastLoginDate).Column("LastLoginDate");
            Map(x => x.DealershipAddressLine1).Column("DealershipAddressLine1");
            Map(x => x.DealershipAddressLine2).Column("DealershipAddressLine2");
            Map(x => x.Zipcode).Column("Zipcode");
            Map(x => x.City).Column("City");
            Map(x => x.LicenseNumber).Column("LicenseNumber");
            //Relationship

            References(x => x.User).Cascade.All().Column("AppUserId").ForeignKey("FK_UserProfile_AppUsers");

        }
    }
}
