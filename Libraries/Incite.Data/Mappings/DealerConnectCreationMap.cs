﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    public class DealerConnectCreationMap : ClassMap<DealerConnectCreation>
    {
        public DealerConnectCreationMap()
        {
            Table("DealerConnectCreation");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.ActId).Column("ActId");
            Map(x => x.ExternalId).Column("ExternalId");
            Map(x => x.IsVerified).Column("IsVerified");

            //mapping

            References(x => x.User).Cascade.All().Column("AppUserId").ForeignKey("FK_DealerConnectCreation_AppUsers");
        }
        
    }
}
