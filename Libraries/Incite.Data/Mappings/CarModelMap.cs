﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
   public class CarModelMap : ClassMap<CarModel>
    {
        public CarModelMap()
        {
            Table("CarModel");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Name).Column("Name");
            //HasMany(x => x.carDetails).KeyColumn("CarModelId");
        }
    }
}
