﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    public class CarDetailsMap : ClassMap<CarDetails>
    {
        public CarDetailsMap()
        {
            Table("CarDetails");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");

            Map(x => x.IndexId).Column("IndexId").Unique();
            Map(x => x.Trim).Column("Trim");
            Map(x => x.Price).Column("Price");
            Map(x => x.Engine).Column("Engine");
            Map(x => x.BodyType).Column("BodyType");
            Map(x => x.ColorsExterior).Column("ColorsExterior").CustomSqlType("varbinary(max)");
            Map(x => x.ColorsInterior).Column("ColorsInterior").CustomSqlType("varbinary(max)");
            Map(x => x.Length).Column("Length");
            Map(x => x.Width).Column("Width");
            Map(x => x.Height).Column("Height");
            Map(x => x.WheelBase).Column("WheelBase");
            Map(x => x.CurbWeight).Column("CurbWeight");
            Map(x => x.Cylinders).Column("Cylinders");
            Map(x => x.EngineSize).Column("EngineSize");
            Map(x => x.HP).Column("HP");
            Map(x => x.RPM).Column("RPM");
            Map(x => x.Torque).Column("Torque");
            Map(x => x.TorqueRPM).Column("TorqueRPM");
            Map(x => x.FuelTankCap).Column("FuelTankCap");
            Map(x => x.CombinedMPG).Column("CombinedMPG");
            Map(x => x.EPAMileage).Column("EPAMileage");
            Map(x => x.RangeMiles).Column("RangeMiles");

            //mapping

            References(x => x.Maker).Cascade.All().Column("CarMakerId").ForeignKey("FK_CarDetails_CarMaker");
            References(x => x.Model).Cascade.All().Column("CarModelId").ForeignKey("FK_CarDetails_CarModel");
            References(x => x.Year).Cascade.All().Column("CarYearId").ForeignKey("FK_CarDetails_CarYear");

            References(x => x.FuelType).Cascade.All().Column("FuelTypeId").ForeignKey("FK_CarDetails_FuelType");
            References(x => x.Transmission).Cascade.All().Column("TransmissionId").ForeignKey("FK_CarDetails_Transmission");
            References(x => x.DriveType).Cascade.All().Column("DriveTypeId").ForeignKey("FK_CarDetails_DriveType");
        }
    }
}
