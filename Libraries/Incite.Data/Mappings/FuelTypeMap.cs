﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    class FuelTypeMap: ClassMap<FuelType>
    {
        public FuelTypeMap()
        {
            Table("FuelType");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.FuelTypeName).Column("FuelTypeName");
            
           // HasMany(x => x.carDetails).KeyColumn("FuelTypeId");
        }
    }
}
