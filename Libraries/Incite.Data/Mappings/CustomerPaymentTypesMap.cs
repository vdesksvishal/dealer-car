﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    public class CustomerPaymentTypesMap:ClassMap<CustomerPaymentTypes>
    {
        public CustomerPaymentTypesMap()
        {
            Table("CustomerPaymentTypes");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.PaymentId).Column("PaymentId");
            Map(x => x.PaymentType).Column("PaymentType");
            Map(x => x.StripeId).Column("StripeId");
            Map(x => x.IsVerified).Column("IsVerified");
            //mapping
            References(x => x.DealerCustomerCreation).Cascade.All().Column("DealerCustId").ForeignKey("FK_CustomerPaymentTypes_DealerCustomerCreation");

        }
    }
}
