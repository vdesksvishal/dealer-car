﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    public class DealerCustomerCreationMap : ClassMap<DealerCustomerCreation>
    {
        public DealerCustomerCreationMap()
        {
            Table("DealerCustomerCreation");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.CustomerId).Column("CustomerId");
            Map(x => x.IsVerified).Column("IsVerified");
            //mapping

            References(x => x.User).Cascade.All().Column("AppUserId").ForeignKey("FK_DealerCustomerCreation_AppUsers");
            HasMany(x => x.CustomerPaymentTypes).KeyColumn("DealerCustId");
        }
        
    }
}
