﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Incite.Core.Domain;

namespace Incite.Data.Mappings
{
    public class CarOrderOnHoldMap : ClassMap<CarOrderOnHold>
    {
        public CarOrderOnHoldMap()
        {
            Table("CarOrderOnHold");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            //Map(x => x.CarOrderDetailId).Column("CarOrderDetailId");
            Map(x => x.PartialAmountPaid).Column("PartialAmountPaid");
            Map(x => x.RestAmount).Column("RestAmount");
            Map(x => x.TimeToHold).Column("TimeToHold");

            //mapping
            References(x => x.carOrderDetail).Cascade.All().Column("CarOrderDetailId").ForeignKey("FK_CarOrderOnHold_CarOrderDetail");
        }
    }
}
