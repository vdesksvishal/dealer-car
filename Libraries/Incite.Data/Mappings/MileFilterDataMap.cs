﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    public class MileFilterDataMap: ClassMap<MileFilterData>
    {
        public MileFilterDataMap()
        {
            Table("MileFilter");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Zipcod).Column("Zipcod");
            Map(x => x.Mile).Column("Mile");
            Map(x => x.Record).Column("Record").CustomSqlType("nvarchar(MAX)").Length(int.MaxValue);

        }

    }
}
