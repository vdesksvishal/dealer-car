﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    public class CarMakerMap : ClassMap<CarMaker>
    {
        public CarMakerMap()
        {
            Table("CarMaker");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Name).Column("Name");
            //HasMany(x => x.carModelYearRels).KeyColumn("CarMakerId");
            //HasMany(x => x.carDetails).KeyColumn("CarMakerId");


        }
    }
}
