﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    public class DealerCarDetailsMap:ClassMap<DealerCarDetails>
    {
        public DealerCarDetailsMap()
        {
            Table("DealerCarDetails");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Miles).Column("Miles");
            Map(x => x.PriceType).Column("pricetype");
            Map(x => x.OwnershipType).Column("ownershiptype");
            Map(x => x.TitleType).Column("titleId");
            Map(x => x.CarConditionType).Column("conditionId");
            Map(x => x.VIN).Column("VIN");
            Map(x => x.VehicleOverview).Column("VehicleOverview");
            Map(x => x.CreatedTs).Column("CreatedTs");
            Map(x => x.Price).Column("Price");
            Map(x=>x.UpdatedTs).Column("UpdatedTs");
            Map(x => x.IsOnHold).Column("IsOnHold");
            Map(x => x.IsSold).Column("IsSold");
            //mapping

            References(x => x.User).Cascade.All().Column("AppUserId").ForeignKey("FK_DealerCarDetails_Appuser");
            References(x => x.CarDetails).Cascade.All().Column("CarDetailsId").ForeignKey("FK_DealerCarDetails_CarDetails");
            References(x => x.EngineType).Cascade.All().Column("EngineTypeId").ForeignKey("FK_DealerCarDetails_EngineType");
            HasMany(x => x.CarAccessories).KeyColumn("DealerCarDetailsId");
            HasMany(x => x.CarImages).KeyColumn("DealerCarDetailsId");
        }
    }
}
