﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    class DriveTypeMap:ClassMap<DriveType>
    {
        public DriveTypeMap()
        {
            Table("DriveType");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.DriveTypeName).Column("DriveTypeName");

           // HasMany(x => x.carDetails).KeyColumn("DriveTypeId");
        }
    }
}
