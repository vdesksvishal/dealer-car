﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Incite.Core.Domain;

namespace Incite.Data.Repositories
{
    public interface ICarOrderOnHoldRepository : IRepository<CarOrderOnHold, int>
    {
    }
}
