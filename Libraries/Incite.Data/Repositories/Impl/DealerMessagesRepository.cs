﻿using Incite.Core.Domain;

namespace Incite.Data.Repositories
{
    public class DealerMessagesRepository : RepositoryBase<DealerMessages, int>, IDealerMessagesRepository
    {
    }
}
