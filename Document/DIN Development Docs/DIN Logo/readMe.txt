Thanks you for your order.

Files description:

* Free font used on Logo Design : harabara (https://www.dafont.com/harabara-mais.font)
				  helvetica (https://www.myfonts.com/fonts/linotype/neue-helvetica/)
* main files.zip included:
   - Outlined	 : Ai CS5 (RGB color mode) and EPS CS5 (CMYK color mode)
   - PDF format (User Manual Guide & Tips 'FINAL' purposes)
   - readMe.txt (Font information & files description)
* EPS CS5 in CMYK color mode 300 ppi (Printing purposes)
* Ai CS5 in RGB color mode 75 ppi (Digital Purposes)

Notes:
*In PDF format (User Manual Guide) you will find common answers on how to customize the logo color & text. If you need more help or assistance, feel free to contact me via my Fiverr profile page. I really glad to help. :)

Thank You
Best Regards,

SharpNshark